import Pagination from '@/components/Helpers/Pagination';

export default {
  data: () => ({
    PER_PAGE: 10,
  }),
  components: { Pagination },

  methods: {
    paginatedItems(items, perPage, currentPage) {
      const firstItemIndex = perPage * currentPage;
      const lastItemIndex  = firstItemIndex + perPage;

      return items.slice(firstItemIndex, lastItemIndex);
    },
  },
}
