import { mapState } from 'vuex';
import SupplierForm from '@/components/Suppliers/SupplierForm';
// import Supplier from '@/classes/Supplier';

export default {
  components: { SupplierForm },

  data: () => ({
    pending: false,
  }),
  computed: {
    ...mapState('supplierPricelists', [
      'supplierPricelists',
    ]),
  },
}
