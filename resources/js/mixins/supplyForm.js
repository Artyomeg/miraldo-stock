import { mapState } from 'vuex';
import SupplyForm from '@/components/Supplies/SupplyForm';
import Supply from '@/classes/Supply';

export default {
  components: { SupplyForm },

  data: () => ({
    supplier: new Supply(),
    pending: false,
  }),
  computed: {
    ...mapState('supplierPricelists', [
      'supplierPricelists',
    ]),
  },
}
