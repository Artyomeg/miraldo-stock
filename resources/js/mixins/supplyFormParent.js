export const getFormDataParams = {
  methods: {
    getFormDataParams(form, supply) {
      const arrayFormElements = [...form.target.elements];
      const formData = new FormData();

      // TODO: передалать на reduce
      arrayFormElements.map((el, i) => {
        if (el.name !== '') {
          formData.append(form.target.elements[i].name, form.target.elements[i].value)
        }
      });

      if (supply.id) {
        formData.append('id', supply.id);
      }

      formData.append('supplier_id', supply.supplierId);
      formData.append('currency_id', supply.currencyId);
      formData.append('invoice_number', supply.invoiceNumber);

      if (supply.invoiceDate) {
        formData.append('invoice_date', supply.invoiceDate);
      }

      return formData;
    },
  },
}
