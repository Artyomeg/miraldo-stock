import store from '@/store/store';

export default {
  store,
  install(Vue) {
    Vue.prototype.$myStore = store;
  },
}
