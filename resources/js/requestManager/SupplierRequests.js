import { snakeCaseKeys, deleteRequest } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_SUPPLIERS_URL = '/api/suppliers';
const GET_SUPPLIER_URL  = '/api/suppliers/{id}';
const CREATE_SUPPLIER_URL = '/api/suppliers';
const UPDATE_SUPPLIER_URL = '/api/suppliers/{id}';
const DELETE_SUPPLIER_URL = '/api/suppliers/{id}';
const DELETE_PRICELIST_URL = '/api/supplier-pricelists/{id}';

export const getSuppliersRequest = () => axios.get(GET_SUPPLIERS_URL);

export const getSupplierRequest = id => {
  const getSupplierUrl = GET_SUPPLIER_URL.replace(/{id}/g, id);
  return axios.get(getSupplierUrl);
}

export const createSupplierRequest = params => axios.post(CREATE_SUPPLIER_URL, {
  _token: getMeta('csrf-token'),
  ...snakeCaseKeys(params),
})

export const updateSupplierRequest = params => {
  const updateSupplierUrl = UPDATE_SUPPLIER_URL.replace(/{id}/g, params.id),
   snakeCasedParams = snakeCaseKeys(params);

  return axios.patch(updateSupplierUrl, {
    _token: getMeta('csrf-token'),
    ...snakeCasedParams,
  });
}

export const deleteSupplierRequest = id => deleteRequest(DELETE_SUPPLIER_URL, id);

export const deletePriceListRequest = id => {
  const deletePricelistUrl = DELETE_PRICELIST_URL.replace(/{id}/g, id);
  const deleteFormParams = {
    _method: 'DELETE',
    _token: getMeta('csrf-token')
  };

  return axios.delete(deletePricelistUrl, deleteFormParams);
}
