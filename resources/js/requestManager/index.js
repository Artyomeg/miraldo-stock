import { snakeCase } from 'lodash';
import { getMeta } from '@/helpers/template';

export const snakeCaseKeys = params => Object.keys(params).reduce((accumulator, loopParam) => {
  accumulator[snakeCase(loopParam)] = params[loopParam];
  return accumulator;
}, {});

export const replaceUrlPattern = (url, params) =>
  Object.keys(params).reduce((replacedUrl, paramKey) => {
    const regExp = new RegExp(`{${paramKey}}`, "g");
    replacedUrl = replacedUrl.replace(regExp, params[paramKey]);

    return replacedUrl;
  }, url);

export const deleteRequest = (url, id) => {
  const deleteUrl = url.replace(/{id}/g, id);

  return axios.delete(deleteUrl, {
    _token: getMeta('csrf-token'),
  });
}
