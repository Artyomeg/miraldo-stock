import { snakeCaseKeys } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_CLIENTS_URL   = '/api/clients';
const GET_CLIENT_URL    = '/api/clients/{id}';
const CREATE_CLIENT_URL = '/api/clients';
const UPDATE_CLIENT_URL = '/api/clients/{id}';
const DELETE_CLIENT_URL = '/clients/{id}';

export const getClientsRequest = onlyNotArchived => axios.get(GET_CLIENTS_URL, {
  params: { onlyNotArchived },
});

export const getClientRequest = id => {
  const getClientUrl = GET_CLIENT_URL.replace(/{id}/g, id);
  return axios.get(getClientUrl);
}

export const createClientRequest = params =>
  axios.post(CREATE_CLIENT_URL, {
    _token: getMeta('csrf-token'),
    ...params,
  });

export const updateClientRequest = params => {
  const updateClientUrl = UPDATE_CLIENT_URL.replace(/{id}/g, params.id),
    snakeCasedParams = snakeCaseKeys(params);

  return axios.patch(updateClientUrl, {
    _token: getMeta('csrf-token'),
    ...snakeCasedParams,
  });
}
export const deleteClientRequest = id => {
  const deleteClientUrl = DELETE_CLIENT_URL.replace(/{id}/g, id);
  const deleteFormParams = {
    _method: 'DELETE',
    _token: getMeta('csrf-token')
  };

  return axios.post(deleteClientUrl, deleteFormParams);
}
