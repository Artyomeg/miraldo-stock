import { snakeCaseKeys, deleteRequest } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_SUPPLIER_INVOICE_URL    = '/api/supplier-invoices/{id}';
const CREATE_SUPPLIER_INVOICE_URL = '/api/supplier-invoices/store';
const UPDATE_SUPPLIER_INVOICE_URL = '/api/supplier-invoices/{id}';
const DELETE_SUPPLIER_INVOICE_URL = '/api/supplier-invoices/{id}';

export const getSupplierInvoiceRequest = id => {
  const getSupplierInvoiceUrl = GET_SUPPLIER_INVOICE_URL.replace(/{id}/g, id);
  return axios.get(getSupplierInvoiceUrl);
}

export const createSupplierInvoiceRequest = params => axios.post(CREATE_SUPPLIER_INVOICE_URL, {
  _token: getMeta('csrf-token'),
  ...snakeCaseKeys(params),
})

export const updateSupplierInvoiceRequest = params => {
  const updateSupplierUrl = UPDATE_SUPPLIER_INVOICE_URL.replace(/{id}/g, params.id),
    snakeCasedParams = snakeCaseKeys(params);

  return axios.patch(updateSupplierUrl, {
    _token: getMeta('csrf-token'),
    ...snakeCasedParams,
  });
}

export const deleteSupplierInvoiceRequest = id => deleteRequest(DELETE_SUPPLIER_INVOICE_URL, id);
