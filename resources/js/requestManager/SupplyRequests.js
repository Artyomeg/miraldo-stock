import { snakeCaseKeys, deleteRequest } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_SUPPLY_URL          = '/api/supplies/{id}';
const GET_SUPPLY_PRODUCTS_URL = '/api/supplies/{id}/supply-products';
const CREATE_SUPPLY_URL       = '/api/supplies';
const UPDATE_SUPPLY_URL       = '/api/supplies/{id}';
const DELETE_SUPPLY_URL       = '/api/supplies/{id}';

export const getSupplyRequest = id => {
  const getSupplyUrl = GET_SUPPLY_URL.replace(/{id}/g, id);
  return axios.get(getSupplyUrl);
}

export const getSupplyProductsRequest = id => {
  const getSupplyUrl = GET_SUPPLY_PRODUCTS_URL.replace(/{id}/g, id);
  return axios.get(getSupplyUrl);
}

export const createSupplyRequest = params => {
  if (params instanceof FormData) {
    params.append('_token', getMeta('csrf-token'));

    return axios.post(CREATE_SUPPLY_URL, params);
  } else {
    return axios.post(CREATE_SUPPLY_URL, {
      _token: getMeta('csrf-token'),
      ...params,
    });
  }
}

export const updateSupplyRequest = (params, id) => {
  const updateSupplyUrl = UPDATE_SUPPLY_URL.replace(/{id}/g, id);

  if (params instanceof FormData) {
    params.append('_token', getMeta('csrf-token'));

    return axios.post(updateSupplyUrl, params);
  } else {
    return axios.post(updateSupplyUrl, {
      _token: getMeta('csrf-token'),
      ...params,
    });
  }
}

export const deleteSupplyRequest = id => deleteRequest(DELETE_SUPPLY_URL, id);
