import { snakeCaseKeys } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_INVOICES_URL                  = '/api/invoices';
const GET_INVOICE_URL                   = '/api/invoices/{id}';
const GET_INVOICE_INVOICE_PRODUCTS_URL  = '/api/invoices/{id}/invoice-products';
const CREATE_INVOICES_URL               = '/invoices';
const UPDATE_INVOICES_URL               = '/api/invoices/{id}';

export const getInvoicesRequest = () => axios.get(GET_INVOICES_URL);

export const getInvoiceRequest = id => {
  const getInvoiceUrl = GET_INVOICE_URL.replace(/{id}/g, id);
  return axios.get(getInvoiceUrl);
}

export const getInvoiceInvoiceProductsRequest = id => {
  const getInvoiceInvoiceProductsUrl = GET_INVOICE_INVOICE_PRODUCTS_URL.replace(/{id}/g, id);
  return axios.get(getInvoiceInvoiceProductsUrl);
}

export const createInvoiceRequest = params => axios.post(CREATE_INVOICES_URL, params);

export const updateInvoiceRequest = params => {
  const updateInvoiceUrl = UPDATE_INVOICES_URL.replace(/{id}/g, params.id),
    snakeCasedParams = snakeCaseKeys(params);

  return axios.patch(updateInvoiceUrl, {
    _token: getMeta('csrf-token'),
    ...snakeCasedParams,
  });
}
