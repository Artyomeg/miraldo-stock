const GET_CLIENT_PAYMENTS_REPORT_URL = '/api/reports/by-client-payments';
const GET_REVISION_BY_SUPPLIERS_REPORT_URL = '/api/reports/revision-by-suppliers';
const GET_SUPPLIERS_REPORT_URL = '/api/reports/by-supplies';
const GET_PRODUCTS_REPORT_URL = '/api/reports/by-products';
const GET_PRODUCTS_MOVING_REPORT_URL = '/api/reports/products-moving';
const GET_REVISION_REPORT_URL = '/api/revision/get-supplies-and-invoices';

export const getPaymentsReportRequest = params => axios.get(GET_CLIENT_PAYMENTS_REPORT_URL, { params });

// 2.1
export const getRevisionBySuppliersReportRequest = params =>
  axios.get(GET_REVISION_BY_SUPPLIERS_REPORT_URL, { params });

// 2.2
export const getSuppliersReportRequest = params => axios.get(GET_SUPPLIERS_REPORT_URL, { params });

export const getProductsReportRequest = params => axios.post(GET_PRODUCTS_REPORT_URL, params);

export const getProductsMovingReportRequest = params => axios.post(GET_PRODUCTS_MOVING_REPORT_URL, params);

export const getRevisionReportRequest = params => axios.post(GET_REVISION_REPORT_URL, params);
