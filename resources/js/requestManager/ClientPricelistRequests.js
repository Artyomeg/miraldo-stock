import { getMeta } from '@/helpers/template';
import { replaceUrlPattern } from '@/requestManager';

const GET_PRICE_LIST_URL       = '/api/client-pricelists/{id}';
const DELETE_PRICE_LIST_URL    = '/api/client-pricelists/{id}';
const DUPLICATE_PRICE_LIST_URL = '/api/client-pricelists/{id}/duplicate';

export const getPriceListRequest = id => axios.get(
  replaceUrlPattern(GET_PRICE_LIST_URL, { id }));

export const deletePriceListRequest = id => axios.post(
  replaceUrlPattern(DELETE_PRICE_LIST_URL, { id }),
  {
    _method: 'DELETE',
    _token: getMeta('csrf-token')
  });

export const duplicatePriceListRequest = ({ id, newPriceList }) => axios.post(
  replaceUrlPattern(DUPLICATE_PRICE_LIST_URL, { id }),
  {
    _token: getMeta('csrf-token'),
    ...newPriceList,
  });
