import { snakeCaseKeys } from '@/requestManager';
import { getMeta } from '@/helpers/template';

const GET_PRODUCTS_URL              = '/api/products';
const GET_PRODUCT_URL               = '/api/products/{id}';
const CREATE_PRODUCTS_URL           = '/api/products';
const UPDATE_PRODUCTS_URL           = '/api/products/{id}';
const DELETE_PRODUCT_URL            = '/products/{id}';

const GET_PRODUCT_PRICES_URL        = '/api/products/prices';
const GET_PRODUCT_SUPPLY_PRICES_URL = '/api/products/supply-prices';
const GET_PRODUCTS_BARCODES_URL     = '/api/products/barcodes';

export const getProductsRequest = (sortedBySupplierCode = false) => axios.get(GET_PRODUCTS_URL, {
  params: {
    order: sortedBySupplierCode
      ? 'supplier-code'
      : undefined
  },
});

export const getProductPricesRequest = () => axios.get(GET_PRODUCT_PRICES_URL);

export const getProductWithSupplyPricesRequest = () => axios.get(GET_PRODUCT_SUPPLY_PRICES_URL);

export const getProductRequest = id => {
  const getProductUrl = GET_PRODUCT_URL.replace(/{id}/g, id);
  return axios.get(getProductUrl);
}

export const createProductRequest = params => axios.post(CREATE_PRODUCTS_URL, {
  _token: getMeta('csrf-token'),
  ...params,
});

export const updateProductRequest = params => {
  const updateProductUrl = UPDATE_PRODUCTS_URL.replace(/{id}/g, params.id),
   snakeCasedParams = snakeCaseKeys(params);

  return axios.patch(updateProductUrl, {
    _token: getMeta('csrf-token'),
    ...snakeCasedParams,
  });
}

export const deleteProductRequest = id => {
  const deleteFormParams = {
    _method: 'DELETE',
    _token: getMeta('csrf-token')
  }
  const deleteProductUrl = DELETE_PRODUCT_URL.replace(/{id}/g, id);

  return axios.post(deleteProductUrl, deleteFormParams);
}

export const getProductsBarcodesRequest = () => axios.get(GET_PRODUCTS_BARCODES_URL);
