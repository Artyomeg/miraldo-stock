import { getMeta } from '@/helpers/template';
import { deleteRequest } from '@/requestManager';

const CREATE_PAYMENT_URL = '/api/payments/store';
const DELETE_PAYMENT_URL = '/api/payments/{id}';

export const createPaymentRequest = params => axios.post(CREATE_PAYMENT_URL, {
  _token: getMeta('csrf-token'),
  ...params,
})

export const deletePaymentRequest = id => deleteRequest(DELETE_PAYMENT_URL, id);
