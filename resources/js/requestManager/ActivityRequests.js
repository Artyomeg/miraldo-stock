const GET_ACTIVITY_URL = '/api/activity';

export const getActivityRequest = params => axios.get(GET_ACTIVITY_URL, {
  params: {
    model_type: 'App\\Models\\User',
    ...params,
  },
});
