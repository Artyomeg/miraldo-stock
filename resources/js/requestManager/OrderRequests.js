import { replaceUrlPattern } from '@/requestManager';

const GET_ORDERS_URL          = '/api/orders';
const GET_ORDER_URL           = '/api/orders/{id}';
const CREATE_ORDER_URL        = '/api/orders';
const UPDATE_ORDER_URL        = '/api/orders/{id}';
const DELETE_ORDER_URL        = '/api/orders/{id}';
const GET_ORDER_PRODUCTS_URL  = '/api/orders/{id}/order-products';

export const getOrdersRequest = () => axios.get(GET_ORDERS_URL);

export const getOrderRequest = id => axios.get(
  replaceUrlPattern(GET_ORDER_URL, { id }));

export const createOrderRequest = ({ order, orderProducts }) => axios.post(
  CREATE_ORDER_URL,
  ({ ...order, orderProducts }));

export const updateOrderRequest = ({ id, order, orderProducts }) => axios.put(
  replaceUrlPattern(UPDATE_ORDER_URL, { id }),
  ({ ...order, orderProducts }));

export const deleteOrderRequest = id => axios.delete(
  replaceUrlPattern(DELETE_ORDER_URL, { id }));

export const getOrderProductsRequest = id => axios.get(
  replaceUrlPattern(GET_ORDER_PRODUCTS_URL, { id }));
