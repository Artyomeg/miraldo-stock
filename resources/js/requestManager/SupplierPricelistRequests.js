const GET_SUPPLIER_PRICE_LISTS_URL = '/api/supplier-pricelists';
const GET_SUPPLIER_PRICE_LIST_URL  = '/api/supplier-pricelists/{id}';

export const getSupplierPricelistsRequest = () => axios.get(GET_SUPPLIER_PRICE_LISTS_URL);

export const getSupplierPricelistRequest = id => {
  const getSupplierUrl = GET_SUPPLIER_PRICE_LIST_URL.replace(/{id}/g, id);
  return axios.get(getSupplierUrl);
}
