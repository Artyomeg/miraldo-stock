import { getMeta } from '@/helpers/template';

const GET_CURRENCIES_URL = '/api/settings';
const UPDATE_EURO_CURRENCY_URL = '/api/settings';
const DELETE_CURRENCIES_URL = '/api/settings';

export const getCurrenciesRequest = () => axios.get(GET_CURRENCIES_URL);

export const updateEuroCurrencyRequest = euroPrice => axios.post(
  UPDATE_EURO_CURRENCY_URL,
  {
    _method: 'PATCH',
    _token: getMeta('csrf-token'),
    dollar_price: euroPrice,
  });

export const deleteCurrenciesRequest = ids => axios.post(
  DELETE_CURRENCIES_URL,
  {
    _method: 'DELETE',
    _token: getMeta('csrf-token'),
    id: ids,
  });
