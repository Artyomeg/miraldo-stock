export const DOC_TYPES = {
  SUPPLIER_INVOICE: 'supplier-invoice',
  SUPPLY: 'supply',
  INVOICE: 'invoice',
  PAYMENT: 'payment',
}
