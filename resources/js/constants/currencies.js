// TODO: переделать в UPPERCASE
export const currencies = {
  RUBLE: 1,
  EURO: 2,
}

// TODO: переделать в UPPERCASE
export const currenciesWithoutRatio = [
  {
    id: currencies.RUBLE,
    name: 'Рубль',
    alias: 'руб.',
    system_name: 'ruble',
  },
  {
    id: currencies.EURO,
    name: 'Евро',
    alias: '€',
    system_name: 'euro',
  },
]
