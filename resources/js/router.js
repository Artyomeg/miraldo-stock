import Vue from 'vue';
import Router from 'vue-router';

import ordersRoutes from '@/featureSliced/areas/orders/routes';
import suppliersRoutes from '@/featureSliced/areas/suppliers/routes';
import suppliesRoutes from '@/featureSliced/areas/supplies/routes';
import clientPriceListsRoutes from '@/featureSliced/areas/clientPriceLists/routes';
import reportRoutes from '@/featureSliced/areas/reports/routes';
import logsRoutes from '@/featureSliced/areas/logs/routes';
import settingsRoutes from '@/featureSliced/areas/settings/routes';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // Заказы
    ...ordersRoutes,

    // Поставщики
    ...suppliersRoutes,
    // Поставки
    ...suppliesRoutes,

    // Прайслисты клиентов
    ...clientPriceListsRoutes,

    // Отчеты
    ...reportRoutes,

    // Логи
    ...logsRoutes,

    // Настройки
    ...settingsRoutes,
  ],
})
