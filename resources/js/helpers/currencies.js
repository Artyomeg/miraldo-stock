import { currenciesWithoutRatio } from '@/constants/currencies';

export const getCurrencyById = id => currenciesWithoutRatio.find(currency => currency.id === id);
