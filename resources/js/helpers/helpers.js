import { currencies } from '@/constants/currencies';

export function beautyNumber(number, {
  currencyId = null,
  showCurrency = false,
  round = false,
  fixedPrecision = null,
} = {}) {
  if (round) {
    number = Math.round(number)
  }

  let beautifulNumber = number.toLocaleString('ru', !!fixedPrecision
    ? {
      minimumFractionDigits: fixedPrecision,
      maximumFractionDigits: fixedPrecision
    }
    : undefined)

  if (showCurrency || currencyId || currencyId === '' ) {
    switch (+currencyId) {
      case currencies.RUBLE:
        beautifulNumber = `${beautifulNumber} руб.`;
        break;
      case currencies.EURO:
        beautifulNumber = `€${beautifulNumber}`;
        break;
      default:
        beautifulNumber = `${beautifulNumber} руб.`;
    }
  }

  return beautifulNumber;
}

export function beautyMoney(number, { round = 1, currencyId = null } = {}) {
  let beautifulMoney = roundTo(number, round).toLocaleString('ru')

  switch (currencyId) {
    // TODO: загнать в константы
    case currencies.RUBLE:
      beautifulMoney = `${beautifulMoney} руб.`
      break
    case currencies.EURO:
      beautifulMoney = `€${beautifulMoney}`
      break
    default:
      beautifulMoney = `${beautifulMoney} руб.`
  }

  return beautifulMoney;
}

export function beautyWeight(number, round = 1000) {
  return roundTo(number, round).toLocaleString('ru') + ' кг';
}

export function roundTo(num, precision = 1000) {
  return (+num < 0)
    ? Math.round((-num) * precision) / precision * -1
    : Math.round((+num) * precision) / precision;
}

export function filterToFloat(textValue, canBeNegative = false) {
  if (typeof textValue === 'number') {
    return textValue < 0 && !canBeNegative
      ? -textValue
      : textValue;
  }

  const firstReplace = canBeNegative
    ? textValue.replace(/[,]/g, '.')
    : textValue.replace(/[,-]/g, '.')

  return firstReplace
    .replace(/[^0-9.-]/g, '')
    .replace(/(\..*)\./g, '$1')
    .replace(/(?!^)-/g, '')
    .split('')
    .filter(function (item, pos, self) {
      return (item !== '.') || ((item === '.') && (self.indexOf(item) == pos));
    })
    .join('')
}

export function getBeautyRubleValue(value) {
  return beautyNumber(value, {
    round: true,
    currencyId: currencies.RUBLE,
  });
}
export function getBeautyEuroValue(value) {
  return beautyNumber(value, {
    round: false,
    currencyId: currencies.EURO,
    fixedPrecision: 2,
  });
}

export function getBeautyMoney(value, currencyId) {
  return currencyId === currencies.EURO
    ? getBeautyEuroValue(value)
    : getBeautyRubleValue(value);
}

export function getPathEntityId() {
  const pathname = window.location.pathname;
  const splittedPathname = pathname.split('/');
  return +splittedPathname[2];
}
