export const getProductBarcodeId = fullBarcode => fullBarcode.substring(0, 7);

export const getBarcodeWeight = (fullBarcode, kgChars = 2) => {
  const RAW_WEIGHT_CHARS_NUMBER = 6;

  // получим 6-символьную строку, содержащую вес товара
  const rawWeight = fullBarcode.substring(7, 13);
  const kilograms = rawWeight.substring(0, kgChars);
  const gramsString = rawWeight.substring(kgChars, RAW_WEIGHT_CHARS_NUMBER);
  const grams = kgChars === 2
    ? Math.floor(parseInt(gramsString, 10) / 10)
    : +gramsString;

  return parseInt(kilograms, 10) + grams / 1000;
}
