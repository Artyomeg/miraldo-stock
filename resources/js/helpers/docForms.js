import { roundTo } from '@/helpers/helpers';

export const reduceTotal = (products, trueUnit) => {
  const totalAmount = products.reduce((totalAmount, product) =>
    (product.unit === trueUnit)
      ? +totalAmount + +product.count
      : totalAmount,
    0
  );

  return roundTo(totalAmount);
}
