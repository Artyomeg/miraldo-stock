/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// тут ок, что require вместь import
require('./bootstrap');

import Vue from 'vue';
window.Vue = Vue;

import Vuex from 'vuex';
Vue.use(Vuex);
import store from './store/store';

import router from './router';

// multiselect
import Multiselect from 'vue-multiselect';
Vue.component('Multiselect', Multiselect);

// or import all icons if you don't care about bundle size
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
// globally (in your main .js file)
Vue.component('VIcon', Icon);


import dayjs from 'dayjs';
// тут ок, что require вместь import
dayjs.extend(require('dayjs/plugin/customParseFormat'));

// dayjs
Object.defineProperties(Vue.prototype, {
  $dayjs: {
    get() {
      return dayjs;
    },
  },
});


import underscore from 'vue-underscore';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import FloatNumber from '@/components/FloatNumber';Vue.component('FloatNumber', FloatNumber);
import FloatNumberView from '@/components/FloatNumberView';Vue.component('FloatNumberView', FloatNumberView);

// Глобальные
import PreloaderWrapper from '@/components/Helpers/PreloaderWrapper.vue'; Vue.component('PreloaderWrapper', PreloaderWrapper);
import ErrorMessageBox from '@/components/MessageBoxes/ErrorMessageBox.vue'; Vue.component('ErrorMessageBox', ErrorMessageBox);
import RevisionTable from '@/components/Reports/Revision/RevisionTable.vue'; Vue.component('RevisionTable', RevisionTable);

import ReportByProductsTable from '@/components/Reports/ReportByProductsTable'; Vue.component('ReportByProductsTable', ReportByProductsTable);
import ReportProductsMoving from '@/components/Reports/ReportProductsMoving'; Vue.component('ReportProductsMoving', ReportProductsMoving);

// Неразобранные
import InvoiceForm from '@/components/Invoices/InvoiceForm'; Vue.component('InvoiceForm', InvoiceForm);
import Clients from '@/components/Clients/Clients'; Vue.component('Clients', Clients);
import Products from '@/featureSliced/areas/products/products/components/Products'; Vue.component('Products', Products);
import ClientPricelists from '@/featureSliced/areas/clientPriceLists/clientPricelists/components/ClientPricelists'; Vue.component('ClientPricelists', ClientPricelists);
import SupplierPricelists from '@/components/Pricelists/SupplierPricelists'; Vue.component('SupplierPricelists', SupplierPricelists);


import { ToggleButton } from 'vue-js-toggle-button';
Vue.component('ToggleButton', ToggleButton);

Vue.use(underscore);

// import vuetify from '@/plugins/vuetify' // path to vuetify export

const appId = document.getElementById('app');

const app = new Vue({
  // el: '#app',
  // vuetify,
  store,
  router,
  data: {
    globalVar: {
      clients: [],
      CSRFToken: '',
      language: 'ru',
      currencyId: null,
    },
    errors: [],
  },
  methods: {
    // TODO: понять, почему этот метод лежит тут,
    // и либо переиспользовать, либо вернуть в шаблон, в котором он используется
    getDaysForCalendarFields() {
      // 1 set today - to
      var today = new Date(),
        dates = {};

      dates['currentDay'] = today.getFullYear()
        + '-' + ((today.getMonth() + 1 < 10) ? "0" : "") + (today.getMonth() + 1)
        + '-' + ((today.getDate() < 10) ? "0" : "") + today.getDate();

      // 2 set 'from' - first day of this month
      dates['firstDayOfCurrentMonth'] = today.getFullYear()
        + '-' + ((today.getMonth() + 1 < 10) ? "0" : "") + (today.getMonth() + 1)
        + '-01';

      return dates;
    },

    changeCurrency(event) {
      this.globalVar.currencyId = event.target.value
    },
  },

  mounted() {
    if (this.$root.$refs.currency) {
      this.globalVar.currencyId = this.$root.$refs.currency.value;
    }
  },
}).$mount(appId);

// other
// $('.datepicker').datepicker();
