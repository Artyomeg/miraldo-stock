declare namespace App.Models {
  export interface ProductPrice {
    id: number;
    client_pricelist_id: number;
    name: string;
    price: string;
    product_id: number,
    product: App.Models.Product;
  }

  export interface Product {
    id: number;
    barcode: number | null;
    barcode_kg_chars: string;
    count: number;
    name: string;
    unit: "кг" | "шт";
  }

  export interface ClientPriceList {
    id: number;
    name: string;
    assigned_to_euro: number;
  }
  export interface Setting {
    id: number;
    value: string;
    created_at: number;
    deleted_at: number;
  }
}
