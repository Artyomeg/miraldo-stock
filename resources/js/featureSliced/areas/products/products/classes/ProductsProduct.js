import Product from '@/classes/Product';

class ProductsProduct extends Product {
  get supplierCodeWithName() {
    return `${this.supplierCode} ${this.name}`;
  }
}

export default ProductsProduct;
