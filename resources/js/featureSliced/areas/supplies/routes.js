import CreateSupply from '@/views/Supplies/CreateSupply';
import EditSupply from '@/views/Supplies/EditSupply';

export default [
  {
    path: '/supplies/create',
    name: 'supplies-create',
    component: CreateSupply,
  },
  {
    path: '/supplies/:id/edit',
    name: 'supplies-edit',
    component: EditSupply,
  },
]
