import SettingsEuroPriceEdit from '@/featureSliced/areas/settings/settingsEuroPriceEdit';

export default {
  path: '/settings/euro-price/edit',
  name: 'settings-euro-price-edit',
  component: SettingsEuroPriceEdit,
  // meta: {
  //   title: 'ClientPriceListsDuplicate',
  // },
}
