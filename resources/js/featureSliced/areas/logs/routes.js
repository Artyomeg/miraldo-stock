import LogsLogin from '@/featureSliced/areas/logs/logsLogin';

export default [
  {
    path: '/logs/login',
    name: 'logs-login',
    component: LogsLogin,
  },
]
