import ReportRevisionBySuppliers from '@/views/Reports/ReportRevisionBySuppliers';
import ReportBySupplies from '@/views/Reports/ReportBySupplies';
import ReportByClientPayments from '@/views/Reports/ReportByClientPayments';
import EditSupplierInvoice from '@/views/SupplierInvoices/EditSupplierInvoice';

export default [
  {
    path: '/reports/revision-by-suppliers',
    name: 'report-by-suppliers',
    component: ReportRevisionBySuppliers,
    meta: {
      title: 'ReportRevisionBySuppliers',
    },
  },
  {
    path: '/reports/by-supplies',
    name: 'report-by-supplies',
    component: ReportBySupplies,
    meta: {
      title: 'ReportBySupplies',
    },
  },
  {
    path: '/reports/by-client-payments',
    name: 'report-by-client-payments',
    component: ReportByClientPayments,
    meta: {
      title: 'ReportByClientPayments',
    },
  },

  {
    path: '/supplier-invoices/:id/edit',
    name: 'supplier-invoices-edit',
    component: EditSupplierInvoice,
  },
]
