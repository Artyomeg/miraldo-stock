import Suppliers from '@/views/Suppliers/Suppliers';
import CreateSupplier from '@/views/Suppliers/CreateSupplier';
import EditSupplier from '@/views/Suppliers/EditSupplier';

export default [
  {
    path: '/suppliers',
    name: 'suppliers',
    component: Suppliers,
    meta: {
      title: 'Suppliers',
    },
  },
  {
    path: '/suppliers/create',
    name: 'suppliers-create',
    component: CreateSupplier,
  },
  {
    path: '/suppliers/:id/edit',
    name: 'suppliers-edit',
    component: EditSupplier,
  },
]
