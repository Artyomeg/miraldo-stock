import ClientPriceListsDuplicate from '@/featureSliced/areas/clientPriceLists/clientPriceListsDuplicate/components/ClientPriceListsDuplicate';

export default {
  path: '/client-pricelists/:id/duplicate',
  name: 'client-price-lists-duplicate',
  component: ClientPriceListsDuplicate,
  meta: {
    title: 'ClientPriceListsDuplicate',
  },
}
