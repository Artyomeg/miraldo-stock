import ClientPricelists from '@/featureSliced/areas/clientPriceLists/clientPricelists/components/ClientPricelists';

export default {
  path: '/client-pricelists',
  name: 'client-price-lists',
  component: ClientPricelists,
  meta: {
    title: 'ClientPricelists',
  },
}
