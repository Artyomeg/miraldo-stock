import clientPriceListsRoute from '@/featureSliced/areas/clientPriceLists/clientPricelists/route';
import clientPriceListsDuplicateRoute from '@/featureSliced/areas/clientPriceLists/clientPriceListsDuplicate/route';

export default [
  clientPriceListsRoute,
  clientPriceListsDuplicateRoute,
]
