import Orders from '@/views/Orders/Orders';
import CreateOrder from '@/views/Orders/CreateOrder';
import EditOrder from '@/views/Orders/EditOrder';

export default [
  {
    path: '/orders',
    name: 'orders',
    component: Orders,
    meta: {
      title: 'Orders',
    },
  },
  {
    path: '/orders/create',
    name: 'orders-create',
    component: CreateOrder,
  },
  {
    path: '/orders/:id/edit',
    name: 'orders-edit',
    component: EditOrder,
  },
]
