import { getProductsRequest, getProductPricesRequest, getProductWithSupplyPricesRequest } from '@/requestManager/ProductRequests';
import SupplierPricelist from '@/classes/SupplierPricelist';

const state = {
  productsInited: false,
  products: [],
}

const getters = {
  getProduct: state => id => {
    return state.products.find(product => product.id === id);
  },
}

const mutations = {
  SET_PRODUCTS(state, products) {
    state.products = products;
  },

  SET_PRODUCTS_NOT_INITIALIZED(state) {
    state.productsInited = false;
  },
  SET_PRODUCTS_INITIALIZED(state) {
    state.productsInited = true;
  },
}

const actions = {
  async loadProducts({ commit }) {
    commit('SET_PRODUCTS_NOT_INITIALIZED');
    const response = await getProductsRequest();

    // // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data.data) {
      return false;
    }
    const products = response.data.data;

    commit('SET_PRODUCTS', products);
    commit('SET_PRODUCTS_INITIALIZED');
    return products;
  },
  async loadProductsWithInvoicePrices({ commit }) {
    commit('SET_PRODUCTS_NOT_INITIALIZED');
    const response = await getProductPricesRequest();

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    const products = response.data.data.map(
      loopSupplierPricelist => new SupplierPricelist(loopSupplierPricelist)
    );

    commit('SET_PRODUCTS', products);
    return products;
  },
  async loadProductsWithSupplyPrices({ commit }) {
    commit('SET_PRODUCTS_NOT_INITIALIZED');
    const response = await getProductWithSupplyPricesRequest();

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    const products = response.data.data.map(
      loopSupplierPricelist => new SupplierPricelist(loopSupplierPricelist)
    );

    commit('SET_PRODUCTS', products);
    commit('SET_PRODUCTS_INITIALIZED');
    return products;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
