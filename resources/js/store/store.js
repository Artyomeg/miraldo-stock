import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import clients from './clients';
import clientPricelists from './clientPricelists';

import products from './products';

import suppliers from './suppliers';
import supplierPricelists from './supplierPricelists';

import supplies from './supplies';
import supplyProducts from './supplyProducts';

export default new Vuex.Store({
  modules: {
    clients,
    clientPricelists,

    suppliers,
    supplierPricelists,

    supplies,
    supplyProducts,

    products,
  }
})
