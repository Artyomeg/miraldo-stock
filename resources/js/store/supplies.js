import { getSuppliesRequest, deleteSupplyRequest } from '@/requestManager/SupplyRequests';
import Supply from '@/classes/Supply';

const state = {
  suppliesInitialized: false,
  supplies: [],
}

const getters = {
  getSupply: state => id => {
    return state.supplies.find(supply => supply.id === id);
  },
}

const mutations = {
  SET_SUPPLIES(state, supplies) {
    state.supplies = supplies;
  },
  SET_SUPPLIES_INITIALIZED(state) {
    state.suppliesInitialized = true;
  },

  CREATE_SUPPLY(state, supply) {
    state.supplies.push(supply);
  },
  UPDATE_SUPPLY(state, { supply, forgetDifference = false }) {
    const currentSupplyIndex = state.supplies.findIndex(loopSupply => loopSupply.id === supply.id);

    if (currentSupplyIndex === -1) {
      state.supplies.push(supply);
      return;
    }

    const updatedSupply = forgetDifference
      ? supply
      : {
        ...state.supplies[currentSupplyIndex],
        ...supply,
      }

    state.supplies.splice(currentSupplyIndex, 1, updatedSupply);
  },
  DELETE_SUPPLY(state, id) {
    const currentSupplyIndex = state.supplies.findIndex(loopSupply => loopSupply.id === id);
    state.supplies.splice(currentSupplyIndex, 1);
  },
}

const actions = {
  async deleteSupply({ state, commit }, id) {
    const response = await deleteSupplyRequest(id);
    commit('DELETE_SUPPLY', id);

    return response;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
