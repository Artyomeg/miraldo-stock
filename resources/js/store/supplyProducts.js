import { getSupplyProductsRequest } from '@/requestManager/SupplyRequests';
import SupplyProduct from '@/classes/supplyProduct';

const state = {
  supplyProductsInitialized: false,
  supplyProducts: [],
}

const mutations = {
  SET_SUPPLIERPRODUCTS_INITIALIZED(state, initStatus) {
    state.supplyProductsInitialized = initStatus;
  },

  CLEAR_SUPPLY_PRODUCTS(state) {
    state.supplyProducts = [];
  },
  SET_SUPPLY_PRODUCTS(state, supplies) {
    state.supplyProducts = supplies;
  },

  CREATE_SUPPLY_PRODUCT(state, supplier) {
    state.supplyProducts.push(supplier);
  },
  UPDATE_SUPPLY_PRODUCT(state, supplyProduct) {
    const currentSupplyProductIndex = state.supplyProducts.findIndex(loopSupplyProduct => loopSupply.id === supplier.id);

    if (currentSupplyIndex === -1) {
      state.supplies.push(supplyProduct);
      return;
    }

    state.supplies.splice(currentSupplyIndex, 1, supplyProduct);
  },
}

const actions = {
  /**
   * Если `force` === false, то при загруженных данных не будем прогружать новые.
   * @param state
   * @param commit
   * @param shouldRewrite
   * @returns {Promise<boolean|[]|*>}
   */
  async loadSupplyProducts({ state, commit }, supplyId) {
    commit('SET_SUPPLIERPRODUCTS_INITIALIZED', false);
    const response = await getSupplyProductsRequest(supplyId);

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    const supplyProducts = response.data.data.map(dbSupplyProduct =>
      new SupplyProduct({
        ...dbSupplyProduct,
        oldProductId: dbSupplyProduct.productId,
        oldProductCount: dbSupplyProduct.count,
      })
    );
    commit('SET_SUPPLY_PRODUCTS', supplyProducts);
    commit('SET_SUPPLIERPRODUCTS_INITIALIZED', true);

    return supplyProducts;
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
