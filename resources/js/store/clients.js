import { getClientsRequest } from '@/requestManager/ClientRequests';

const state = {
  clientsInited: false,
  clients: [],
}

const getters = {
  getClient: state => id => {
    return state.clients.find(client => client.id === id);
  },
}
const mutations = {
  REMOVE_CLIENT(state, id) {
    state.clients = state.clients.filter(
      client => client.id !== id
    );
  },
  SET_CLIENTS(state, clients) {
    state.clients = clients;
  },
  SET_CLIENTS_INITIALIZED(state) {
    state.clientsInited = true;
  },
}

const actions = {
  async loadClients({ commit }, onlyNotArchived) {
    const response = await getClientsRequest(onlyNotArchived);

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    const clients = response.data.data;
    commit('SET_CLIENTS', clients);
    commit('SET_CLIENTS_INITIALIZED');

    return clients;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
