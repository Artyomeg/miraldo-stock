import { getSuppliersRequest, deleteSupplierRequest } from '@/requestManager/SupplierRequests';

const state = {
  suppliersInitialized: false,
  suppliers: [],
}

const getters = {
  getSupplier: state => id => {
    return state.suppliers.find(supplier => supplier.id === id);
  },
}

const mutations = {
  SET_SUPPLIERS(state, suppliers) {
    state.suppliers = suppliers;
  },
  SET_SUPPLIERS_INITIALIZED(state) {
    state.suppliersInitialized = true;
  },

  CREATE_SUPPLIER(state, supplier) {
    state.suppliers.push(supplier);
  },
  UPDATE_SUPPLIER(state, { supplier, forgetDifference = false }) {
    const currentSupplierIndex = state.suppliers.findIndex(loopSupplier => loopSupplier.id === supplier.id);

    if (currentSupplierIndex === -1) {
      state.suppliers.push(supplier);
      return;
    }

    const updatedSupplier = forgetDifference
      ? supplier
      : {
        ...state.suppliers[currentSupplierIndex],
        ...supplier,
      }

    state.suppliers.splice(currentSupplierIndex, 1, updatedSupplier);
  },
  DELETE_SUPPLIER(state, id) {
    const currentSupplierIndex = state.suppliers.findIndex(loopSupplier => loopSupplier.id === id);
    state.suppliers.splice(currentSupplierIndex, 1);
  },
}

const actions = {
  /**
   * Если `force` === false, то при загруженных данных не будем прогружать новые.
   * @param state
   * @param commit
   * @param shouldRewrite
   * @returns {Promise<boolean|[]|*>}
   */
  async loadSuppliers({ state, commit }, shouldRewrite = false) {
    if (!shouldRewrite && state.suppliersInitialized) {
      return state.suppliers;
    }

    let response = await getSuppliersRequest();

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    let suppliers = response.data.data;
    commit('SET_SUPPLIERS', suppliers);

    if (!shouldRewrite) {
      commit('SET_SUPPLIERS_INITIALIZED');
    }

    return suppliers;
  },
  async deleteSupplier({ state, commit }, id) {
    const response = await deleteSupplierRequest(id);
    commit('DELETE_SUPPLIER', id);

    return response;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
