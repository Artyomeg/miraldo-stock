import { _ } from 'vue-underscore';
import SupplierPricelist from '@/classes/SupplierPricelist';

const GET_PRICELISTS_URL = '/api/supplier-pricelists';

const state = {
  // supplierPricelistsInited: false,
  supplierPricelists: [],
}

const getters = {}

const mutations = {
  SET_SUPPLIER_PRICELISTS(state, supplierPricelists) {
    state.supplierPricelists = supplierPricelists;
  },
}

const actions = {
  async loadSupplierPricelists({ state, commit }) {
    let response = await axios.get(GET_PRICELISTS_URL);

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    let supplierPricelists = response.data.data.map(
      loopSupplierPricelist => new SupplierPricelist(loopSupplierPricelist)
    );

    supplierPricelists = _.sortBy(supplierPricelists, 'name');
    commit('SET_SUPPLIER_PRICELISTS', supplierPricelists);
    return supplierPricelists;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
