import { _ } from 'vue-underscore';

const GET_PRICELISTS_URL = '/api/client-pricelists';
// const DELETE_PRICELIST_URL = '/client-pricelists/{id}';

const state = {
  // clientPricelistsInited: false,
  clientPricelists: [],
}

const getters = {}

const mutations = {
  SET_CLIENT_PRICELISTS(state, clientPricelists) {
    state.clientPricelists = clientPricelists;
  },
}

const actions = {
  async loadClientPricelists({ commit }) {
    const response = await axios.get(GET_PRICELISTS_URL);

    // TODO: подтянуть babel7 и заменить на `!(response?.data?.data)`
    if (!response || !response.data || !response.data.data) {
      return false;
    }

    let clientPricelists = response.data.data;
    clientPricelists = _.sortBy(clientPricelists, 'name');
    commit('SET_CLIENT_PRICELISTS', clientPricelists);
    return clientPricelists;
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
