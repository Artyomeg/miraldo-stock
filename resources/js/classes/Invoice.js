import DocumentModel from './DocumentModel'

class Invoice {
  constructor(params = {}) {
    Object.keys(params).forEach(index => {
      this[index] = params[index];
    })
  }
}

export default Invoice;
