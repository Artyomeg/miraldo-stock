import DocumentModel from './DocumentModel';

class SupplierInvoice extends DocumentModel {
  constructor(params = {
    date: null,
    supplierId: null,
    currencyId: null,
    invoiceNumber: null,
    invoiceDate: null,
    comment: '',
  }) {
    super(params);
  }
}

export default SupplierInvoice;
