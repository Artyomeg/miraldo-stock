import Model from './Model';

class OrderProduct extends Model {
  constructor(params) {
    super(params)
  }

  fieldName(field, i) {
    let prefix = 'InvoiceProductsForm[' + this.oldNew + ']';

    // если новый - выводим i счетчика, иначе - id записи
    prefix += (this.oldNew === 'new')
      ? '[' + i + ']'
      : '[' + this.id + ']';

    return prefix + '[' + field + ']';
  }

  get oldNew() {
    return (this.orderId && this.productId)
      ? 'old'
      : 'new'
  }
}

export default OrderProduct
