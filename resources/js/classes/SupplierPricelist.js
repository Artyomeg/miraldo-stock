import Model from './Model'

class SupplierPricelist extends Model {
  constructor(params = {
    name: '',
    assigned_to_euro: false,
  }) {
    super({
      ...params,
    })
  }
}

export default SupplierPricelist;
