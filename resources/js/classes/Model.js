import { camelCase } from 'lodash';

class Model {
  constructor(params = {}) {
    Object.keys(params).forEach(param => {
      let camelCasedParam = camelCase(param);
      this[camelCasedParam] = params[param];
    })
  }
}

export default Model;
