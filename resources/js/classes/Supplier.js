import Model from './Model';

class Supplier extends Model {
  constructor(params = {
    name: '',
    phone: '',
    address: '',
    currencyId: undefined,
    supplierPricelistId: undefined,
  }) {
    super(params)
  }
}

export default Supplier;
