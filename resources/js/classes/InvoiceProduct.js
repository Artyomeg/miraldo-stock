import Model from './Model';

class InvoiceProduct extends Model {
  constructor(params) {
    super(params)
  }

  fieldName(field, i) {
    let prefix = 'InvoiceProductsForm[' + this.oldNew + ']';

    // если новый - выводим i счетчика, иначе - id записи
    prefix += (this.oldNew === 'new')
      ? '[' + i + ']'
      : '[' + this.id + ']';

    return prefix + '[' + field + ']';
  }

  get oldNew() {
    return (this.invoiceId && this.productId)
      ? 'old'
      : 'new';
  }
}

const getSum = function () {
  let lineSum = parseFloat(this.count)
    * parseFloat(this.price)
    * (100 - parseFloat(this.discount)) / 100;

  return isNaN(lineSum)
    ? 0
    : Math.round(lineSum);
}

InvoiceProduct.prototype.getSum = getSum;

export default InvoiceProduct;
