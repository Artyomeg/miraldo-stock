class SupplyProduct {
  defaultModel = {
    id: null,
    productId: null,
    name: '',
    price: '',
    count: '',
    markForDeletion: false,
    product: null,
    unit: null,
  }

  constructor(params = {}) {
    const modelFields = {
      ...this.defaultModel,
      ...params,
    }

    Object.keys(modelFields).forEach(index => {
      this[index] = modelFields[index];
    });
  }

  fieldName(field, i) {
    let prefix = 'SupplyProductsForm[' + this.oldNew + ']';

    // если новый - выводим i счетчика, иначе - id записи
    prefix += (this.oldNew === 'new')
      ? '[' + i + ']'
      : '[' + this.id + ']';

    return prefix + '[' + field + ']';
  }

  get oldNew() {
    return (this.supplyId && this.productId)
      ? 'old'
      : 'new';
  }

  get sum() {
    let lineSum = parseFloat(this.count) * parseFloat(this.price)

    if (this.discount !== undefined) {
      lineSum *= (100 - parseFloat(this.discount)) / 100;
    }

    return isNaN(lineSum)
      ? 0
      : Math.round(lineSum);
  }
  get sumWithPrecision() {
    let lineSum = parseFloat(this.count) * parseFloat(this.price)

    if (this.discount !== undefined) {
      lineSum *= (100 - parseFloat(this.discount)) / 100;
    }

    return isNaN(lineSum)
      ? 0
      : Math.round(lineSum * 100) / 100;
  }
}

const getSum = function () {
  let lineSum = parseFloat(this.count)
    * parseFloat(this.price)
    * (100 - parseFloat(this.discount)) / 100;

  return isNaN(lineSum)
    ? 0
    : Math.round(lineSum);
}

SupplyProduct.prototype.getSum = getSum;

export default SupplyProduct;
