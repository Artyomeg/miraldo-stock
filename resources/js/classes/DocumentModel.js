import Model from './Model';

class DocumentModel extends Model {
  constructor(params = {}) {
    super(params);
  }
}

export default DocumentModel;
