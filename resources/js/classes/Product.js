// import Model from './Model';

class Product {
  constructor(params = {}) {
    Object.keys(params).forEach(index => {
      this[index] = params[index];
    })
  }
}

export default Product;
