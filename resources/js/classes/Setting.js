import Model from '@/classes/Model';

class Setting extends Model {
  constructor(params = {
    id: null,
    value: null,
    created_at: null,
    deleted_at: null,
  }) {
    super(params);
  }
}

export default Setting;
