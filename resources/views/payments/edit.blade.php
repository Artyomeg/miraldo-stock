@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Редактирование оплаты №{{ $payment->id }}</h2>
      </div>
      <div class="mb-3">
        @if(isset($payment->client_id))
          <a
            class="btn btn-primary"
            href="{{ route('clients.payments', $payment->client_id) }}"
          >Назад</a>
        @endif
        {!! Form::open([
             'method' => 'DELETE',
             'route'  => ['payments.destroy', $payment->id],
             'style'  => 'display: inline'
        ]) !!}
        <button
          onclick="if (confirm('Хотите удалить этот элемент?')) this.form.submit(); else return false;"
          class="btn btn-danger"
        >
          <v-icon name='trash'></v-icon>
        </button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      {{-- TODO: сделать что-то чтоб поддерживались 2 языка --}}
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::model($payment, ['method' => 'PATCH', 'route' => ['payments.update', $payment->id]]) !!}
    @include('payments.form')
  {!! Form::close() !!}
@endsection
