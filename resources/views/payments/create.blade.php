@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Создание оплаты</h2>
      </div>
      <p class="pull-right">
        <a
          href="{{ route('clients.payments') }}"
          class="btn btn-primary"
        >Назад</a>
      </p>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::open(['route' => 'payments.store', 'method' => 'POST']) !!}
    @include('payments.form')
  {!! Form::close() !!}
@endsection
