<div class="row">
  {{-- Поставщик --}}
  <div class="col-md-4">
    @if($payment->client_id)
      <div class="form-group">
        <strong>Клиент</strong>
        <input
          class="form-control"
          disabled="disabled"
          type="text"
          value="{{ $payment->client->name }}"
        >
        {!! Form::hidden('client_id', $payment->client_id) !!}
      </div>
    @elseif($payment->supplier_id)
      <div class="form-group">
        <strong>Поставщик</strong>
        <input
          class="form-control"
          disabled="disabled"
          type="text"
          value="{{ $payment->supplier->name }}"
        >
        {!! Form::hidden('client_id', $payment->supplier_id) !!}
      </div>
    @endif
  </div>

  {{-- Дата --}}
  <div class="col-md-3">
    <div class="form-group">
      <strong>Дата</strong>
      <input
        class="form-control"
        name="date"
        type="date"
        value="{{ $payment ? $payment->date : new \DateTime() }}"
      />
    </div>
  </div>

  {{-- Сумма --}}
  <div class="col-md-3">
    <div class="form-group">
      <strong>Сумма</strong>
      <float-number-view
        class="form-control"
        name="amount"
        required
        negative
        customvalue="{{ $payment->amount }}"
      ></float-number-view>
    </div>
  </div>

  {{-- Валюта --}}
  <div class="col-md-2">
    <div class="form-group">
      <strong>Валюта</strong>
      {!! Form::select('currency_id', $currencies, null, [
        'class' => 'form-control',
        'placeholder' => '',
        'ref' => 'currency',
        'required' => '',
        '@change' => 'changeCurrency($event)'
      ]) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <strong>Комментарий</strong>
      {!! Form::text('comment', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" class="btn btn-primary">Сохранить</button>
  </div>
</div>
