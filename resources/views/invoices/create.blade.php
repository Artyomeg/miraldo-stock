@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Создание счета</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary"
           href="{{ route('invoices.index') }}">Назад</a>
      </p>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <error-message-box :errors="errors"></error-message-box>

  {!! Form::open(['route' => 'invoices.store', 'method' => 'POST']) !!}
    <invoice-form />
  {!! Form::close() !!}
@endsection
