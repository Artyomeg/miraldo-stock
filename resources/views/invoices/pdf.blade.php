@php
  /** @var \App\Models\Invoice $invoice */
  $fmt = new NumberFormatter( 'ru_RU', NumberFormatter::CURRENCY );
@endphp

@extends('layouts.pdf')

@section('content')
  <h2>Счет №{{ $invoice->number }}</h2>

  <table>
    <tbody>
      <tr>
        <td style="border-width: 0; padding-left: 0; padding-right: 2rem;">
          <strong>Клиент</strong>
          <div>{{ $invoice->client->name }}</div>
        </td>
        <td style="border-width: 0; padding-right: 2rem;">
          <strong>Адрес клиента</strong>
          <div>{{ $invoice->client->address }}</div>
        </td>
        <td style="border-width: 0;">
          <strong>Телефон клиента</strong>
          <div>{{ $invoice->client->phone }}</div>
        </td>
      </tr>
    </tbody>
  </table>
  <br />

  <table>
    <tbody>
      <tr>
        <td style="border-width: 0; padding-left: 0; padding-right: 2rem;">
          <strong>Дата</strong>
          <div>{{ date('d.m.Y', strtotime($invoice->date)) }}</div>
        </td>
        <td style="border-width: 0;">
          <strong>Сумма</strong>
          <div>{{ $fmt->formatCurrency($invoice->amount, 'RUR') }}</div>
          <div>{{ $invoice->client->cashless ? 'Безналичный расчет' : 'Наличный расчет' }}</div>
        </td>
      </tr>
    </tbody>
  </table>
  <br />

  <table style="width: 100%;">
    <thead>
      <tr>
        <th>#</th>
        <th>Товар</th>
        <th>Кол-во</th>
        <th>Цена</th>
        <th>Скидка</th>
        <th>Сумма</th>
      </tr>
    </thead>
    <tbody>
      @forelse($invoice->invoiceProducts as $i => $invoice_product)
        <tr>
          <td style="text-align: right;">{{ $i + 1 }}</td>
          <td>{{ $invoice_product->product->name }}</td>
          <td style="white-space: nowrap; text-align: center;">
            {{ round($invoice_product->count, 2) }}
            {{ $invoice_product->product->unit }}
          </td>
          <td style="white-space: nowrap; text-align: right;">
            {{ $fmt->formatCurrency($invoice_product->price, 'RUR') }}
          </td>
          <td style="text-align: center;">
            @if($invoice_product->discount > 0)
              {{ $invoice_product->discount }}%
            @endif
          </td>
          <td style="white-space: nowrap; text-align: right;">
            {{ $fmt->formatCurrency($invoice_product->total, 'RUR') }}
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="6" style="text-align: center;">
            Нет товаров
          </td>
        </tr>
      @endforelse
    </tbody>
  </table>
  <br />

  <table style="width: 100%;">
    <tbody>
      <tr>
        <td style="border-width: 0; padding-left: 0;">
          <strong>Долг по клиенту</strong>
          <div>{{ $fmt->formatCurrency($invoice->client->debt, 'RUR') }}</div>
        </td>
        <td style="border-width: 0; text-align: right; vertical-align: bottom;">Подпись</td>
        <td style="border-width: 0; width: 200px; vertical-align: bottom; padding-right: 0;">
          <div style="border-bottom: solid 1px #444;"></div>
        </td>
      </tr>
    </tbody>
  </table>

  @if($invoice->comment)
    <br />
    <strong>Комментарий</strong>
    <div>{{ nl2br($invoice->comment) }}</div>
  @endif
@endsection
