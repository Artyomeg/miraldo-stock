@extends('layouts.app')

@section('content')
  <div class="g-technical-info__client_id" style="display: none">
    {{ $invoice->client_id }}
  </div>

  <div class="g-technical-info__invoice_date" style="display: none">
    {{ isset($invoice) ? $invoice->date : date('Y-m-d')}}
  </div>

  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>№ {{ $invoice->number }}</h2>
      </div>

      <p class="pull-right">
        <a href="{{ route('invoices.index') }}" class="btn btn-primary">
          Назад
        </a>
      </p>
    </div>
  </div>

  {{ Form::open(['method' => 'PATCH', 'route' => ['invoices.update', $invoice->id]]) }}
    <invoice-form :id="{{ $invoice->id }}" />
  {{ Form::close() }}
@endsection
