@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h2>Счет</h2>
    </div>
    <p class="pull-right">
      <a
        href="{{ route('invoices.create') }}"
        class="btn btn-success"
      >Добавить счет</a>
    </p>
  </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif

<table class="table table-striped">
  <thead>
    <tr>
      <th>№</th>
      <th>Клиент</th>
      <th>Дата</th>
      <th>Сумма</th>
      <th style="width: 160px;">Действие</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($invoices as $invoice)
      <tr>
        <td>{{ $invoice->number }}</td>
        <td>{{ $invoice->client->name }}</td>
        <td>{{ date("d.m.Y", strtotime($invoice->date)) }}</td>
        <td>{{ fineNumber($invoice->amount, false) . ' руб.' }}</td>
        <td>
          <a class="btn btn-primary" href="{{ route('api.invoices.download', $invoice->id) }}">
            <v-icon name='download'></v-icon>
          </a>
          <a class="btn btn-primary" href="{{ route('invoices.edit', $invoice->id) }}">
            <v-icon name='pen'></v-icon>
          </a>
          {!! Form::open([
            'method' => 'DELETE',
            'route'  => ['invoices.destroy', $invoice->id],
            'style'  => 'display:inline'
          ]) !!}
          <button
            onclick="if (confirm('Хотите удалить этот элемент?')){ this.form.submit(); } else { return false; }"
            class="btn btn-danger"
          >
            <v-icon name='trash'></v-icon>
          </button>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach

    @if(!count($invoices))
      <tr>
        <td colspan="5">Счетов нет</td>
      </tr>
    @endif
  </tbody>
</table>

{!! $invoices->links() !!}
@endsection
