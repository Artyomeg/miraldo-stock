@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 mb-3">
      <div class="pull-left">
        <h2>Клиент "{{ $client->name }}"</h2>
      </div>

      <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('clients.index') }}">
          <v-icon name='arrow-left'></v-icon>
        &nbsp;&nbsp;Назад
        </a>
        <a class="btn btn-primary" href="{{ route('clients.edit', $client->id) }}">
          <v-icon name='pen'></v-icon>
        </a>
      </div>
    </div>
  </div>

  @include('clients.payments__client-details')
  @include('clients.payments__payments')
@endsection
