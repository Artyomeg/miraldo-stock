{{-- !!! Комментим это, потому что не работает --}}
{{--<p class="pull-right">--}}
{{--  <a--}}
{{--    :class="globalVar.showAddingPayment ? 'btn btn-primary' : 'btn btn-success'"--}}
{{--    href="javascript: void(0)"--}}
{{--    @click="globalVar.showAddingPayment = !globalVar.showAddingPayment"--}}
{{--  >--}}
{{--    <template v-if="globalVar.showAddingPayment">Скрыть добавление оплаты</template>--}}
{{--    <template v-else>@lang('payments.buttons.add')</template>--}}
{{--  </a>--}}
{{--</p>--}}

{!! Form::open(['route' => 'payments.store', 'method' => 'POST']) !!}
{{--    <div--}}
{{--      v-if="globalVar.showAddingPayment"--}}
{{--      class="row border pt-3 pb-2 mb-2 b-payment-adding"--}}
{{--    >--}}
{{--        <div class="col-md-12">--}}
{{--            <h3>Добавление оплаты</h3>--}}
{{--        </div>--}}

{{--        <div class="col-md-6">--}}
{{--            <div class="form-group">--}}
{{--                <strong>@lang('payments.fields.date'):</strong>--}}
{{--                {{ Form::date('date', new \DateTime(), ['class' => 'form-control']) }}--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-md-6">--}}
{{--            <div class="form-group">--}}
{{--                <strong>@lang('payments.fields.amount'):</strong>--}}
{{--                <float-number-view--}}
{{--                    class="form-control"--}}
{{--                    name="amount"--}}
{{--                    required="required"--}}
{{--                ></float-number-view>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <strong>@lang('payments.fields.comment'):</strong>--}}
{{--                {!! Form::text('comment', null, ['placeholder' => '', 'class' => 'form-control']) !!}--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-md-12 text-center">--}}
{{--            <button type="submit" class="btn btn-primary">@lang('elements.buttons.save')</button>--}}
{{--        </div>--}}
{{--    </div>--}}

    {!! Form::hidden('client_id', $client->id) !!}
{!! Form::close() !!}
