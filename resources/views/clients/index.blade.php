@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Клиенты</h2>
            </div>
            <p class="pull-right">
                <a
                    class="btn btn-success"
                    href="{{ route('clients.create') }}"
                >Добавить клиента</a>
            </p>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <clients></clients>
@endsection
