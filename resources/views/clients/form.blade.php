<div class="row">
  <div class="col-md-5">
    <div class="form-group">
      <strong>Имя</strong>
      {!! Form::text('name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <strong>Телефон</strong>
      {!! Form::text('phone', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>

  <div class="col-md-5">
    <div class="form-group">
      <strong>
        @if(isset($client) && $client->client_pricelist_id)
          <a
            href="/client-pricelists/{{ $client->client_pricelist_id }}"
          >Прайслист</a>
        @else
          Прайслист
        @endif
      </strong>
      {!! Form::select('client_pricelist_id', $pricelists, null, ['placeholder' => '', 'class' => 'form-control', 'required' => '']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-5">
    <div class="form-group">
      <strong>Адрес</strong>
      {!! Form::text('address', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>
  <div class="col-md-5">
    <div style="padding-top: 2rem" class="form-group d-flex">
      {!! Form::checkbox('cashless', null, null, [
          'style' => "height: 18px; width: 18px;",
          'id' => 'cashless-field',
        ]) !!}
      <label class="ml-2" for="cashless-field">
        <strong>Безналичный расчет</strong>
      </label>
    </div>
  </div>
  <div class="col-md-2">
    <div style="padding-top: 2rem" class="form-group d-flex">
      {!! Form::checkbox('is_archived', null, null, [
        'style' => "height: 18px; width: 18px;",
        'id' => 'is-archived-field',
      ]) !!}
      <label class="ml-2" for="is-archived-field">
        <strong>В архиве</strong>
      </label>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" class="btn btn-primary">Сохранить</button>
  </div>
</div>
