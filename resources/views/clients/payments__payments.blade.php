<div class="row">
  <div class="col-lg-12 margin-tb">
    <div class="pull-left">
      <h3>Оплаты</h3>
    </div>

    @include('clients.payments__payment-form')
  </div>

  <table class="table table-striped">
    <thead>
    <tr>
      {{--<th>#</th>--}}
      <th>№</th>
      <th>Дата</th>
      <th>Сумма</th>
      <th>Комментарий</th>
      <th style="width: 120px;">Действие</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($client->payments as $i => $payment)
      <tr>
        {{--                    <td>{{ ++$i }}</td>--}}
        <td>{{ $payment->id }}</td>
        <td>{{ date("d.m.Y", strtotime($payment->date)) }}</td>
        <td>{{ fineNumber($payment->amount, false) . ' руб.' }}</td>
        <td>{{ $payment->comment }}</td>

        <td>
          <a class="btn btn-primary" href="{{ route('payments.edit', $payment->id) }}">
            <v-icon name='pen'></v-icon>
          </a>
          {!! Form::open([
               'method' => 'DELETE',
               'route'  => ['payments.destroy', $payment->id],
               'style'  => 'display: inline'
          ]) !!}
          <button onclick="if (confirm('Хотите удалить этот элемент?')) this.form.submit(); else return false;"
                  class="btn btn-danger">
            <v-icon name='trash'></v-icon>
          </button>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach

    @if (!count($client->payments))
      <tr>
        <td colspan="5">Оплат нет</td>
      </tr>
    @endif
    </tbody>
  </table>
</div>
