<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <strong>Телефон:</strong>
      {{ $client->phone }}
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
      <strong>Адрес:</strong>
      {{ $client->address }}
    </div>
  </div>

  <div class="col-md-12">
    <div class="form-group">
      <strong>Прайслист:</strong>
      {{ $client->pricelist ? $client->pricelist->name : '<Не указан>' }}
    </div>
  </div>
</div>
