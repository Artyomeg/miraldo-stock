@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Заказы</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-success"
           href="{{ route('orders.create') }}">
          Добавить заказ
        </a>
      </p>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif

  <table class="table table-striped">
    <thead>
    <tr>
      <th>№</th>
      <th>Клиент</th>
      <th>Дата</th>
      <th>Сумма</th>
      <th style="width: 115px;">Действие</th>
    </tr>
    </thead>
    <tbody>
    @forelse($orders as $order)
      <tr>
        <td>{{ $order->number }}</td>
        <td>{{ $order->client->name }}</td>
        <td>{{ date("d.m.Y", strtotime($order->date)) }}</td>
        <td>{{ fineNumber($order->amount, false) . ' руб.' }}</td>
        <td>
          <a class="btn btn-primary" href="{{ route('orders.edit', $order->id) }}">
            <v-icon name='pen'></v-icon>
          </a>
          {!! Form::open([
            'method' => 'DELETE',
            'route'  => ['orders.destroy', $order->id],
            'style'  => 'display:inline'
          ]) !!}
          <button onclick="if (confirm('Хотите удалить этот элемент?')){ this.form.submit(); } else { return false; }"
                  class="btn btn-danger">
            <v-icon name='trash'></v-icon>
          </button>
          {!! Form::close() !!}
        </td>
      </tr>
    @empty
      <tr>
        <td colspan="5">Счетов нет</td>
      </tr>
    @endforelse
    </tbody>
  </table>

  {!! $orders->links() !!}
@endsection
