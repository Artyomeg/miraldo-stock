@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Создание заказа</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary"
           href="{{ route('orders.index') }}">Назад</a>
      </p>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <error-message-box :errors="errors"></error-message-box>

  {!! Form::open(['route' => 'orders.store', 'method' => 'POST']) !!}
    <order-form />
  {!! Form::close() !!}
@endsection
