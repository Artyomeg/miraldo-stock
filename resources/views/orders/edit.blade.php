@extends('layouts.app')

@section('content')
  <div class="g-technical-info__client_id" style="display: none">
    {{ $order->client_id }}
  </div>

  <div class="g-technical-info__order_date" style="display: none">
    {{ isset($order) ? $order->date : date('Y-m-d')}}
  </div>

  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>№ {{ $order->number }}</h2>
      </div>

      <p class="pull-right">
        <a href="{{ route('orders.index') }}" class="btn btn-primary">
          Назад
        </a>
      </p>
    </div>
  </div>

  {{ Form::open(['method' => 'PATCH', 'route' => ['orders.update', $order->id]]) }}
    <order-form :id="{{ $order->id }}" />
  {{ Form::close() }}
@endsection
