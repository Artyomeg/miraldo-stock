@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Прайс-лист поставщика "{{ $pricelist->name }}"</h2>
        <p>от {{ $currentDate }}</p>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary" href="{{ route('supplier-pricelists.index') }}">Назад</a>
      </p>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped">
        <tbody>
          @foreach ($productPrices as $i => $productPrice)
            @if ($productPrice->price != 0)
              <tr>
                <td>{{ ($i + 1)  }}</td>
                <td>{{ $productPrice->product->name }}</td>
                <td>{{ fineNumber($productPrice->price, true) }} руб.</td>
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
