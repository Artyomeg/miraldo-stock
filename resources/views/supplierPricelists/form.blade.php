<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <strong>Название:</strong>
      {!! Form::text('name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <strong>Курс евро:</strong>
      <toggle-button
        name="assigned_to_euro"
        :value="{{ isset($pricelist) && $pricelist->assigned_to_euro ? 'true' : 'false' }}"
      />
    </div>
  </div>
</div>

<div class="col-md-12 text-center">
  <button type="submit" class="btn btn-primary">Сохранить</button>
</div>
