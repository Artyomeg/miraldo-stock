@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Вход в режим контроля</div>

          <div class="card-body">
            <form
              method="POST"
              action="/set-control"
              autocomplete="false"
            >
              @csrf

              <div class="form-group row">
                <label
                  for="password"
                  class="col-md-4 col-form-label text-md-right"
                >
                  Пароль
                </label>

                <div class="col-md-6">
                  <input type="password" class="stealthy" tabindex="-1">
                  <input
                    type="text"
                    name="password"
                    required
                    autocomplete="off"
                    class="form-control g-password {{ $errors->has('password') ? ' is-invalid' : '' }}"
                    onfocus="this.removeAttribute('readonly');"
                  />
                  <input type="password" class="stealthy" tabindex="-1">

                  @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary form-control">
                    Войти
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
