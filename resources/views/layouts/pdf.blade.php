<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>{{ config('app.name', 'Stock') }}</title>
  <script src="{{ mix('js/app.js') }}" defer></script>
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <style>
    @font-face {
      font-family: "DejaVu Sans";
      font-style: normal;
      font-weight: 400;
      src: url("/fonts/dejavu-sans/DejaVuSans.ttf");
      /* IE9 Compat Modes */
      src: local("DejaVu Sans"),
      local("DejaVu Sans"),
      url("/fonts/dejavu-sans/DejaVuSans.ttf") format("truetype");
    }

    body {
      font-family: "DejaVu Sans", sans-serif;
      font-size: 13px;
    }

    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    table td {
      vertical-align: top;
    }

    table td,
    table th {
      border: solid 1px #444;
      padding: 0.5rem;
    }
  </style>
</head>
<body>
<div
  class="b-app"
  @if(Auth::user()) id="app" @endif
>
  <h1>{{ config('app.name', 'Stock') }}</h1>
  @yield('content')
</div>
</body>
</html>
