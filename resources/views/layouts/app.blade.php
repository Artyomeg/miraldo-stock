<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Stock') }}</title>

  <!-- Scripts -->
  <script src="{{ mix('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div
  class="b-app"

  @if(Auth::user())
    id="app"
  @endif
>
  <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
      <div class="navbar-nav mr-auto">
        <div class="nav-item">
          @if (\App\Models\ControlMode::checkModeIsControl())
            <a class="nav-link" href="{{ route('unset-control') }}">-S</a>
          @else
            <a class="nav-link" href="{{ route('control') }}">S</a>
          @endif
        </div>
      </div>

      <a class="navbar-brand" href="{{ \App\Models\User::HOME_URL }}">
        {{ config('app.name', 'Stock') }}
      </a>

      <button
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="{{ __('Toggle navigation') }}"
        class="navbar-toggler"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        type="button"
      >
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        @auth
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">
            @if (\App\Models\ControlMode::checkModeIsControl())
              {{-- Сверка --}}
              <li class="nav-item">
                <a class="nav-link" href="/revisions">Сверка</a>
              </li>

              {{-- Склад --}}
              <li class="nav-item">
                <a class="nav-link" href="{{ route('products.index') }}">Склад</a>
              </li>
            @endif

            {{-- Поставщики --}}
            <li class="nav-item">
              <a class="nav-link" href="/suppliers">Поставщики</a>
            </li>

            {{-- Клиенты --}}
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="javascript: void(0);"
                id="navbarReportsDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Клиенты
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarReportsDropdown">
                <a class="dropdown-item" href="{{ route('clients.index') }}">Все клиенты</a>
                <a class="dropdown-item" href="{{ route('clients.debts') }}">Долги клиентов</a>
              </div>
            </li>

            {{-- Счета --}}
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="javascript: void(0);"
                id="navbarReportsDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Счета и заказы
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarReportsDropdown">
                <a class="dropdown-item" href="{{ route('invoices.index') }}">Счета</a>
                <a class="dropdown-item" href="{{ route('orders.index') }}">Заказы</a>
              </div>
            </li>

            {{-- Прайс-листы --}}
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="javascript: void(0);"
                id="navbarReportsDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Прайс-листы
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarReportsDropdown">
                <a
                  class="dropdown-item"
                  href="{{ route('supplier-pricelists.index') }}"
                >Прайс-листы поставщиков</a>
                <a
                  class="dropdown-item"
                  href="{{ route('client-pricelists.index') }}"
                >Прайс-листы клиентов</a>
              </div>
            </li>

            {{-- Отчеты --}}
            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="javascript: void(0);"
                id="navbarReportsDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Отчеты
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarReportsDropdown">
                <a class="dropdown-item" href="/reports/by-products">Отчет по товарам</a>
                <a class="dropdown-item" href="/reports/by-client-payments">Платежи клиентов</a>

                @if (\App\Models\ControlMode::checkModeIsControl())
                  <a class="dropdown-item" href="/reports/products-moving">Отчет по продажам</a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item" href="/reports/revision-by-suppliers">Сверка поставщиков</a>
                  <a class="dropdown-item" href="/reports/by-supplies">Грузить на склад</a>
                @endif
              </div>
            </li>
          </ul>
        @endauth

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
          @guest
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">Вход</a>
            </li>
            {{--<li class="nav-item">--}}
            {{--@if (Route::has('register'))--}}
            {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
            {{--@endif--}}
            {{--</li>--}}
          @else
            <li class="nav-item dropdown">
              <a
                aria-haspopup="true"
                aria-expanded="false"
                data-toggle="dropdown"
                href="javascript: void(0);"
                id="navbarDropdown"
                role="button"
                class="nav-link dropdown-toggle"
              >
                S <span class="caret"></span>
              </a>

              <div aria-labelledby="navbarDropdown" class="dropdown-menu dropdown-menu-right">
                @if (\App\Models\ControlMode::checkModeIsControl())
                  <a
                    href="{{ route('settings.index') }}"
                    class="dropdown-item"
                  >
                    Настройки
                  </a>
                  <a
                    href="{{ route('logs.login') }}"
                    class="dropdown-item"
                  >
                    История входов
                  </a>
                @endif
                <a
                  href="{{ route('logout') }}"
                  class="dropdown-item"
                  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                  Выход
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>

  <main class="container py-4">
    @yield('content')
  </main>
</div>
</body>
</html>
