\@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Сверка</h2>
      </div>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif

  <revision-table></revision-table>
{{--  <example-component></example-component>--}}
{{--  <ExampleComponent></ExampleComponent>--}}
@endsection
{{--    <script>--}}
{{--      import ExampleComponent from '@/components/ExampleComponent.vue';--}}
{{--      export default {--}}
{{--        components: {ExampleComponent},--}}
{{--      }--}}
{{--    </script>--}}
