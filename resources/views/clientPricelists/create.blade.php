@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Создание прайс-листа клиента</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary" href="{{ route('client-pricelists.index') }}">Назад</a>
      </p>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::open(['route' => 'client-pricelists.store', 'method' => 'POST']) !!}
    @include('clientPricelists.form')
  {!! Form::close() !!}
@endsection
