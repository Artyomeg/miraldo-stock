@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Поставки</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-success" href="/supplies/create">Добавить поставку</a>
      </p>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif

  <table class="table table-striped">
    <thead>
    <tr>
      <th>№</th>
      <th>Дата</th>
      <th>Поставщик</th>
      <th style="width: 120px;">Действие</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($supplies as $supply)
      <tr>
        <td>{{ $supply->number }}</td>
        <td>{{ date('d.m.Y', strtotime($supply->date)) }}</td>
        <td>{{ $supply->supplier->name }}</td>
        <td>
          <a class="btn btn-primary" href="/supplies/{{ $supply->id }}/edit">
            <v-icon name='pen'></v-icon>
          </a>
          {!! Form::open([
              'method' => 'DELETE',
              'route'  => ['supplies.destroy', $supply->id],
              'style'  => 'display:inline'
          ]) !!}
          <button onclick="if (confirm('Хотите удалить этот элемент?')) this.form.submit(); else return false;"
                  class="btn btn-danger">
            <v-icon name='trash'></v-icon>
          </button>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach

    @if(!count($supplies))
      <tr>
        <td colspan="5">Поставок нет</td>
      </tr>
    @endif
    </tbody>
  </table>

  {!! $supplies->links() !!}
@endsection
