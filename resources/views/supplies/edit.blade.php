@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Поставка № {{ $supply->number }}</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary" href="{{ route('clients.index') }}">Назад</a>
      </p>
    </div>
  </div>

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::model($supply, ['method' => 'PATCH', 'route' => ['supplies.update', $supply->id]]) !!}
    @include('supplies.form')
  {!! Form::close() !!}
@endsection
