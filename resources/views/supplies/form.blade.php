<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <strong>Поставщик:</strong>
    </div>
  </div>

  <div class="col-md-4">
    <div class="form-group">
      <strong>Дата:</strong>
      <input
        class="form-control"
        name="date"
        required
        type="date"
        value="{{ isset($supply) ? $supply->date : date('Y-m-d') }}"
      >
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <strong>Валюта:</strong>
      {!! Form::select('currency_id', $currencies, null, [
        'class' => 'form-control',
        'placeholder' => '',
        'ref' => 'currency',
        'required' => '',
        '@change' => 'changeCurrency($event)'
      ]) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="form-group col-md-12">
    <supply-products-form
      :supply-products-id="{{ isset($supply) ? $supply->id : "''" }}"
      :currency-id="globalVar.currencyId"
    ></supply-products-form>
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" class="btn btn-primary">Сохранить</button>
  </div>
</div>
