@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Отчет по товарам</h2>
      </div>
    </div>
  </div>

  <report-by-products-table></report-by-products-table>
@endsection
