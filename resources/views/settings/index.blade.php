@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Настройки</h2>
      </div>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <div class="row">
    <div class="col-md-12">
      <table class="table table-striped">
        <tbody>
          @foreach ($settings as $i => $setting)
            <tr>
              <td>Курс евро</td>
              <td>{{ $setting->value }}
                @if($setting->code === 'dollar_price')
                  руб.
                @endif</td>
              <td>
                <a class="btn btn-primary" href="/settings/euro-price/edit">
                  <v-icon name='pen'></v-icon>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
