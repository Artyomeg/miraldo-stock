@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Настройки</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-primary" href="{{ route('settings.index') }}">Назад</a>
      </p>
    </div>
  </div>

{{--    <div class="row">--}}
{{--        <div class="col-md-12 margin-tb">--}}
{{--            <div class="pull-left">--}}
{{--                <h3>@lang('settings.fields.euro_price')</h3>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

  @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Ошибка!</strong><br><br>
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  {!! Form::open(['route' => 'api.settings.update', 'method' => 'PATCH']) !!}
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <th>Время установки</th>
            <th>Курс евро</th>
            <th></th>
          </thead>
          <tbody>
            @if($currentSetting)
              <tr>
                <td>Новое значение</td>
                <td>
                  <float-number
                    class="form-control"
                    initvalue="{{ $currentSetting->value }}"
                    name="dollar_price"
                    required
                  />
                </td>
                <td style="vertical-align: middle;">
                  @if($currentSetting->code === 'dollar_price')
                    руб.
                  @endif
                </td>
              </tr>
            @endif

            @foreach ($settings as $i => $setting)
              <tr>
                <td>{{ $setting->created_at->format('d.m.Y H:i') }}</td>
                <td>
                  <input
                    type="text"
                    value="{{ $setting->value }}"
                    disabled
                    class="form-control"
                  >
                </td>
                <td style="vertical-align: middle;">
                  @if($setting->code === 'dollar_price')
                    руб.
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Сохранить</button>
      </div>
    </div>
  {!! Form::close() !!}
@endsection
