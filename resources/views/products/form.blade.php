<div class="row">
  {{-- Название --}}
  <div class="col-md-5">
    <div class="form-group">
      <strong>Наименование</strong>
      {!! Form::text('name', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>

  {{-- Единица измерения --}}
  <div class="col-md-2">
    <div class="form-group">
      <strong>Единица измерения</strong>
      {!! Form::select(
        'unit',
        [
          'кг' => 'килограммы',
          'шт' => 'штуки'
        ],
        'кг',
        ['placeholder' => '', 'class' => 'form-control']
      ) !!}
    </div>
  </div>

  {{-- Штрих-код --}}
  <div class="col-md-2">
    <div class="form-group">
      <strong>Штрих-код</strong>
      {!! Form::text('barcode', null, ['placeholder' => '', 'class' => 'form-control']) !!}
    </div>
  </div>

  {{-- Штрих-код - Символов для кг --}}
  <div class="col-md-1">
    <div class="form-group">
      <strong>ШК - кг</strong>
      {!! Form::select(
        'barcode_kg_chars',
        [
          '2' => '2',
          '3' => '3',
        ],
        '2',
        ['class' => 'form-control']
      ) !!}
    </div>
  </div>

  {{-- Количество --}}
  <div class="col-md-2">
    <div class="form-group">
      <strong>Количество</strong>
      <float-number-view
        name="count"
        <?php if (isset($product)) { ?>
          :customvalue="{{ $product->count }}"
        <?php } ?>
        negative
        class="form-control"
      ></float-number-view>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <h3>Цены в прайс-листах для клиентов</h3>

    @if(isset($product))
      @foreach ($product->getFullProductPrice() as $pricelistId => $productPriceStructure)
        <div class="form-group">
          <strong>Цена по прайсу клиента "{{ $productPriceStructure['pricelist']->name }}", руб.:</strong>
          <float-number-view
            name="productPriceByClientPriceList[{{ $pricelistId }}]"
            :customvalue="{{ $productPriceStructure['price'] }}"
            class="form-control"
          ></float-number-view>
        </div>
      @endforeach
    @else
      @foreach ($clientPricelists as $pricelist)
        <div class="form-group">
          <strong>{{ $pricelist->name }}:</strong>
          <float-number-view
            name="productPriceByClientPriceList[{{ $pricelist->id }}]"
            :customvalue="0"
            class="form-control"
          ></float-number-view>
        </div>
      @endforeach
    @endif
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <h3>Цены в прайс-листах от поставщиков</h3>

    @if(isset($product))
      @foreach ($product->getSupplyProductPrices() as $pricelistId => $productPriceStructure)
        <div class="form-group">
          <strong>Цена по прайсу поставщика "{{ $productPriceStructure['pricelist']->name }}", руб.:</strong>
          <float-number-view
            name="productPriceBySupplierPriceList[{{ $pricelistId }}]"
            :customvalue="{{ $productPriceStructure['price'] }}"
            class="form-control"
          ></float-number-view>
        </div>
      @endforeach
    @else
      @foreach ($supplierPricelists as $pricelist)
        <div class="form-group">
          <strong>{{ $pricelist->name }}:</strong>
          <float-number-view
            name="productPriceBySupplierPriceList[{{ $pricelist->id }}]"
            :customvalue="0"
            class="form-control"
          ></float-number-view>
        </div>
      @endforeach
    @endif
  </div>
</div>

<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" class="btn btn-primary">Сохранить</button>
  </div>
</div>
