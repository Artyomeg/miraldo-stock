@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-lg-12 margin-tb">
      <div class="pull-left">
        <h2>Склад</h2>
      </div>
      <p class="pull-right">
        <a class="btn btn-success" href="{{ route('products.create') }}">Добавить товар</a>
      </p>
    </div>
  </div>

  @if ($message = Session::get('success'))
    <div class="alert alert-success">
      <p>{{ $message }}</p>
    </div>
  @endif


  <products></products>

  {{--<table class="table table-striped">--}}
  {{--<thead>--}}
  {{--<tr>--}}
  {{--<th>#</th>--}}
  {{--<th>@lang('products.fields.name')</th>--}}
  {{--<th>@lang('products.fields.count')</th>--}}
  {{--<th class="g-hide-on-print" style="width: 120px;">@lang('elements.action')</th>--}}
  {{--</tr>--}}
  {{--</thead>--}}
  {{--<tbody>--}}
  {{--@foreach ($products as $product)--}}
  {{--<tr>--}}
  {{--<td>{{ ++$i }}</td>--}}
  {{--<td>{{ $product->name }}</td>--}}
  {{--<td>{{ (float)$product->count }}</td>--}}
  {{--<td class="g-hide-on-print">--}}
  {{--<a class="btn btn-primary" href="{{ route('products.edit', $product->id) }}"><v-icon name='pen'></v-icon></a>--}}

  {{--{!! Form::open([--}}
  {{--'method' => 'DELETE',--}}
  {{--'route'  => ['products.destroy', $product->id],--}}
  {{--'style'  => 'display:inline'--}}
  {{--]) !!}--}}
  {{--<button onclick="if (confirm('Хотите удалить этот элемент?')) this.form.submit(); else return false;" class="btn btn-danger">--}}
  {{--<v-icon name='trash'></v-icon>--}}
  {{--</button>--}}
  {{--{!! Form::close() !!}--}}
  {{--</td>--}}
  {{--</tr>--}}
  {{--@endforeach--}}

  {{--<tr>--}}
  {{--<td colspan="2">@lang('elements.labels.total')</td>--}}
  {{--<td colspan="2">{{ number_format($totalWeight, 0, '.', ' ') }} @lang('elements.labels.kg')</td>--}}
  {{--</tr>--}}

  {{--@if(!count($products))--}}
  {{--<tr>--}}
  {{--<td colspan="5">Товаров нет</td>--}}
  {{--</tr>--}}
  {{--@endif--}}
  {{--</tbody>--}}
  {{--</table>--}}

  {{--{!! $products->links() !!}--}}

@endsection
