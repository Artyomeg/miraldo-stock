## Установка

1. Проверьте версию PHP. Она должна быть `8.0`.
2. Установите laravel
    * `composer install`
    * Создайте файл `.env`
    * `php artisan key:generate`
3. Установите пакеты npm - `npm install`
4. Установите пакеты composer v2 - `composer install`

## Кодстайл

`;` обязательна.


## Добавление нового пользователя
    \App\User::create([
        'name' => 'test',
        'email' => 'test@test.ru',
        'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
    ]);


## Установка пароля ночного режима
    User::find(1)->update(['night_mode_password' => Hash::make('12345678')])
