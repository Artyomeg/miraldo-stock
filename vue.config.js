const aliases = require('./vue.alias');

module.exports = {
  chainWebpack: config => {
    // добавляем свои сокращения
    for (let aliasName in aliases) {
      config.resolve.alias.set(aliasName, aliases[aliasName]);
    }
  },
}
