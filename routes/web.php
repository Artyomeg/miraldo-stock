<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes([
    'register' => false,
]);

Route::get('/home', function () {
    return redirect('/');
});
Route::post('/login-only-with-password', 'Auth\LoginController@loginOnlyWithPassword')->middleware('guest');

Route::get('/control', 'Auth\ControlModeController@showControlPage')->name('control');
Route::get('/unset-control', 'Auth\ControlModeController@unsetControlMode')->name('unset-control');
Route::post('/set-control', 'Auth\ControlModeController@setControlMode')->name('set-control');

Route::middleware(['auth', 'locale'])->group(function () {
    // --- Clients
    Route::get('/api/clients', 'Api\ClientController@list');
    Route::post('/api/clients', 'Api\ClientController@list');
    Route::get('/clients/debts', 'ClientController@debts')->name('clients.debts');
    Route::resource('clients', 'ClientController');
    Route::get('/clients/{id}/payments', 'ClientController@payments')->name('clients.payments');

    // --- Invoices
    // TODO: should refactor
    Route::get('/api/invoices', 'Api\InvoiceController@list');
    Route::get('/api/invoices/{id}', 'Api\InvoiceController@show');
    Route::get('/api/invoices/{id}/invoice-products', 'Api\InvoiceProductController@getInvoiceProductsByInvoiceId');
    Route::resource('invoices', 'InvoiceController');
    Route::get('/api/invoices/{invoice}/download', 'Api\InvoiceController@download')->name('api.invoices.download');

    // --- Payments
    Route::post('/api/payments/store', 'Api\PaymentController@store');
    Route::delete('/api/payments/{id}', 'Api\PaymentController@destroy');
    Route::resource('payments', 'PaymentController');

    // --- Supplier Invoices
    Route::get('/api/supplier-invoices/{id}', 'Api\SupplierInvoiceController@show');
    Route::post('/api/supplier-invoices/store', 'Api\SupplierInvoiceController@store');
    Route::patch('/api/supplier-invoices/{id}', 'Api\SupplierInvoiceController@update');
    Route::delete('/api/supplier-invoices/{id}', 'Api\SupplierInvoiceController@destroy');
    Route::get('/supplier-invoices{all}', function () {
        return view('default');
    })->where('all', '.*');

    // --- Products
    Route::resource('products', 'ProductController')->names('products');
    Route::prefix('api')->name('api.')->group(function () {
        Route::prefix('products')->name('products.')->group(function () {
            Route::get('/', 'Api\ProductController@list')->name('list');
            // TODO: should refactor to /invoice-prices
            Route::get('/prices', 'Api\ProductController@getProductsWithInvoicePrices')->name('get-products-with-invoice-prices');
            Route::get('/supply-prices', 'Api\ProductController@getProductsWithSupplyPrices')->name('get-products-with-supply-prices');
            // TODO: should refactor
            Route::get('/barcodes', 'Api\ProductController@getAllWithBarcode')->name('barcodes');
        });
    });

    // --- Client Price lists
    Route::prefix('api')->name('api.')->group(function () {
        Route::get('/client-pricelists', 'Api\ClientPricelistController@list')->name('client-pricelist.index');
        Route::get('/client-pricelists/{id}', 'Api\ClientPricelistController@show')->name('client-pricelist.show');
        Route::post('/client-pricelists/{pricelist}/duplicate', 'Api\ClientPricelistController@duplicate')->name('client-pricelist.duplicate');
        Route::delete('/client-pricelists/{id}', 'Api\ClientPricelistController@destroy')->name('client-pricelist.destroy');
    });
    Route::resource('client-pricelists', 'ClientPricelistController');
    Route::get('/client-pricelists', function () {
        return view('default');
    })->where('all', '.*')->name('client-pricelists.index');
    Route::get('/client-pricelists/{id}/duplicate', function () {
        return view('default');
    })->where('all', '.*');

    // --- Supplier Price lists
    Route::get('/api/supplier-pricelists', 'Api\SupplierPricelistController@list');
    Route::delete('/api/supplier-pricelists/{id}', 'Api\SupplierPricelistController@destroy');
    Route::resource('supplier-pricelists', 'SupplierPricelistController');

    // --- Reports
    Route::get('/api/reports/by-client-payments', 'Api\ReportController@byPayments');
    Route::post('/api/reports/by-products', 'Api\ReportController@byProducts');
    Route::post('/api/reports/products-moving', 'Api\ReportController@productsMoving')->middleware('control-mode');
    Route::get('/api/reports/revision-by-suppliers', 'Api\ReportController@revisionBySuppliers')->middleware('control-mode');
    Route::get('/api/reports/by-supplies', 'Api\ReportController@bySupplies')->middleware('control-mode');
    Route::get('/reports/by-products', function () {
        return view('reports.by-products');
    })->name('reports.by-products');
    Route::get('/reports/products-moving', function () {
        return view('reports.products-moving');
    })->middleware('control-mode')->name('reports.products-moving');
    Route::get('/reports/by-client-payments', function () {
        return view('default');
    })->name('reports.by-payments');
    Route::get('/reports/revision-by-suppliers', function () {
        return view('default');
    })->middleware('control-mode')->name('reports.by-suppliers');
    Route::get('/reports/by-supplies', function () {
        return view('default');
    })->middleware('control-mode')->name('reports.by-supplies');

    // --- Revisions
    Route::post('/api/revision/get-supplies-and-invoices', 'Api\RevisionController@getSuppliesAndInvoices')->middleware('control-mode');
    Route::get('/revisions', 'RevisionController@index')->middleware(['locale', 'control-mode'])->name('revisions');
    //Route::get('/stocktaking', 'RevisionController@stocktaking')->middleware('locale');

    // --- Suppliers
    Route::get('/api/suppliers', 'Api\SupplierController@index');
    Route::post('/api/suppliers', 'Api\SupplierController@store');
    Route::get('/api/suppliers/{id}', 'Api\SupplierController@show');
    Route::patch('/api/suppliers/{id}', 'Api\SupplierController@update');
    Route::delete('/api/suppliers/{id}', 'Api\SupplierController@destroy');
    Route::get('/suppliers{all}', function () {
        return view('default');
    })->where('all', '.*');

    // --- Supplies
    Route::post('/api/supplies', 'Api\SupplyController@store');
    Route::get('/api/supplies/{id}', 'Api\SupplyController@show');
    // patch не работает с form-data в laravel
    Route::post('/api/supplies/{id}', 'Api\SupplyController@update');
    Route::delete('/api/supplies/{id}', 'Api\SupplyController@destroy');
    Route::get('/api/supplies/{id}/supply-products', 'Api\SupplyProductController@getSupplyProductsBySupplyId');
    Route::get('/supplies{all}', function () {
        return view('default');
    })->where('all', '.*');
//    Route::apiResource('activity', 'Api\ActivityController')->names('activity');

    // --- Orders
    Route::prefix('api')->name('api.')->group(function () {
        Route::apiResource('orders', 'Api\OrderController')->names('order');
    });
    Route::get('/orders', function () {
        return view('default');
    })->name('orders.index');
    Route::get('/orders{all}', function () {
        return view('default');
    })->where('all', '.*');

    // --- Logs
    Route::get('/logs/login', function () {
        return view('default');
    })->middleware('control-mode')->name('logs.login');

    // --- Settings
    Route::prefix('settings')
        ->name('settings.')
        ->middleware('control-mode')
        ->group(function () {
            Route::get('/', 'SettingsController@index')->name('index');
            Route::get('/euro-price/edit', function () {
                return view('default');
            })->name('euro-price.edit');
        });
    Route::prefix('api/settings')
         ->name('api.settings.')
         ->middleware('control-mode')
         ->group(function () {
             Route::get('/', 'Api\SettingController@index')->name('index');
             Route::post('/', 'Api\SettingController@store')->name('store');
             Route::patch('/', 'Api\SettingController@update')->name('update');
             Route::delete('/', 'Api\SettingController@destroy')->name('destroy');
         });
});

Route::apiResource('/api/activity', 'Api\ActivityController')->names('activity');

//Route::resource('supplies', 'SupplyController');

Route::permanentRedirect('/', \App\Models\User::HOME_URL);

//Route::get('/', 'RevisioController@index')->middleware('locale');
