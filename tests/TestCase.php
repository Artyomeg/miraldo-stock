<?php

namespace Tests;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, LazilyRefreshDatabase;

    /**
     * Indicates whether the default seeder should run before each test.
     *
     * @var bool
     */
    protected $seed = true;

    /**
     * Indicates whether act as the authenticated user
     *
     * @var bool
     */
    protected $guest = false;

    /** @var Authenticatable */
    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        if ($this->guest === false) {
            $this->user = User::factory()->createOne();

            auth()->login($this->user);
        }
    }
}
