<?php declare(strict_types = 1);

namespace Http\Controllers\Api;

use App\Models\Client;
use App\Models\Invoice;
use App\Models\InvoiceProduct;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testDownload()
    {
        $invoice = Invoice::factory()
                          ->has(Client::factory())
                          ->has(InvoiceProduct::factory()->count(10))
                          ->createOne();

        $this->get(route('api.invoices.download', compact('invoice')))
             ->assertOk()
             ->assertHeader('content-type', 'application/pdf');
    }
}
