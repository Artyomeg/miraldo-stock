<?php

namespace Http\Controllers\Api;

use App\Models\ClientPricelist;
use App\Models\ClientPricelistProductPrice;
use App\Models\Setting;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SettingControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $count = Setting::withTrashed()->count();
        Setting::withTrashed()->inRandomOrder()->firstOrFail()->delete();

        $response = $this->get(route('api.setting.index', ['code' => 'dollar_price']));
        $response->assertSuccessful()
            ->assertJsonStructure([
                'data' => [['id', 'value', 'deleted_at', 'created_at']],
            ]);

        $this->assertCount($count, $response->json('data'));
    }

    public function testStore()
    {
        Setting::factory()->create([
            'code'  => 'dollar_price',
            'value' => 10,
        ]);

        $pricelist = ClientPricelist::factory()->create([
            'assigned_to_euro' => true,
        ]);

        $price = ClientPricelistProductPrice::factory()->create([
            'client_pricelist_id' => $pricelist->id,
            'price'               => 10,
        ]);

        $count = Setting::withTrashed()->count();
        $response = $this->post(route('api.setting.store', ['code' => 'dollar_price', 'value' => 200]));

        $response->assertSuccessful()
            ->assertJsonStructure([
                'data' => [['id', 'value', 'deleted_at', 'created_at']],
            ]);

        $this->assertCount($count + 1, $response->json('data'));
        $this->assertCount($count, Setting::onlyTrashed()->get());
        $this->assertDatabaseHas(Setting::class, ['code' => 'dollar_price', 'value' => 200]);
        $this->assertNotEquals($price->price, $price->refresh()->price);
    }

    public function testFailedDestroy()
    {
        $id = Setting::query()
            ->latest('id')
            ->limit(3)
            ->pluck('id')
            ->toArray();

        $this->delete(route('api.setting.destroy', compact('id')))
            ->assertInvalid('id');
    }

    public function testSucceedDestroy()
    {
        $id = Setting::query()
            ->latest('id')
            ->offset(1)
            ->limit(2)
            ->pluck('id')
            ->toArray();

        $this->delete(route('api.setting.destroy', compact('id')))
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [['id', 'value', 'created_at']],
            ]);

        $this->assertDatabaseMissing(Setting::class, ['id' => $id[0]]);
        $this->assertDatabaseMissing(Setting::class, ['id' => $id[1]]);
    }
}
