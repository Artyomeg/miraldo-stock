<?php declare(strict_types = 1);

namespace Http\Controllers\Api;

use App\Models\Client;
use App\Models\Order;
use App\Models\OrderProduct;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    protected const PRODUCT_RESOURCE_STRUCTURE = [
        'id',
        'orderId',
        'comment',
        'product' => ['id'],
    ];

    protected const ORDER_RESOURCE_STRUCTURE = [
        'id',
        'date',
        'comment',
        'client' => ['id'],
        'products' => [self::PRODUCT_RESOURCE_STRUCTURE],
    ];

    public function testIndex()
    {
        Order::factory()
             ->has(OrderProduct::factory()->count(2))
             ->count(2)
             ->create();

        $response = $this->get(route('api.order.index'));
        $response->assertOk()
                 ->assertJsonStructure(['data' => [['id', 'date', 'comment', 'client']]]);
    }

    public function testShow()
    {
        $order = Order::factory()
                      ->has(OrderProduct::factory()->count(2))
                      ->create();

        $response = $this->get(route('api.order.show', compact('order')));

        $response->assertOk()
                 ->assertJsonStructure(['data' => self::ORDER_RESOURCE_STRUCTURE]);
    }

    public function testStore()
    {
        $formData = [
            'date'      => now()->addMonth()->format('Y-m-d H:i:s'),
            'client_id' => 1,
            'comment'   => 'Lorem ipsum',
        ];

        $formProducts = [
            'orderProducts' => [
                'new' => [
                    1 => ['productId' => 1, 'comment' => 'Lorem'],
                    2 => ['productId' => 2, 'comment' => 'Ipsum'],
                    3 => ['productId' => 3],
                ],
            ],
        ];

        $this->post(route('api.order.store'), array_merge($formData, $formProducts))
             ->assertCreated()
             ->assertJsonStructure(['data' => self::ORDER_RESOURCE_STRUCTURE]);

        $this->assertDatabaseHas(Order::class, $formData);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => 1, 'product_id' => 1, 'comment' => 'Lorem']);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => 1, 'product_id' => 2, 'comment' => 'Ipsum']);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => 1, 'product_id' => 3, 'comment' => null]);
    }

    public function testUpdate()
    {
        $order  = Order::factory()->createOne();
        $client = Client::factory()->createOne();

        OrderProduct::factory()->createMany([
            ['order_id' => $order->id, 'product_id' => 1, 'comment' => 'Lorem'],
            ['order_id' => $order->id, 'product_id' => 2, 'comment' => null],
        ]);

        $formData = [
            'date'      => now()->addMonth()->format('Y-m-d'),
            'client_id' => $client->id,
            'comment'   => 'Lorem ipsum',
        ];

        $formProducts = [
            'orderProducts' => [
                'old' => [
                    2 => ['productId' => 2, 'comment' => 'Ipsum'],
                ],
                'new' => [
                    3 => ['productId' => 3, 'comment' => 'Foo'],
                    4 => ['productId' => 4, 'comment' => 'Bar'],
                ],
            ],
        ];

        $this->put(route('api.order.update', compact('order')), array_merge($formData, $formProducts))
             ->assertOk()
             ->assertJsonStructure(['data' => self::ORDER_RESOURCE_STRUCTURE]);

        $order->refresh();

        $this->assertEquals($order->date->format('Y-m-d'), $formData['date']);
        $this->assertEquals($order->client_id, $formData['client_id']);
        $this->assertEquals($order->comment, $formData['comment']);

        $this->assertDatabaseMissing(OrderProduct::class, ['id' => 1]);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => $order->id, 'product_id' => 2, 'comment' => 'Ipsum']);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => $order->id, 'product_id' => 3, 'comment' => 'Foo']);
        $this->assertDatabaseHas(OrderProduct::class, ['order_id' => $order->id, 'product_id' => 4, 'comment' => 'Bar']);
    }

    public function testDestroy()
    {
        $order = Order::factory()
                      ->has(OrderProduct::factory()->count(2))
                      ->create();

        $response = $this->delete(route('api.order.destroy', compact('order')));
        $response->assertNoContent();

        $this->assertDatabaseCount(Order::class, 0);
        $this->assertDatabaseCount(OrderProduct::class, 0);
    }
}
