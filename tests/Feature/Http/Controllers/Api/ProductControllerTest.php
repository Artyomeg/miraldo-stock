<?php declare(strict_types = 1);

namespace Http\Controllers\Api;

use App\Models\Product;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
//    public function testGetProducts()
//    {
//        $this->get(route('api.products.get-products'))
//             ->assertOk()
//             ->assertJsonStructure([1]);
//    }

    public function testGetProductsWithInvoicePrices()
    {
        $this->get(route('api.products.get-products-with-invoice-prices'))
             ->assertOk()
             ->assertJsonStructure([
                 'data' => [
                     [
                         'id',
                         'barcode',
                         'name',
                         'unit',
                         'prices' => [
                             ['id', 'product_id', 'price', 'client_pricelist_id'],
                         ]
                     ],
                 ],
             ]);
    }

    public function testGetProductsWithSupplyPrices()
    {
        $this->get(route('api.products.get-products-with-supply-prices'))
             ->assertOk()
             ->assertJsonStructure([
                 'data' => [
                     [
                         'id',
                         'barcode',
                         'name',
                         'unit',
                         'prices' => [
                             ['id', 'product_id', 'price', 'supplier_pricelist_id'],
                         ]
                     ],
                 ],
             ]);
    }

//    public function testGetAll()
//    {
//        $this->get(route('api.products.get-all'))
//             ->assertOk()
//             ->assertJsonStructure([
//                 'data' => [
//                     [
//                         'id', 'barcode', 'name', 'count', 'unit', 'supplierCode', 'searchString',
//                     ]
//                 ]
//             ]);
//    }

//    public function testGetAllSortedBySupplierCode()
//    {
//        Product::factory()->createMany([
//            ['supplier_code' => '123'],
//            ['supplier_code' => '234'],
//        ]);
//
//        $response = $this->get(route('api.products.get-all', ['order' => 'supplier-code']))->json('data');
//
//        $this->assertEquals('123', $response[0]['supplierCode']);
//        $this->assertEquals('234', $response[1]['supplierCode']);
//    }

    public function testGetAllWithBarcode()
    {
        $anyProduct = Product::query()
                             ->inRandomOrder()
                             ->first();

        $this->get(route('api.products.barcodes'))
             ->assertOk()
             ->assertJsonStructure([
                 'data' => [
                     $anyProduct->barcode => ['id', 'barcode', 'name', 'count', 'unit', 'supplierCode', 'searchString']
                 ]
             ])
             ->assertJson([
                 'data' => [
                     $anyProduct->barcode => [
                         'id'           => $anyProduct->id,
                         'barcode'      => $anyProduct->barcode,
                         'name'         => $anyProduct->name,
                         'count'        => $anyProduct->count,
                         'unit'         => $anyProduct->unit,
                         'supplierCode' => $anyProduct->supplier_code,
                         'searchString' => $anyProduct->search_string,
                     ]
                 ]
             ]);
    }
}
