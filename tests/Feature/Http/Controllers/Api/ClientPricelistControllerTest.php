<?php declare(strict_types = 1);

namespace Http\Controllers\Api;

use App\Models\ClientPricelist;
use App\Models\ClientPricelistProductPrice;
use Tests\TestCase;

class ClientPricelistControllerTest extends TestCase
{
    public function testDuplicate()
    {
        $pricelist = ClientPricelist::factory()
                                    ->has(ClientPricelistProductPrice::factory()->count(2), 'productPrices')
                                    ->create(['assigned_to_euro' => true]);

        $response = $this->post(
            uri: route('api.client-pricelist.duplicate', compact('pricelist')),
            data: ['name' => 'Copy of Pricelist']
        );

        $new_pricelist_id = $pricelist->id + 1;

        $response
            ->assertCreated()
            ->assertJsonStructure(['data' => ['id', 'name', 'assigned_to_euro', 'productPrices' => []]])
            ->assertJson(['data' => ['id' => $new_pricelist_id, 'name' => 'Copy of Pricelist']]);

        // source model with relations not removed/replaced
        $this->assertNotNull($pricelist->refresh());
        $this->assertCount(2, $pricelist->refresh()->productPrices);

        // new relations
        $this->assertDatabaseHas(ClientPricelistProductPrice::class, [
            'client_pricelist_id' => $new_pricelist_id,
            'product_id'          => $pricelist->productPrices[0]->product_id,
            'price'               => $pricelist->productPrices[0]->price,
        ]);

        $this->assertDatabaseHas(ClientPricelistProductPrice::class, [
            'client_pricelist_id' => $new_pricelist_id,
            'product_id'          => $pricelist->productPrices[1]->product_id,
            'price'               => $pricelist->productPrices[1]->price,
        ]);
    }
}
