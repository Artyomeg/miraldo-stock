<?php declare(strict_types = 1);

namespace Http\Middleware;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class RestrictIpAddressesTest extends TestCase
{
    use RefreshDatabase;

    public function testAllowedRequest()
    {
        $response = $this->get(route('invoices.index'));
        $response->assertOk();
    }

    public function testRestrictedRequest()
    {
        $ip = request()->ip();

        DB::table('banned_ip_addresses')->insert(compact('ip'));
        $response = $this->get(route('invoices.index'));

        $response->assertForbidden()
                 ->assertSee('Access denied');
    }
}
