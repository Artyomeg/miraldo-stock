<?php

declare(strict_types = 1);

namespace Auth;

use App\Console\Commands\ResetAuth;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\HttpKernel\Exception\UnexpectedSessionUsageException;
use Tests\TestCase;

class NightModeAuth extends TestCase
{
    use RefreshDatabase;

    protected $guest = true;

    public function testUnexpectedDriver()
    {
        config(['session.driver' => 'array']);
        $this->expectException(UnexpectedSessionUsageException::class);

        Artisan::call(ResetAuth::class);
    }

    public function testPassword()
    {
        [$from_h, $from_m] = explode(':', config('auth.night_mode.from'));
        [$till_h, $till_m] = explode(':', config('auth.night_mode.till'));

        $user = User::factory()->create([
            'password'            => 'password',
            'night_mode_password' => 'night_mode_password',
        ]);

        $this->travelTo(now()->setTime($from_h, $from_m)->addMinute());
        $this->assertEquals($user->getAuthPassword(), 'night_mode_password');

        $this->travelTo(now()->setTime($from_h, $from_m)->subMinute());
        $this->assertEquals($user->getAuthPassword(), 'password');

        $this->travelTo(now()->setTime($till_h, $till_m)->addMinute());
        $this->assertEquals($user->getAuthPassword(), 'password');

        $this->travelTo(now()->setTime($till_h, $till_m)->subMinute());
        $this->assertEquals($user->getAuthPassword(), 'night_mode_password');
    }
}
