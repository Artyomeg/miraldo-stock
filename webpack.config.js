const aliasObj = require('./vue.alias');

module.exports = {
  resolve: {
    modules: ["node_modules"],
    alias: aliasObj,
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              // 'es2020',
              // 'es2015',
              // 'stage-0',
            ], // default = env
            compact: true,
            // plugins: [
            //   "@babel/plugin-proposal-class-properties",
            //   "@babel/plugin-proposal-private-methods",
            // ],
          },
        },
      },
      // {
      //   test: /\.ts?$/,
      //   loader: 'ts-loader',
      //   options: {
      //     appendTsSuffixTo: [/\.vue$/]
      //   }
      // },
    ],
  },
}
