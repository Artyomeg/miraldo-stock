<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricelists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 127);
        });

        // 2 Цены на продукты
        Schema::table('product_prices', function (Blueprint $table) {
            $table->unsignedInteger('pricelist_id')->after('id');

            $table->foreign('pricelist_id')
                ->references('id')
                ->on('pricelists')
                ->onDelete('cascade');
        });

        // 3 Клиенты
        Schema::table('clients', function (Blueprint $table) {
            $table->unsignedInteger('pricelist_id')->nullable()->after('id');

            $table->foreign('pricelist_id')
                ->references('id')
                ->on('pricelists')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropForeign('clients_pricelist_id_foreign');
            $table->dropColumn('pricelist_id');
        });

        Schema::table('product_prices', function (Blueprint $table) {
            $table->dropForeign('product_prices_pricelist_id_foreign');
            $table->dropColumn('pricelist_id');
        });

        Schema::dropIfExists('pricelists');
    }
}
