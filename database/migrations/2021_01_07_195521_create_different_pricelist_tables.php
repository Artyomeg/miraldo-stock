<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDifferentPricelistTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1.1 Прайслисты клиентов
        Schema::rename('pricelists', 'client_pricelists');
        Schema::table('client_pricelists', function (Blueprint $table) {
            $table->renameColumn('assigned_to_dollar', 'assigned_to_euro');
        });

        // 1.2 Прайслисты поставщиков
        Schema::create('supplier_pricelists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 127);
            $table->boolean('assigned_to_euro')->default(0);
        });


        // 2.1 Цены прайслистов клиентов
        Schema::rename('product_prices', 'client_pricelist_product_prices');
        Schema::table('client_pricelist_product_prices', function (Blueprint $table) {
            $table->renameColumn('pricelist_id', 'client_pricelist_id');
        });

        // 2.2 Цены прайслистов поставщиков
        Schema::create('supplier_pricelist_product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplier_pricelist_id');
            $table->unsignedInteger('product_id');
            $table->unsignedDecimal('price', 10, 2);

            $table->foreign('supplier_pricelist_id')
                ->references('id')
                ->on('supplier_pricelists')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });


        // 3.1 Клиенты
        Schema::table('clients', function (Blueprint $table) {
            // переименуем колонку
            $table->renameColumn('pricelist_id', 'client_pricelist_id');

            // переименуем внешний ключ
            if (! app()->environment('testing')) {
                $table->dropForeign('clients_pricelist_id_foreign');
            }
            $table->foreign('client_pricelist_id')
                ->references('id')
                ->on('client_pricelists')
                ->onDelete('set null');
        });

        // 3.2 Поставщики
        Schema::table('suppliers', function (Blueprint $table) {
            $table->unsignedInteger('supplier_pricelist_id')->nullable()->after('id');

            $table->foreign('supplier_pricelist_id')
                ->references('id')
                ->on('supplier_pricelists')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 3.2 Поставщики
        Schema::table('suppliers', function (Blueprint $table) {
            $table->dropForeign('suppliers_supplier_pricelist_id_foreign');
            $table->dropColumn('supplier_pricelist_id');
        });

        // 3.1 Клиенты
        Schema::table('clients', function (Blueprint $table) {
            // переименуем колонку
            $table->renameColumn('client_pricelist_id', 'pricelist_id');

            // переименуем внешний ключ
            $table->dropForeign('clients_client_pricelist_id_foreign');
            $table->foreign('pricelist_id')
                ->references('id')
                ->on('client_pricelists')
                ->onDelete('set null');
        });


        // 2.2 Цены прайслистов поставщиков
        Schema::dropIfExists('supplier_pricelist_product_prices');

        // 2.1 Цены прайслистов клиентов
        Schema::table('client_pricelist_product_prices', function (Blueprint $table) {
            $table->renameColumn('client_pricelist_id', 'pricelist_id');
        });
        Schema::rename('client_pricelist_product_prices', 'product_prices');


        // 1.2 Прайслисты поставщиков
        Schema::dropIfExists('supplier_pricelists');

        // 1.1 Прайслисты клиентов
        Schema::table('client_pricelists', function (Blueprint $table) {
            $table->renameColumn('assigned_to_euro', 'assigned_to_dollar');
        });
        Schema::rename('client_pricelists', 'pricelists');
    }
}
