<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Currency;
use App\Models\Supply;
use App\Models\Supplier;

class AddCurrencyColumnToSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1
        Schema::table('suppliers', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->nullable()->after('id');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
        });

        // 2
        $this->migrateCurrency();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('suppliers', function (Blueprint $table) {
            $table->dropForeign('suppliers_currency_id_foreign');
            $table->dropColumn('currency_id');
        });
    }

    public function migrateCurrency() {
        // 2 установим начальные значения
        // 2.1
        $supplyCurrenciesData = Supply::query()
            ->select(
                'supplier_id',
                'currency_id',
                DB::raw('max(number) number'),
                DB::raw('COUNT(id) AS currencyCount')
            )
            ->groupBy(['supplier_id', 'currency_id'])
            ->get();

        // 2.2
        $suppliers = Supplier::all();

        $CURRENCY_RUBLE = Currency::CURRENCY_RUBLE;
        $CURRENCY_EURO = Currency::CURRENCY_EURO;

        $supplierCurrenciesIndexed = [];
        foreach ($suppliers as $supplier) {
            $supplierCurrenciesIndexed[$supplier->id] = [
                $CURRENCY_RUBLE => 0,
                $CURRENCY_EURO => 0,
                'supplier' => $supplier
            ];
        }

        // 2.3
        foreach ($supplyCurrenciesData as $supplyCurrenciesDatum) {
            $supplierId = $supplyCurrenciesDatum['supplier_id'];
            $currencyId = $supplyCurrenciesDatum['currency_id'];
            $currencyCount = $supplyCurrenciesDatum['currencyCount'];

            $supplierCurrenciesIndexed[$supplierId][$currencyId] = $currencyCount;
        }

        // 2.4
        foreach ($supplierCurrenciesIndexed as $supplierId => $supplierCurrencies) {
            // если вообще указано евро -- значит считаем в нём
            if ($supplierCurrencies[$CURRENCY_EURO] > 0) {
                $supplierCurrencies['supplier']->currency_id = $CURRENCY_EURO;
            }
            // если евро не было и был рубль - считаем в рублях
            elseif ($supplierCurrencies[$CURRENCY_RUBLE] > 0) {
                $supplierCurrencies['supplier']->currency_id = $CURRENCY_RUBLE;
            }
            // по умолчанию ставим евро
            else {
                $supplierCurrencies['supplier']->currency_id = $CURRENCY_EURO;
            }

            $supplierCurrencies['supplier']->save();
        }
    }
}
