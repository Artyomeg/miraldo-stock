<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Валюты
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias');
            $table->string('system_name');
            $table->unsignedDecimal('ratio_in_rubles', 9, 2);
        });


        // ПродуктыВПоставке
        Schema::table('supplies', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->after('supplier_id');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
        });


        // ПродуктыВСчетах
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->after('client_id');

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplies', function (Blueprint $table) {
            $table->dropForeign('supplies_currency_id_foreign');
            $table->dropColumn('currency_id');
        });


        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign('invoices_currency_id_foreign');
            $table->dropColumn('currency_id');
        });


        Schema::dropIfExists('currencies');
    }
}
