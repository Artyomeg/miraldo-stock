<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplierIdToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Платежи
        Schema::table('payments', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->nullable()->change();
            $table->unsignedInteger('supplier_id')->after('client_id')->nullable();

            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Платежи
        Schema::table('payments', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->change();

            $table->dropForeign('payments_supplier_id_foreign');
            $table->dropColumn('supplier_id');
        });
    }
}
