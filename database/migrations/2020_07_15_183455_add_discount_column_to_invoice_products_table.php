<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountColumnToInvoiceProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Продукты счетов
        Schema::table('invoice-products', function (Blueprint $table) {
            $table->unsignedDecimal('discount', 2, 0)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Продукты счетов
        Schema::table('invoice-products', function (Blueprint $table) {
            $table->dropColumn('discount');
        });
    }
}
