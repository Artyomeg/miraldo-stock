<?php

use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1.1 Добавление таблицы настроек - таблица
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('value');
            $table->timestamps();
            $table->softDeletes();
        });


        // 1.2 Добавление таблицы настроек - инициация данных в таблице
        Setting::create([
            'code' => 'dollar_price',
            'value' => '73,71',
        ]);

        // 2 Добавление поля к прайслистам
        Schema::table('pricelists', function (Blueprint $table) {
            $table->boolean('assigned_to_dollar')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 2 Добавление поля к прайслистам
        Schema::table('pricelists', function (Blueprint $table) {
            $table->dropColumn('assigned_to_dollar');
        });

        // 1 Добавление таблицы настроек
        Schema::dropIfExists('settings');
    }
}
