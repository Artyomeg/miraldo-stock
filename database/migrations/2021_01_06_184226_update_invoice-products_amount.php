<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInvoiceProductsAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('amount', 10, 2)->unsigned(false)->default(0)->change();
        });

        // ПродуктыВСчетах
        // Здесь сырой sql, потому что запрос с названием таблицы с дефисом не взлетел
        if (! app()->environment('testing')) {
            DB::statement('ALTER TABLE `invoice-products` CHANGE price price DECIMAL(10, 2) NOT NULL DEFAULT 0');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // ПродуктыВСчетах
        // Здесь сырой sql, потому что запрос с названием таблицы с дефисом не взлетел
        DB::statement('ALTER TABLE `invoice-products` CHANGE price price DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0');

        // Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedDecimal('amount', 10, 2)->default(0)->change();
        });
    }
}
