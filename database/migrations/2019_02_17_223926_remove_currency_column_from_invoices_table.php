<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCurrencyColumnFromInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            if (! app()->environment('testing')) {
                $table->dropForeign('invoices_currency_id_foreign');
            }
            $table->dropColumn('currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // ПродуктыВСчетах
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('currency_id')->nullable()->after('client_id');

            $table->foreign('currency_id')
                  ->references('id')
                  ->on('currencies')
                  ->onDelete('cascade');
        });
    }
}
