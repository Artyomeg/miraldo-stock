<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Supply;
use App\Models\SupplierInvoice;

class CreateSupplierInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1
        Schema::create('supplier_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplier_id');
            $table->unsignedInteger('currency_id')->nullable();
            $table->date('date');
            $table->unsignedDecimal('amount', 11, 2);
            $table->string('invoice_number')->nullable();
            $table->date('invoice_date')->nullable();
            $table->unsignedInteger('number');
            $table->text('comment')->nullable();

            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers')
                ->onDelete('cascade');
        });

        // 2
        $supplies = Supply::all();

        foreach ($supplies as $supply) {
            $values = $supply->getAttributes();
            $values['amount'] = $supply->amount;
            SupplierInvoice::create($values);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_invoices');
    }
}
