<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNegativePriceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1 client_pricelist_product_prices
        Schema::table('client_pricelist_product_prices', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->unsigned(false)->change();
        });

        // 2 invoice-products
        if (! app()->environment('testing')) {
            DB::statement('ALTER TABLE `invoice-products` CHANGE price price DECIMAL(10, 2) NOT NULL DEFAULT 0');
            DB::statement('ALTER TABLE `invoice-products` CHANGE count count DECIMAL(10, 2) NOT NULL DEFAULT 0');
        }

        // 3 payments
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('amount')->unsigned(false)->change();
        });

        // 4 supplier_pricelist_product_prices
        Schema::table('supplier_pricelist_product_prices', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->unsigned(false)->change();
        });

        // 5 supply-products
        if (! app()->environment('testing')) {
            DB::statement('ALTER TABLE `supply-products` CHANGE price price DECIMAL(10, 2) NOT NULL DEFAULT 0');
            DB::statement('ALTER TABLE `supply-products` CHANGE count count DECIMAL(10, 2) NOT NULL DEFAULT 0');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 5 supply-products
        DB::statement('ALTER TABLE `supply-products` CHANGE price price DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE `supply-products` CHANGE count count DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0');

        // 4 supplier_pricelist_product_prices
        Schema::table('supplier_pricelist_product_prices', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->unsigned(true)->change();
        });

        // 3 payments
        Schema::table('payments', function (Blueprint $table) {
            $table->integer('amount')->unsigned(true)->change();
        });

        // 2 invoice-products
        DB::statement('ALTER TABLE `invoice-products` CHANGE price price DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE `invoice-products` CHANGE count count DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0');

        // 1 client_pricelist_product_prices
        Schema::table('client_pricelist_product_prices', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->unsigned(true)->change();
        });
    }
}
