<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1 Продукты
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 511);
        });

        // 2 Цены на продукты
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->unsignedDecimal('price', 10, 2);

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        // 3 Клиенты
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 511);
            $table->string('phone', 20);
            $table->string('address', 511);
        });

        // 4 Поставщики
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 511);
            $table->string('phone', 20);
            $table->string('address', 511);
        });

        // 5 Поставки
        Schema::create('supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supplier_id');
            $table->timestamp('date');

            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers')
                ->onDelete('cascade');
        });

        // 6 ПродуктыВПоставке
        Schema::create('supply-products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('supply_id');
            $table->unsignedInteger('product_id');
            $table->unsignedDecimal('count', 11, 3)->default(0);
            $table->unsignedDecimal('price', 10, 2)->default(0);

            $table->foreign('supply_id')
                ->references('id')
                ->on('supplies')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });

        // 7 Счета
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->timestamp('date');
            $table->unsignedDecimal('amount', 10, 2)->default(0);
            $table->unsignedDecimal('payed', 10, 2)->default(0);

            $table->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('cascade');
        });

        // 8 ПродуктыВСчетах
        Schema::create('invoice-products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('invoice_id');
            $table->unsignedInteger('product_id');
            $table->unsignedDecimal('price', 10, 2)->default(0);
            $table->unsignedDecimal('count', 11, 3)->default(0);

            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice-products');
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('supply-products');
        Schema::dropIfExists('supplies');
        Schema::dropIfExists('suppliers');
        Schema::dropIfExists('clients');
        Schema::dropIfExists('product_prices');
        Schema::dropIfExists('products');
    }
}
