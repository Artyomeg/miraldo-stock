<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DateForInvoiceAndSupply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 5 Поставки
        Schema::table('supplies', function (Blueprint $table) {
            $table->date('date')->change();
        });

        // 7 Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->date('date')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 7 Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->timestamp('date')->change();
        });

        // 5 Поставки
        Schema::table('supplies', function (Blueprint $table) {
            $table->timestamp('date')->change();
        });
    }
}
