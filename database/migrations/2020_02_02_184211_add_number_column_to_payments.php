<?php

use App\Models\Payment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNumberColumnToPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Платежи
        Schema::table('payments', function (Blueprint $table) {
            $table->unsignedInteger('number');
        });

        $payments = Payment::all();

        if (count($payments) != 0) {
            $newCounter = 1;

            foreach ($payments as $i => $payment) {
                $payment->number = (date('Y', strtotime($payment->date)) == '2020')
                    ? $payment->number = $newCounter++
                    : $payment->id;

                $payment->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 2 Счета
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('number');
        });
    }
}
