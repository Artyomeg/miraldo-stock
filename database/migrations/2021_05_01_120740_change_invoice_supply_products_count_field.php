<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInvoiceSupplyProductsCountField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! app()->environment('testing')) {
            DB::statement('ALTER TABLE `invoice-products` CHANGE count count DECIMAL(10, 3) NOT NULL DEFAULT 0');
            DB::statement('ALTER TABLE `supply-products` CHANGE count count DECIMAL(10, 3) NOT NULL DEFAULT 0');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `supply-products` CHANGE count count DECIMAL(10, 2) NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE `invoice-products` CHANGE count count DECIMAL(10, 2) NOT NULL DEFAULT 0');
    }
}
