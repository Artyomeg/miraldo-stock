<?php

use App\Models\Invoice;
use App\Models\Supply;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNumberColumnToInvoicesAndSupplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 1 Поставки
        Schema::table('supplies', function (Blueprint $table) {
            $table->unsignedInteger('number');
        });

        $supplies = Supply::all();

        if (count($supplies) != 0) {
            $newCounter = 1;

            foreach ($supplies as $i => $supply) {
                $supply->number = (date('Y', strtotime($supply->date)) == '2020')
                    ? $supply->number = $newCounter++
                    : $supply->id;

                $supply->save();
            }
        }


        // 2 Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedInteger('number');
        });

        $invoices = Invoice::all();

        if (count($invoices) != 0) {
            $newCounter = 1;

            foreach ($invoices as $i => $invoice) {
                $invoice->number = (date('Y', strtotime($invoice->date)) == '2020')
                    ? $invoice->number = $newCounter++
                    : $invoice->id;

                $invoice->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // 2 Счета
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('number');
        });

        // 1 Поставки
        Schema::table('supplies', function (Blueprint $table) {
            $table->dropColumn('number');
        });
    }
}
