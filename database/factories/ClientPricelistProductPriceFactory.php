<?php

namespace Database\Factories;

use App\Models\ClientPricelist;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientPricelistProductPriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_pricelist_id' => ClientPricelist::query()->inRandomOrder()->value('id'),
            'product_id' => Product::query()->inRandomOrder()->value('id'),
            'price' => random_int(1, 10) * 100,
        ];
    }
}
