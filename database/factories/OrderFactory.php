<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date'    => $this->faker->dateTimeBetween('+10 days', '+1 year'),
            'amount'  => $this->faker->numberBetween(1, 100) * 100,
            'number'  => $this->faker->numberBetween(1, 10),
            'comment' => $this->faker->realText(),

            'client_id' => Client::query()->inRandomOrder()->value('id')
                ?: Client::factory()->createOne()->id,
        ];
    }
}
