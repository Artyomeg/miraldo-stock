<?php

namespace Database\Factories;

use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'invoice_id' => Invoice::factory()->createOne()->id,
            'product_id'  => Product::factory()->createOne()->id,
            'price'      => random_int(100, 200) * 100,
            'count'      => random_int(10, 20),
            'discount'   => random_int(0, 5),
        ];
    }
}
