<?php

namespace Database\Factories;

use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_id' => Client::factory()->createOne()->id,
            'date'      => now()->addMonth()->format('Y-m-d'),
            'amount'    => random_int(100, 200) * 1000,
            'payed'     => random_int(50, 100) * 1000,
            'number'    => random_int(1000, 2000),
            'comment'   => $this->faker->sentence(100),
        ];
    }
}
