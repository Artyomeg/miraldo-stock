<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          => $this->faker->sentence(3),
            'barcode'       => $this->faker->unique()->uuid(),
            'count'         => random_int(1, 10) * 100,
            'unit'          => random_int(1, 2) === 1 ? 'кг' : 'шт',
            'supplier_code' => $this->faker->word(),
        ];
    }
}
