<?php

namespace Database\Factories;

use App\Models\Currency;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Currency>
 */
class CurrencyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'            => Str::ucfirst($this->faker->unique()->word),
            'alias'           => $this->faker->unique()->currencyCode,
            'system_name'     => Str::lower($this->faker->name),
            'ratio_in_rubles' => random_int(10, 1000) / 100,
        ];
    }
}
