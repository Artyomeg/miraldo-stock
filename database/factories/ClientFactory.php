<?php

namespace Database\Factories;

use App\Models\ClientPricelist;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'     => $this->faker->name(),
            'phone'    => $this->faker->phoneNumber(),
            'address'  => $this->faker->address(),
            'cashless' => $this->faker->boolean(),

            'client_pricelist_id' => ClientPricelist::query()->inRandomOrder()->value('id')
                ?: ClientPricelist::factory()->createOne()->is,
        ];
    }
}
