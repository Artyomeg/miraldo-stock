<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\SupplierPricelist;
use Illuminate\Database\Eloquent\Factories\Factory;

class SupplierPricelistProductPriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'supplier_pricelist_id' => SupplierPricelist::query()->inRandomOrder()->value('id'),
            'product_id' => Product::query()->inRandomOrder()->value('id'),
            'price' => random_int(1, 10) * 100,
        ];
    }
}
