<?php

namespace Database\Seeders;

use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1 clear payment table
        Payment::truncate();

        // 2 fill payment table from invoices
        $invoices = Invoice::all();

        if (count($invoices) == 0) {
            return true;
        }

        $paymentsData = [];

        foreach ($invoices as $invoice) {
            if ($invoice->payed == 0) {
                continue;
            }

            $paymentsData[] = [
                'client_id' => $invoice->client_id,
                'date'      => $invoice->date,
                'amount'    => $invoice->payed,
                'comment'   => "Оплата по счету №{$invoice->id}"
            ];
        }

        Payment::insert($paymentsData);
    }
}
