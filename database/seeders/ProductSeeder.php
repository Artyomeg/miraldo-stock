<?php

namespace Database\Seeders;

use App\Models\ClientPricelist;
use App\Models\Product;
use App\Models\SupplierPricelist;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClientPricelist::factory()
                       ->count(10)
                       ->create();

        SupplierPricelist::factory()
                         ->count(10)
                         ->create();

        Product::factory()
               ->hasProductInvoicePrices(random_int(1, 3))
               ->hasProductSupplyPrices(random_int(1, 3))
               ->count(10)
               ->create();
    }
}
