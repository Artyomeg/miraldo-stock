<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::factory()->createMany([
            ['name' => 'Рубль', 'alias' => 'руб.', 'system_name' => 'ruble', 'ratio_in_rubles' => 1],
            ['name' => 'Евро', 'alias' => '€', 'system_name' => 'euro', 'ratio_in_rubles' => 75],
        ]);
    }
}
