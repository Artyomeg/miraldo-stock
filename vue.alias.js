// const path = require('path');
// const { join } = require('path');

const alias = {
    // services
    // '@components' : join(__dirname, 'src/components/'              ),
    // '@serviceKb'  : join(__dirname, 'src/components/knowledgeBase/'),

    // base
    // '@constants'  : join(__dirname, './src/utils/constants/'       ),
    // '~constants'  : join(__dirname, './src/utils/constants/'       ),
    // '@'           : join(__dirname, 'resources/js/'                        ),
    // '@': path.resolve('./resources/js/'),
    '@': __dirname + '/resources/js'

}

module.exports = alias;
