const aliasObj = require('./vue.alias');

module.exports = {
  resolve: {
    alias: aliasObj,
    extensions: ['.js', '.vue', '.json']
  },
};
