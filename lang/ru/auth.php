<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'       => 'К сожалению, у вас нет прав для этого действия.',
    'throttle'     => 'Слишком много попыток. Пожалуйста, попробуйте снова через :seconds секунж.',
    'login'        => 'Вход',
    'logout'       => 'Выход',
    'loginLink'    => 'Вход',
    'registerLink' => 'Регистрация',
    'loginPage'    => [
        'email'       => 'E-mail',
        'password'    => 'Пароль',
        'rememberMe'  => 'Запомнить меня',
        'loginButton' => 'Войти',
    ],
    'resetPasswordPage' => [
        'header'     => 'Сброс пароля',
        'buttonText' => 'Отправить ссылку для сброса пароля',
    ],
];
