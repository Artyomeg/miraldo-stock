<?php

return [
    'pages'   => [
        'index'  => 'Pagamento',
        'create' => 'Crea pagamento',
        'edit'   => 'Modifica pagamento'
    ],
    'buttons' => [
        'add' => 'Aggiungi pagamento',
    ],
    'fields' => [
        'client_id'   => 'Cliente',
        'supplier_id' => 'Fornitore',
        'amount'      => 'Quantità',
        'date'        => 'Data',
        'comment'     => 'Un commento',
    ],
    'messages' => [
        'created' => 'Pagamento creato',
        'updated' => 'Pagamento aggiornato',
        'deleted' => 'Pagamento eliminato',
    ]
];
