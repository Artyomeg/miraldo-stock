<?php

namespace App\Presenters;

use Illuminate\Support\Facades\DB;
use App\Http\Resources\Report\PaymentResource;
use App\Http\Resources\Report\SupplierInvoiceResource;
use App\Models\Currency;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\Supplier;
use App\Models\SupplierInvoice;

class RevisionBySuppliersPresenter
{
    private $query;
    private $rawDocs;
    private $docsCollection = [];
    private $totalDebt = 0;
    private $selectFrom;
    private $selectTo;
    private $supplierId;
    private $DOC_TYPES = [];

    // 0
    public function __construct($selectParams)
    {
        $this->selectFrom = $selectParams['from'];
        $this->selectTo = $selectParams['to'];
        $this->supplierId = (int)$selectParams['supplierId'];

        $this->DOC_TYPES = [
            'SUPPLIER_INVOICE' => SupplierInvoice::$DOC_TYPE,
            'PAYMENT' => Payment::$DOC_TYPE,
        ];
    }

    // 1
    private function prepareQuery()
    {
        $supplierInvoiceQuery = DB::table('supplier_invoices')
            ->select(
                'amount',
                'supplier_id',
                'date',
                'id',
                'number',
                'comment',
                DB::raw("\"{$this->DOC_TYPES['SUPPLIER_INVOICE']}\" as \"docType\""),

                // чтобы вычислить сумму по поставке в рублях
                'invoice_number',
                'invoice_date',

                'currency_id',
                DB::raw('NULL AS "docDebt"')
            )
            ->where('supplier_id', $this->supplierId);

        $paymentQuery = DB::table('payments')
            ->select(
                'amount',
                'supplier_id',
                'date',
                'id',
                'number',
                'comment',
                DB::raw("\"{$this->DOC_TYPES['PAYMENT']}\" as \"docType\""),

                DB::raw('NULL AS "invoice_number"'),
                DB::raw('NULL AS "invoice_date"'),

                'currency_id',
                DB::raw('NULL AS "docDebt"')
            )
            ->where('supplier_id', $this->supplierId);

        $this->query = $supplierInvoiceQuery
            ->union($paymentQuery)
            ->orderBy('date', 'asc')
            ->orderBy('id', 'asc');
    }

    // 2
    private function getRawDocs()
    {
        $this->rawDocs = $this->query->get();
    }

    // 3
    private function calculateDocDebt()
    {
        $docDebt = 0;

        $supplier = Supplier::find($this->supplierId);
        $euroValue = Setting::getEuroValue();

        foreach ($this->rawDocs as $doc) {
            $amount = round((float)$doc->amount, 2);
            $currentAmount = $doc->docType === $this->DOC_TYPES['PAYMENT']
                ? (float)$amount
                : -(float)$amount;

            // если по поставщику считаем в евро - переведем текущие документы в евро
            if ($supplier->currency_id === Currency::CURRENCY_EURO
                && $doc->currency_id !== Currency::CURRENCY_EURO) {
                $currentAmount = round($currentAmount / $euroValue, 2);
                $doc->euroAmount = round($amount / $euroValue, 2);
            }

            $docDebt += $currentAmount;
            $doc->docDebt = $docDebt;
        }

        $this->totalDebt = $docDebt;
    }

    // 4
    private function calculateDocsCollection()
    {
        foreach ($this->rawDocs as $doc) {
            if (
                strtotime($doc->date) < strtotime($this->selectFrom)
                || strtotime($doc->date) > strtotime($this->selectTo)
            ) {
                continue;
            }

            if ($doc->docType === $this->DOC_TYPES['SUPPLIER_INVOICE']) {
                $supplierInvoice = new SupplierInvoice();

                foreach ((array)$doc as $docAttributeKey => $docAttributeValue) {
                    $supplierInvoice->$docAttributeKey = $docAttributeValue;
                }

                $this->docsCollection[] = new SupplierInvoiceResource($supplierInvoice);
            } else {
                $payment = new Payment();

                foreach ((array)$doc as $docAttributeKey => $docAttributeValue) {
                    $payment->$docAttributeKey = $docAttributeValue;
                }

                $this->docsCollection[] = new PaymentResource($payment);
            }
        }
    }

    public function getDocs()
    {
        $this->prepareQuery();
        $this->getRawDocs();
        $this->calculateDocDebt();
        $this->calculateDocsCollection();

        return [
            'total'          => $this->totalDebt,
            'docsCollection' => $this->docsCollection
        ];
    }
}
