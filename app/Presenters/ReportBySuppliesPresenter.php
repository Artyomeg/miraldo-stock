<?php

namespace App\Presenters;

use App\Models\SupplierInvoice;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\Report\PaymentResource;
use App\Http\Resources\Report\SupplyResource;
use App\Models\Currency;
use App\Models\Payment;
use App\Models\Setting;
use App\Models\Supplier;
use App\Models\Supply;

class ReportBySuppliesPresenter
{
    private $query;
    private $rawDocs;
    private $docsCollection = [];
    private $totalDebt = 0;
    private $selectFrom;
    private $selectTo;
    private $supplierId;
    private $DOC_TYPES = [];

    // 0
    public function __construct($selectParams)
    {
        $this->selectFrom = $selectParams['from'];
        $this->selectTo = $selectParams['to'];
        $this->supplierId = $selectParams['supplierId'];

        $this->DOC_TYPES = [
            'SUPPLY' => Supply::$DOC_TYPE,
            'PAYMENT' => Payment::$DOC_TYPE,
        ];
    }

    // 1
    private function prepareQuery()
    {
        $supplyQuery = DB::table('supplies')->where('supplier_id', $this->supplierId)
            ->select(
                DB::raw('SUM(`supply-products`.`price` * `supply-products`.`count`) AS "amount"'),
                'supplies.supplier_id',
                'supplies.date',
                'supplies.id',
                'supplies.number',
                'supplies.comment',
                DB::raw("\"{$this->DOC_TYPES['SUPPLY']}\" as \"docType\""),

                // чтобы вычислить сумму по поставке в рублях
                'supplies.invoice_number',
                'supplies.invoice_date',

                'supplies.currency_id',
                DB::raw('NULL AS "docDebt"')
            )
            ->leftJoin('supply-products', 'supplies.id', '=', 'supply-products.supply_id')
            ->groupBy(['supplies.id']);

        $paymentQuery = DB::table('payments')
            ->select(
                'amount',
                'supplier_id',
                'date',
                'id',
                'number',
                'comment',
                DB::raw("\"{$this->DOC_TYPES['PAYMENT']}\" as \"docType\""),

                DB::raw('NULL AS "invoice_number"'),
                DB::raw('NULL AS "invoice_date"'),

                'currency_id',
                DB::raw('NULL AS "docDebt"')
            )
            ->where('supplier_id', $this->supplierId);

        $this->query = $supplyQuery
            ->union($paymentQuery)
            ->orderBy('date', 'asc')
            ->orderBy('id', 'asc');
    }

    // 2
    private function getRawDocs()
    {
        $this->rawDocs = $this->query->get();
    }

    // 3
    private function calculateDocDebt()
    {
        $docDebt = 0;

        $supplier = Supplier::find($this->supplierId);
        $euroValue = Setting::getEuroValue();

        foreach ($this->rawDocs as $doc) {
            $amount = round((float)$doc->amount, 2);
            $currentAmount = $doc->docType === $this->DOC_TYPES['PAYMENT']
                ? (float)$amount
                : -(float)$amount;

            // если по поставщику считаем в евро - переведем текущие документы в евро
            if ($supplier->currency_id === Currency::CURRENCY_EURO
                && $doc->currency_id !== Currency::CURRENCY_EURO) {
                $currentAmount = round($currentAmount / $euroValue, 2);
                $doc->euroAmount = round($amount / $euroValue, 2);
            }

            $docDebt += $currentAmount;
            $doc->docDebt = $docDebt;
        }

        $this->totalDebt = $docDebt;
    }

    // 4
    private function calculateDocsCollection()
    {
        foreach ($this->rawDocs as $doc) {
            if (
                strtotime($doc->date) < strtotime($this->selectFrom)
                || strtotime($doc->date) > strtotime($this->selectTo)
            ) {
                continue;
            }

            if ($doc->docType === $this->DOC_TYPES['SUPPLY']) {
                $supply = new Supply();

                foreach ((array)$doc as $docAttributeKey => $docAttributeValue) {
                    $supply->$docAttributeKey = $docAttributeValue;
                }

                $this->docsCollection[] = new SupplyResource($supply);
            }
        }
    }

    public function getDocs()
    {
        $this->prepareQuery();
        $this->getRawDocs();
        $this->calculateDocDebt();
        $this->calculateDocsCollection();

        return [
            'total'          => $this->totalDebt,
            'docsCollection' => $this->docsCollection
        ];
    }
}
