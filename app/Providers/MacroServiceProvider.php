<?php

namespace App\Providers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Response;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(ResponseFactory $response)
    {
        $response
            ->macro('api', function (
                array $data = [],
                bool $success = true,
                int $code = Response::HTTP_OK
            ) use ($response) {
                return $response->make(
                    compact('success', 'data'),
                    $code
                );
            });
    }
}
