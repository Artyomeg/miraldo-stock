<?php

namespace App\Providers;

use App\Listeners\Auth as AuthListeners;
use Illuminate\Auth\Events as AuthEvents;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        AuthEvents\Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        AuthEvents\Login::class => [
            AuthListeners\Login::class,
        ],

        AuthEvents\Failed::class => [
            AuthListeners\Failed::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
