<?php declare(strict_types = 1);

namespace App\Contracts;

use Illuminate\Database\Eloquent\Builder;

interface Repository
{
    public function query(): Builder;
}
