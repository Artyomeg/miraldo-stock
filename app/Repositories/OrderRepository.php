<?php declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repository;
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class OrderRepository implements Repository
{
    /**
     * @return Paginator
     */
    public function getIndexCollection()
    {
        $query = Order::query()
                      ->with('client')
                      ->latest('date')
                      ->latest('id');

        return $query->paginate();
    }

    /**
     * @param  Order  $order
     * @param  array  $data
     *
     * @return void
     */
    public function storeProducts(Order $order, array $data = [])
    {
        // переберем присланные старые
        $order->orderProducts->each(function (OrderProduct $order_product) use ($order, $data) {
            // удаляем помеченные на удаление
            if (! isset($data['old'][$order_product->id])) {
                $order_product->delete();

                return;
            }

            // если нуждается в обновлении - обновим
            if (
                (int) $data['old'][$order_product->id]['productId'] !== $order_product->product_id
                || $data['old'][$order_product->id]['comment'] !== $order_product->comment
            ) {
                // если тот же товар
                $order_product->update([
                    'comment' => $data['old'][$order_product->id]['comment'] ?? null,
                    'product_id' => $data['old'][$order_product->id]['productId'],
                ]);
            }
        });

        // переберем присланные новые - добавляем все
        collect($data['new'] ?? [])
            ->each(function (array $new_order_product) use ($order) {
                OrderProduct::query()->create([
                    'product_id' => $new_order_product['productId'],
                    'order_id'   => $order->id,
                    'comment'    => $new_order_product['comment'] ?? null,
                ]);
            });
    }

    public function query(): Builder
    {
        return Order::newModelQuery();
    }
}
