<?php declare(strict_types = 1);

namespace App\Repositories;

use App\Contracts\Repository;
use App\Models\Activity;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;

class ActivityRepository implements Repository
{
    /**
     * @param  string|null  $model_type
     * @param  int|null  $model_id
     * @param  string|null  $description
     *
     * @return Paginator
     */
    public function searchActivities(string $model_type = null, int $model_id = null, string $description = null)
    {
        $query = $this->query()
                      ->with('user')
                      ->when($model_type, function ($query, $model_type) {
                          $query->where('subject_type', $model_type);
                      })
                      ->when($model_id, function ($query, $model_id) {
                          $query->where('subject_id', $model_id);
                      })
                      ->when($description, function ($query, $description) {
                          $query->where('description', $description);
                      })
                      ->latest('created_at');

        return $query->paginate();
    }

    public function query(): Builder
    {
        return Activity::query();
    }
}
