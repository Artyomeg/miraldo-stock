<?php

namespace App\Listeners\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Failed as Event;
use Illuminate\Support\Arr;

class Failed
{
    /**
     * @param  Event  $event
     *
     * @return void
     */
    public function handle(Event $event)
    {
        $properties = request()->all();
        Arr::forget($properties, '_token');

        activity()
            ->on(new User)
            ->withProperties($properties)
            ->log('login.failed');
    }
}
