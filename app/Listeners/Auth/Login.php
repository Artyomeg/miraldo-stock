<?php

namespace App\Listeners\Auth;

use Illuminate\Auth\Events\Login as Event;

class Login
{
    /**
     * @param  Event  $event
     *
     * @return void
     */
    public function handle(Event $event)
    {
        activity()
            ->on(auth()->user())
            ->log('login.succeed');
    }
}
