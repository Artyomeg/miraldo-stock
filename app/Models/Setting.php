<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $code
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Setting euroPrice()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Query\Builder|Setting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|Setting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Setting withoutTrashed()
 * @method static \Database\Factories\SettingFactory factory(...$parameters)
 * @mixin \Eloquent
 */
class Setting extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'value',
    ];

    public function scopeEuroPrice($query)
    {
        return $query->where('code', 'dollar_price');
    }

    public static function formatEuro($rawValue)
    {
        return str_replace(',', '.', $rawValue);
    }

    public static function updateDollarAssignedPricelists($oldDollarPrice, $newDollarPrice)
    {
        $pricelists = ClientPricelist::dollarAssigned()->get();
        $oldDollarPrice = Setting::formatEuro($oldDollarPrice);

        foreach ($pricelists as $pricelist) {
            foreach ($pricelist->productPrices as $productPrices) {
                $productPrices->price = round($productPrices->price * $newDollarPrice / $oldDollarPrice, -1);
                $productPrices->save();
            }
        }
    }

    public static function getEuroValue()
    {
        return (float) self::euroPrice()->latest('id')->get()[0]->value;
    }
}
