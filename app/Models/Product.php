<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string|null $barcode
 * @property string|null $supplier_code
 * @property string $name
 * @property float $count
 * @property string $unit Единица измерения
 * @property-read string $search_string
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientPricelistProductPrice[] $productInvoicePrices
 * @property-read int|null $product_invoice_prices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SupplierPricelistProductPrice[] $productSupplyPrices
 * @property-read int|null $product_supply_prices_count
 * @method static \Database\Factories\ProductFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereBarcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSupplierCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUnit($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'barcode',
        'barcode_kg_chars',
        'name',
        'count',
        'unit',
        'supplier_code',
    ];

    protected $casts = [
        'count' => 'float',
    ];

    protected $appends = [
        'search_string',
    ];

    /**
     * @return HasMany
     */
    public function productInvoicePrices()
    {
        return $this->hasMany(ClientPricelistProductPrice::class);
    }

    /**
     * @return HasMany
     */
    public function productSupplyPrices()
    {
        return $this->hasMany(SupplierPricelistProductPrice::class);
    }

    /**
     * @return array
     */
    public function getFullProductPrice()
    {
        $pricelists = ClientPricelist::all();

        $productPriceByIds = [];

        foreach ($this->productInvoicePrices as $productPrice) {
            $productPriceByIds[$productPrice->client_pricelist_id] = $productPrice;
        }

        $fullProductPriceByIds = [];

        foreach ($pricelists as $pricelist) {
            $fullProductPriceByIds[$pricelist->id] = [
                'pricelist' => $pricelist,
                'price'     => isset($productPriceByIds[$pricelist->id])
                    ? $productPriceByIds[$pricelist->id]->price
                    : 0
            ];
        }

        return $fullProductPriceByIds;
    }

    /**
     * @return array
     */
    public function getSupplyProductPrices()
    {
        $pricelists = SupplierPricelist::all();

        $productPriceByIds = [];

        foreach ($this->productSupplyPrices as $productPrice) {
            $productPriceByIds[$productPrice->supplier_pricelist_id] = $productPrice;
        }

        $fullProductPriceByIds = [];

        foreach ($pricelists as $pricelist) {
            $fullProductPriceByIds[$pricelist->id] = [
                'pricelist' => $pricelist,
                'price'     => isset($productPriceByIds[$pricelist->id])
                    ? $productPriceByIds[$pricelist->id]->price
                    : 0
            ];
        }

        return $fullProductPriceByIds;
    }

    /**
     * @return string
     */
    public function getSearchStringAttribute()
    {
        return $this->supplier_code . $this->name;
    }
}
