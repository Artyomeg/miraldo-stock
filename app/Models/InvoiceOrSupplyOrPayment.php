<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\InvoiceOrSupplyOrPayment
 *
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupplyOrPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupplyOrPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupplyOrPayment query()
 * @mixin \Eloquent
 */
class InvoiceOrSupplyOrPayment extends Model
{
    public $timestamps = false;

    public static function getNewNumber($year = null)
    {
        if (!$year) {
            $year = date('Y');
        }

        $lastDoc = self::query()
            ->select(DB::raw('max(number) number'))
            ->whereYear('date', '=', $year)
            ->first();

        return ($lastDoc)
            ? $lastDoc->number + 1
            : 1;
    }
}
