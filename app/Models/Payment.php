<?php

namespace App\Models;

use App\Contracts\HasDocType;
use App\Traits\HasActivityLogs;

/**
 * App\Models\Payment
 *
 * @property int $id
 * @property int|null $currency_id
 * @property int|null $client_id
 * @property int|null $supplier_id
 * @property string $date
 * @property string $amount
 * @property string|null $comment
 * @property int $number
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Client|null $client
 * @property-read mixed $ruble_amount
 * @property-read \App\Models\Supplier|null $supplier
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payment whereSupplierId($value)
 * @mixin \Eloquent
 */
class Payment extends InvoiceOrSupplyOrPayment implements HasDocType
{
    use HasActivityLogs;

    protected $fillable = [
        'client_id',
        'supplier_id',
        'amount',
        'date',
        'comment',
        'number',
        'currency_id'
    ];
    public static $DOC_TYPE = 'payment';

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function getDocTitle()
    {
        return 'Оплата № ' . $this->number . ' от ' . date("d.m.Y", strtotime($this->date));
    }

//    public function getDocDebtAttribute()
//    {
//        $docDebt = 0;
//
//        $invoiceQuery = Invoice::where('client_id', $this->client_id)
//            ->select(
//                'id',
//                DB::raw('"invoice" as "type"'),
//                'date',
//                'amount'
//            );
//
//        $paymentQuery = Payment::where('client_id', $this->client_id)
//            ->select(
//                'id',
//                DB::raw('"payment" as "type"'),
//                'date',
//                'amount'
//            );
//
//        $docsQuery = $invoiceQuery
//            ->union($paymentQuery)
//            ->orderBy('date', 'desc')
//            ->orderBy('id', 'desc');
//
//        $sql = getSql($docsQuery);
//        $docs = $docsQuery->get();
//
//        foreach ($docs as $doc) {
//            $docDebt += $doc->type == 'payment'
//                ? $doc->amount
//                : -$doc->amount;
//
//            if ($doc->type == 'invoice' && $doc->id == $this->id) {
//                break;
//            }
//        }
//
//        return $docDebt;
//    }

    public function getRubleAmountAttribute()
    {
        return $this->amount;
    }
}
