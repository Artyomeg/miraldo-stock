<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Client
 *
 * @property int $id
 * @property int|null $client_pricelist_id
 * @property string $name
 * @property string $phone
 * @property string $address
 * @property int $cashless
 * @property boolean $is_archived
 * @property-read mixed $debt
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Payment[] $payments
 * @property-read int|null $payments_count
 * @property-read \App\Models\ClientPricelist|null $pricelist
 * @method static \Database\Factories\ClientFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Client newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Client query()
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereCashless($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereClientPricelistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Client wherePhone($value)
 * @mixin \Eloquent
 */
class Client extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['client_pricelist_id', 'name', 'phone', 'address', 'cashless', 'is_archived'];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function pricelist()
    {
        return $this->belongsTo(ClientPricelist::class, 'client_pricelist_id');
    }

    public function scopeIsNotArchived($query)
    {
        return $query->where('is_archived', 0);
    }

    public function getDebtAttribute()
    {
        $debt = 0;

        if ($this->invoices) {
            foreach ($this->invoices as $invoice) {
                $debt += $invoice->amount;
            }
        }

        if ($this->payments) {
            foreach ($this->payments as $payment) {
                $debt -= $payment->amount;
            }
        }

        return $debt;
    }
}
