<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupplierPricelist
 *
 * @property int $id
 * @property string $name
 * @property bool $assigned_to_euro
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SupplierPricelistProductPrice[] $productPrices
 * @property-read int|null $product_prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist dollarAssigned()
 * @method static \Database\Factories\SupplierPricelistFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist whereAssignedToEuro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelist whereName($value)
 * @mixin \Eloquent
 */
class SupplierPricelist extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'assigned_to_euro'];

    protected $casts = [
        'assigned_to_euro' => 'bool',
    ];

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function productPrices()
    {
        return $this->hasMany(SupplierPricelistProductPrice::class);
    }

    public function scopeDollarAssigned($query)
    {
        return $query->where('assigned_to_euro', true);
    }
}
