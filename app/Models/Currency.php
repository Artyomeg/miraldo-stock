<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string $system_name
 * @property string $ratio_in_rubles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceProduct[] $invoiceProducts
 * @property-read int|null $invoice_products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SupplyProduct[] $supplyProducts
 * @property-read int|null $supply_products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|Currency whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Currency whereRatioInRubles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Currency whereSystemName($value)
 * @method static \Database\Factories\CurrencyFactory factory(...$parameters)
 * @mixin \Eloquent
 */
class Currency extends Model
{
    use HasFactory;

    public const CURRENCY_RUBLE = 1;

    public const CURRENCY_EURO = 2;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'alias',
        'system_name',
        'ratio_in_rubles',
    ];

    protected $casts = [
        'ratio_in_rubles' => 'decimal:4',
    ];

    public function invoiceProducts()
    {
        return $this->hasMany(InvoiceProduct::class);
    }

    public function supplyProducts()
    {
        return $this->hasMany(SupplyProduct::class);
    }
}
