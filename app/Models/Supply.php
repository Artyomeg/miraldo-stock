<?php

namespace App\Models;

use App\Contracts\HasDocType;
use App\Traits\HasActivityLogs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * App\Models\Supply
 *
 * @property int $id
 * @property int $supplier_id
 * @property int $currency_id
 * @property string $date
 * @property int $number
 * @property string|null $invoice_number
 * @property string|null $invoice_date
 * @property string|null $comment
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Currency $currency
 * @property-read mixed $amount
 * @property-read mixed $product_ruble_amount
 * @property-read mixed $ruble_amount
 * @property-read \App\Models\Supplier $supplier
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SupplyProduct[] $supplyProducts
 * @property-read int|null $supply_products_count
 * @property-read int $ts
 * @method static \Illuminate\Database\Eloquent\Builder|Supply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Supply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Supply query()
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereInvoiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Supply whereSupplierId($value)
 * @mixin \Eloquent
 */
class Supply extends InvoiceOrSupply implements HasDocType
{
    use HasActivityLogs;

    protected $fillable = [
        'supplier_id',
        'currency_id',
        'date',
        'number',
        'invoice_number',
        'invoice_date',
        'comment',
    ];

    public static $DOC_TYPE = 'supply';

    // TODO: проверить название переменной - возможно здесь уместно capitalize
    const DOC_NAME_CAPITALIZE = 'Поставка';

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function supplyProducts()
    {
        return $this->hasMany(SupplyProduct::class);
    }

    public function getProductRubleAmountAttribute()
    {
        $productAmount =
            isset ($this->price)
                ? (float) $this->price * $this->currency->ratio_in_rubles * (float) $this->count
                : 0;

        return $productAmount;
    }

    public function getAmountAttribute()
    {
        $amount = 0;

        if ($this->supplyProducts) {
            foreach ($this->supplyProducts as $supplyProduct) {
                $amount += $supplyProduct->price * $supplyProduct->count;
            }
        }

        return $amount;
    }

    public function getRubleAmountAttribute()
    {
        return $this->amount * $this->currency->ratio_in_rubles;
    }

    public function updateSupplyProducts($requestSupplyProductsData)
    {
        // переберем присланные старые
        foreach ($this->supplyProducts as $supplyProduct) {
            // удаляем помеченные на удаление
            if (! isset($requestSupplyProductsData['old'][$supplyProduct->id])) {
                // вычитаем из товара количество поставки и сохраняем его
                $supplyProduct->product->count -= $supplyProduct->count;
                $supplyProduct->product->save();
                // удаляем товар-в-поставке
                $supplyProduct->delete();
            } // если нуждается в обновлении - обновим
            else {
                if (
                    ($requestSupplyProductsData['old'][$supplyProduct->id]['productId'] != $supplyProduct->product_id)
                    || ($requestSupplyProductsData['old'][$supplyProduct->id]['price'] != $supplyProduct->price)
                    || ($requestSupplyProductsData['old'][$supplyProduct->id]['count'] != $supplyProduct->count)
                ) {
                    // если тот же товар
                    if ($supplyProduct->product_id == $requestSupplyProductsData['old'][$supplyProduct->id]['productId']) {
                        // фиксируем разницу в количестве у товара
                        $supplyProduct->product->count += $requestSupplyProductsData['old'][$supplyProduct->id]['count'] - $supplyProduct->count;
                        $supplyProduct->product->save();

                        $supplyProduct->product_id = $requestSupplyProductsData['old'][$supplyProduct->id]['productId'];
                        $supplyProduct->price = $requestSupplyProductsData['old'][$supplyProduct->id]['price'];
                        $supplyProduct->count = $requestSupplyProductsData['old'][$supplyProduct->id]['count'];

                        $supplyProduct->save();
                    } // если другой товар
                    else {
                        // вычитаем из товара количество поставки и сохраняем его
                        $supplyProduct->product->count -= $supplyProduct->count;
                        $supplyProduct->product->save();
                        // удаляем товар-в-поставке
                        $supplyProduct->delete();

                        // создаем запись SupplyProduct
                        $supplyProduct = SupplyProduct::create([
                            'product_id' => $requestSupplyProductsData['old'][$supplyProduct->id]['productId'],
                            'supply_id'  => $this->id,
                            'price'      => $requestSupplyProductsData['old'][$supplyProduct->id]['price'],
                            'count'      => $requestSupplyProductsData['old'][$supplyProduct->id]['count'],
                        ]);

                        $supplyProduct->product->count += $supplyProduct->count;
                        $supplyProduct->product->save();
                    }
                }
            }
        }

        // переберем присланные новые - добавляем все
        if (isset($requestSupplyProductsData['new'])) {
            foreach ($requestSupplyProductsData['new'] as $requestSupplyProduct) {
                // создаем запись SupplyProduct
                $supplyProduct = SupplyProduct::create([
                    'product_id' => $requestSupplyProduct['productId'],
                    'supply_id'  => $this->id,
                    'price'      => $requestSupplyProduct['price'],
                    'count'      => $requestSupplyProduct['count'],
                ]);

                $supplyProduct->product->count += $supplyProduct->count;
                $supplyProduct->product->save();
            }
        }

        return true;
    }

    public function ts(): Attribute
    {
        return new Attribute(
            get: fn() => Carbon::parse($this->date)->getTimestamp()
        );
    }
}
