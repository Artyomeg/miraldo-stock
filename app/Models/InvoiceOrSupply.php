<?php

namespace App\Models;

/**
 * App\Models\InvoiceOrSupply
 *
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceOrSupply query()
 * @mixin \Eloquent
 */
class InvoiceOrSupply extends InvoiceOrSupplyOrPayment
{
    // TODO: проверить название переменной - возможно здесь уместно capitalize
    const DOC_NAME_CAPITALIZE = 'Поставка';

    public function getDocTitle()
    {
        return static::DOC_NAME_CAPITALIZE . ' № ' . $this->number . ' от ' . date("d.m.Y", strtotime($this->date));
    }
}
