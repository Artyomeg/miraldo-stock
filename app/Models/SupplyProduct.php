<?php

namespace App\Models;

use App\Traits\HasActivityLogs;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupplyProduct
 *
 * @property int $id
 * @property int $supply_id
 * @property int $product_id
 * @property string $price
 * @property string $count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Currency $currency
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\Supply $supply
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplyProduct whereSupplyId($value)
 * @mixin \Eloquent
 */
class SupplyProduct extends Model
{
    use HasActivityLogs;

    public $timestamps = false;

    protected $fillable = ['product_id', 'supply_id', 'count', 'price'];
    protected $table = 'supply-products';

    public function supply()
    {
        return $this->belongsTo(Supply::class, 'supply_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
