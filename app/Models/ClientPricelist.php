<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClientPricelist
 *
 * @property int $id
 * @property string $name
 * @property int $assigned_to_euro
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ClientPricelistProductPrice[] $productPrices
 * @property-read int|null $product_prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist dollarAssigned()
 * @method static \Database\Factories\ClientPricelistFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist whereAssignedToEuro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelist whereName($value)
 * @mixin \Eloquent
 */
class ClientPricelist extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'assigned_to_euro'];

    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function productPrices()
    {
        return $this->hasMany(ClientPricelistProductPrice::class);
    }

    public function scopeDollarAssigned($query)
    {
        return $query->where('assigned_to_euro', true);
    }
}
