<?php

namespace App\Models;

use App\Traits\HasActivityLogs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupplierPricelistProductPrice
 *
 * @property int $id
 * @property int $supplier_pricelist_id
 * @property int $product_id
 * @property string $price
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\SupplierPricelist $supplierPricelist
 * @method static \Database\Factories\SupplierPricelistProductPriceFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierPricelistProductPrice whereSupplierPricelistId($value)
 * @mixin \Eloquent
 */
class SupplierPricelistProductPrice extends Model
{
    use HasActivityLogs, HasFactory;

    public $timestamps = false;

    protected $fillable = ['supplier_pricelist_id', 'product_id', 'price'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function supplierPricelist()
    {
        return $this->belongsTo(SupplierPricelist::class, 'supplier_pricelist_id');
    }
}
