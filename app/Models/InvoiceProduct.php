<?php

namespace App\Models;

use App\Traits\HasActivityLogs;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\InvoiceProduct
 *
 * @property int $id
 * @property int $invoice_id
 * @property int $product_id
 * @property string $price
 * @property string $count
 * @property string $discount
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Currency|null $currency
 * @property-read \App\Models\Invoice $invoice
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|InvoiceProduct whereProductId($value)
 * @mixin \Eloquent
 */
class InvoiceProduct extends Model
{
    use HasFactory, HasActivityLogs;

    public $timestamps = false;

    protected $fillable = ['product_id', 'invoice_id', 'count', 'price', 'discount'];

    protected $table = 'invoice-products';

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return Attribute
     */
    public function total(): Attribute
    {
        return new Attribute(
            get: fn() => round(
                num: $this->price * $this->count * (100 - $this->discount) / 100,
                mode: PHP_ROUND_HALF_UP
            )
        );
    }
}
