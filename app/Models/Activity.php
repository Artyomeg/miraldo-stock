<?php declare(strict_types = 1);

namespace App\Models;

class Activity extends \Spatie\Activitylog\Models\Activity
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (self $activity) {
            $activity->fill([
                'ip'        => request()->ip(),
                'useragent' => request()->userAgent(),
            ]);
        });
    }

    public function user()
    {
        return $this->hasOne(ActivityLogUser::class, 'ip', 'ip');
    }
}
