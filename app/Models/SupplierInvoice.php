<?php

namespace App\Models;

use App\Contracts\HasDocType;
use App\Traits\HasActivityLogs;
use App\Traits\HasNumber;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupplierInvoice
 *
 * @property int $id
 * @property int $supplier_id
 * @property int|null $currency_id
 * @property string $date
 * @property string $amount
 * @property string|null $invoice_number
 * @property string|null $invoice_date
 * @property int $number
 * @property string|null $comment
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Currency|null $currency
 * @property-read mixed $ruble_amount
 * @property-read \App\Models\Supplier $supplier
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereInvoiceDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInvoice whereSupplierId($value)
 * @mixin \Eloquent
 */
class SupplierInvoice extends Model implements HasDocType
{
    use HasNumber, HasActivityLogs;

    protected $fillable = [
        'currency_id',
        'supplier_id',
        'date',
        'amount',
        'invoice_number',
        'invoice_date',
        'number',
        'comment'
    ];

    public $timestamps = false;

    public $DOC_NAME_CAPITALIZE = 'Счет';

    public static $DOC_TYPE = 'supplier-invoice';

    protected $casts = [
        'date'   => 'date',
        'amount' => 'float',
        'number' => 'integer',
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function getRubleAmountAttribute()
    {
        return $this->amount;
    }
}
