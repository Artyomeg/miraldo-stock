<?php

namespace App\Models;

class ControlMode
{
    private static $cookieKey = 'controlMode';

    public static function set()
    {
        \Cookie::queue(\Cookie::make(self::$cookieKey, 1));
    }

    public static function unset()
    {
        \Cookie::queue(\Cookie::forget(self::$cookieKey));
    }

    public static function checkModeIsControl()
    {
        return \Cookie::get(self::$cookieKey);
    }
}
