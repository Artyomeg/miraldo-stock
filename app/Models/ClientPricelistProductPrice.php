<?php

namespace App\Models;

use App\Traits\HasActivityLogs;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ClientPricelistProductPrice
 *
 * @property int $id
 * @property int $client_pricelist_id
 * @property int $product_id
 * @property string $price
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\ClientPricelist $clientPricelist
 * @property-read \App\Models\Product $product
 * @method static \Database\Factories\ClientPricelistProductPriceFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice whereClientPricelistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClientPricelistProductPrice whereProductId($value)
 * @mixin \Eloquent
 */
class ClientPricelistProductPrice extends Model
{
    use HasActivityLogs, HasFactory;

    public $timestamps = false;

    protected $fillable = ['client_pricelist_id', 'product_id', 'price'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function clientPricelist()
    {
        return $this->belongsTo(ClientPricelist::class, 'client_pricelist_id');
    }
}
