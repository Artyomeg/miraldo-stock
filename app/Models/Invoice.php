<?php

namespace App\Models;

use App\Traits\HasActivityLogs;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property int $client_id
 * @property string $date
 * @property string $amount
 * @property string $payed
 * @property int $number
 * @property string|null $comment
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Models\Client $client
 * @property-read mixed $payed_status
 * @property-read mixed $product_ruble_amount
 * @property-read mixed $ruble_amount
 * @property-read mixed $ruble_payed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceProduct[] $invoiceProducts
 * @property-read int|null $invoice_products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\InvoiceProduct[] $productPrices
 * @property-read int|null $product_prices_count
 * @property-read int $ts
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Invoice wherePayed($value)
 * @mixin \Eloquent
 */
class Invoice extends InvoiceOrSupply
{
    use HasFactory, HasActivityLogs;

    protected $fillable = ['client_id', 'date', 'amount', 'payed', 'number', 'discount', 'comment'];

    const DOC_NAME_CAPITALIZE = 'Счет';

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function getProductRubleAmountAttribute()
    {
        $productAmount =
            isset ($this->price)
                ? (float) $this->price * (float) $this->count * (100 - $this->discount) / 100
                : 0;

        return $productAmount;
    }

    public function getRubleAmountAttribute()
    {
        return $this->amount;
    }

    public function getRublePayedAttribute()
    {
        return $this->payed;
    }

    public function invoiceProducts()
    {
        return $this->hasMany(InvoiceProduct::class);
    }

    public function productPrices()
    {
        return $this->hasMany(InvoiceProduct::class);
    }

    public function getPayedStatusAttribute()
    {
        // эта конструкция не работает через тернарный оператор - непонятно почему
        if ((float) $this->payed == (float) $this->amount) {
            $payedStatus = 'Да';
        } else {
            $payedStatus = ((float) $this->payed == 0)
                ? 'Нет'
                : 'Частично (' . fineNumber($this->payed, false) . ' руб.)';
        }

        return $payedStatus;
    }

//    public function getDocDebtAttribute()
//    {
//        $docDebt = 0;
//
//        $paymentQuery = Payment::where('client_id', $this->client_id)
//            ->select(
//                'id',
//                DB::raw('"payment" as "type"'),
//                'date',
//                'amount'
//            );
//
//        $docsQuery = Invoice::where('client_id', $this->client_id)
//            ->select(
//                'id',
//                DB::raw('"invoice" as "type"'),
//                'date',
//                'amount'
//            )
//            ->union($paymentQuery)
//            ->orderBy('date', 'desc')
//            ->orderBy('id', 'desc');
//
//        $sql = getSql($docsQuery);
//        $docs = $docsQuery->get();
//
//        foreach ($docs as $doc) {
//            $docDebt += $doc->type == 'invoice'
//                ? $doc->amount
//                : -$doc->amount;
//
//            if ($doc->type == 'invoice' && $doc->id == $this->id) {
//                break;
//            }
//        }
//
//        return $docDebt;
//    }

    public function updateInvoiceProducts($requestInvoiceProductsData)
    {
        // переберем присланные старые
        foreach ($this->invoiceProducts as $invoiceProduct) {
            // удаляем помеченные на удаление
            if (! isset($requestInvoiceProductsData['old'][$invoiceProduct->id])) {
                // добавляем к товару количество товара-в-счете и сохраняем его
                $invoiceProduct->product->count += $invoiceProduct->count;
                $invoiceProduct->product->save();
                // удаляем товар-в-счете
                $invoiceProduct->delete();
            } // если нуждается в обновлении - обновим
            else {
                if (
                    ($requestInvoiceProductsData['old'][$invoiceProduct->id]['productId'] != $invoiceProduct->product_id)
                    || ($requestInvoiceProductsData['old'][$invoiceProduct->id]['price'] != $invoiceProduct->price)
                    || ($requestInvoiceProductsData['old'][$invoiceProduct->id]['count'] != $invoiceProduct->count)
                    || ($requestInvoiceProductsData['old'][$invoiceProduct->id]['discount'] != $invoiceProduct->discount)
                ) {
                    // если тот же товар
                    if ($invoiceProduct->product_id == $requestInvoiceProductsData['old'][$invoiceProduct->id]['productId']) {
                        // фиксируем разницу в количестве у товара
                        $invoiceProduct->product->count -= $requestInvoiceProductsData['old'][$invoiceProduct->id]['count'] - $invoiceProduct->count;
                        $invoiceProduct->product->save();

                        $invoiceProduct->product_id = $requestInvoiceProductsData['old'][$invoiceProduct->id]['productId'];
                        $invoiceProduct->price = $requestInvoiceProductsData['old'][$invoiceProduct->id]['price'];
                        $invoiceProduct->discount = $requestInvoiceProductsData['old'][$invoiceProduct->id]['discount'];
                        $invoiceProduct->count = $requestInvoiceProductsData['old'][$invoiceProduct->id]['count'];

                        $invoiceProduct->save();
                    } // если другой товар
                    else {
                        // вычитаем из товара количество поставки и сохраняем его
                        $invoiceProduct->product->count += $invoiceProduct->count;
                        $invoiceProduct->product->save();
                        // удаляем товар-в-поставке
                        $invoiceProduct->delete();

                        // создаем запись InvoiceProduct
                        $invoiceProduct = InvoiceProduct::create([
                            'product_id' => $requestInvoiceProductsData['old'][$invoiceProduct->id]['productId'],
                            'invoice_id' => $this->id,
                            'price'      => $requestInvoiceProductsData['old'][$invoiceProduct->id]['price'],
                            'discount'   => $requestInvoiceProductsData['old'][$invoiceProduct->id]['discount'],
                            'count'      => $requestInvoiceProductsData['old'][$invoiceProduct->id]['count'],
                        ]);

                        $invoiceProduct->product->count -= $invoiceProduct->count;
                        $invoiceProduct->product->save();
                    }
                }
            }
        }

        // переберем присланные новые - добавляем все
        if (isset($requestInvoiceProductsData['new'])) {
            foreach ($requestInvoiceProductsData['new'] as $requestInvoiceProduct) {
                // создаем запись InvoiceProduct
                $invoiceProduct = InvoiceProduct::create([
                    'product_id' => $requestInvoiceProduct['productId'],
                    'invoice_id' => $this->id,
                    'price'      => $requestInvoiceProduct['price'],
                    'discount'   => $requestInvoiceProduct['discount'],
                    'count'      => $requestInvoiceProduct['count'],
                ]);

                $invoiceProduct->product->count -= $invoiceProduct->count;
                $invoiceProduct->product->save();
            }
        }
    }

    public function ts(): Attribute
    {
        return new Attribute(
            get: fn() => Carbon::parse($this->date)->getTimestamp()
        );
    }
}
