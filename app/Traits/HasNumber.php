<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait HasNumber
{
    /**
     * @return string
     */
    public function getDocTitle()
    {
        return sprintf(
            "%s № %d от %s",
            $this->DOC_NAME_CAPITALIZE,
            $this->number,
            $this->date->format('d.m.Y')
        );
    }

    /**
     * @param  int|null  $year
     *
     * @return int
     */
    public static function getNewNumber($year = null)
    {
        $lastDoc = self::query()
                       ->select(DB::raw('max(number) as number'))
                       ->whereYear('date', '=', $year ?: now()->year)
                       ->first();

        return $lastDoc
            ? $lastDoc->number + 1
            : 1;
    }
}
