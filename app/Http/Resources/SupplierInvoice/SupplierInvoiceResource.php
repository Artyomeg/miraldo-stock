<?php

namespace App\Http\Resources\SupplierInvoice;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'supplier_id'   => $this->supplier_id,
            'supplier'      => $this->supplier,
            'date'          => $this->date,
            'amount'        => (float)$this->amount,
            'invoiceNumber' => $this->invoice_number,
            'invoiceDate'   => $this->invoice_date,
            'number'        => $this->number,
            'comment'       => $this->comment,
        ];
    }
}
