<?php

namespace App\Http\Resources\Revision;

class InvoiceResource extends InvoiceOrSupplyResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returned = array_merge(
            parent::toArray($request),
            [
                'counterpartyId'     => $this->client_id,
                'counterpartyTitle'  => $this->client->name,
                'currencySystemName' => 'ruble',
                'docType'            => 'invoice',
                'payed'              => $this->payed,
                'rublePayed'         => $this->rublePayed
            ]
        );

        return $returned;
    }
}
