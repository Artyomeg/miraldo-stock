<?php

namespace App\Http\Resources\Revision;

class SupplyResource extends InvoiceOrSupplyResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returned = array_merge(
            parent::toArray($request),
            [
                'counterpartyId'     => $this->supplier_id,
                'counterpartyTitle'  => $this->supplier->name,
                'currencySystemName' => $this->currency->system_name,
                'date'               => date('d.m.Y', strtotime($this->date)),
                'docType'            => 'supply',
            ]
        );

        return $returned;
    }
}
