<?php

namespace App\Http\Resources\Revision;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceOrSupplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'amount'      => $this->amount,
            'docTitle'    => $this->getDocTitle(),
            'number'      => $this->number,
            'rubleAmount' => (float)$this->rubleAmount,
            'docDebt'     => $this->docDebt,
        ];
    }
}
