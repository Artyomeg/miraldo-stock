<?php

namespace App\Http\Resources\SupplyProduct;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplyProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count'        => (float) $this->count,
            'id'           => $this->id,
            'name'         => optional($this->product)->name,
            'unit'         => optional($this->product)->unit,
            'supplierCode' => optional($this->product)->supplier_code,
            'price'        => (float) $this->price,
            'productId'    => $this->product_id,
            'supplyId'     => $this->supply_id,
            'product'      => $this->product,
        ];
    }
}
