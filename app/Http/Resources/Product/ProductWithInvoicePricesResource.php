<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductWithInvoicePricesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count'          => (float) $this->count,
            'id'             => $this->id,
            'barcode'        => $this->barcode,
            'barcodeKgChars' => $this->barcode_kg_chars,
            'name'           => $this->name,
            'unit'           => $this->unit,
            'prices'         => $this->productInvoicePrices,
            'supplierCode'   => $this->supplier_code,
        ];
    }
}
