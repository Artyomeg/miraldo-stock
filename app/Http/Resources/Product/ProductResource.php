<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'barcode'      => $this->barcode,
            'name'         => $this->name,
            'count'        => (float) $this->count,
            'unit'         => $this->unit,
            'supplierCode' => $this->supplier_code,
            'searchString' => $this->search_string,
        ];
    }
}
