<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount'      => (float)$this->amount,
            'client'      => $this->client,
            'clientDebt'  => $this->client->debt,
            'clientId'    => $this->client_id,
            'clientTitle' => $this->client->name,
            'date'        => $this->date,
            'id'          => $this->id,
            'payed'       => (float)$this->payed,
            'comment'     => $this->comment,
        ];
    }
}
