<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\ClientResource;
use App\Models\Order;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * @mixin Order
 */
class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'number'  => $this->number,
            'date'     => date('Y-m-d', strtotime($this->date)),
            'comment'  => $this->comment,

            'client'   => $this->whenLoaded('client', new ClientResource($this->client)),
            'products' => OrderProductResource::collection($this->whenLoaded('orderProducts')),
        ];
    }
}
