<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceWithProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'ts'                 => $this->ts,
            'counterpartyId'     => $this->client_id,
            'counterpartyTitle'  => $this->client->name,
            'amount'             => (float) $this->amount,
            'payed'              => $this->payed,
            'rubleAmount'        => (float) $this->rubleAmount,
            'rublePayed'         => $this->rublePayed,
            'currencySystemName' => 'ruble',
            'docType'            => 'invoice',
            'docTitle'           => $this->getDocTitle(),
            'productCount'       => (float) $this->count,
            'productPrice'       => (float) $this->price,
            'productRubleAmount' => $this->productRubleAmount,
            'productRelationId'  => $this->productRelationId,
        ];
    }
}
