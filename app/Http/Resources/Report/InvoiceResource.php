<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount'             => (float)$this->amount,
            'counterpartyId'     => $this->client_id,
            'counterpartyTitle'  => $this->client->name,
            'currencySystemName' => 'ruble',
            'date'               => date('d.m.Y', strtotime($this->date)),
            'docDebt'            => $this->docDebt,
            'docTitle'           => $this->getDocTitle(),
            'docType'            => 'invoice',
            'id'                 => $this->id,
            'number'             => $this->number,
            'rubleAmount'        => (float) $this->rubleAmount,
            'rublePayed'         => $this->rublePayed,
            'totalCount'         => $this->totalCount,
        ];
    }
}
