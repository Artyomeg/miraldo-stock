<?php

namespace App\Http\Resources\Report;

use App\Models\Payment;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    public $collects = Payment::class;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // TODO: Запилить проверку с эксепшеном на предмет существования одного из: `client_id` или `supplier_id`
        if ($this->client_id === null && $this->supplier_id === null) {
            return (array) $this;
            // throw new \Exception('No data with' . (array)$this);
        }

        return [
            'amount'             => (float)$this->amount,
            'comment'            => $this->comment,
            'counterpartyId'     => $this->client_id !== null
                                        ? $this->client_id
                                        : $this->supplier_id,
            'counterpartyTitle'  => $this->client
                                        ? $this->client->name
                                        : $this->supplier->name,
            'currencySystemName' => 'ruble',
            'date'               => date('d.m.Y', strtotime($this->date)),
            'docDebt'            => $this->docDebt,
            'docTitle'           => $this->getDocTitle(),
            'docType'            => Payment::$DOC_TYPE,
            'id'                 => $this->id,
            'number'             => $this->number,
            'rubleAmount'        => (float)$this->rubleAmount,
            'rublePayed'         => $this->rublePayed,
            'currencyId'         => $this->currency_id,
        ];
    }
}
