<?php

namespace App\Http\Resources\Report;

use App\Http\Resources\Revision\InvoiceOrSupplyResource;
use App\Models\SupplierInvoice;

class SupplierInvoiceResource extends InvoiceOrSupplyResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $returned = array_merge(
            parent::toArray($request),
            [
                'id'                 => $this->id,
                'currencyId'         => $this->currency_id,
                'counterpartyId'     => $this->supplier_id,
                'counterpartyTitle'  => $this->supplier->name,
                'amount'             => $this->euroAmount
                    ? (float)$this->euroAmount
                    : (float)$this->amount,
                'rubleAmount'        => (float)$this->rubleAmount,
                'currencySystemName' => $this->currency->system_name,
                'docType'            => SupplierInvoice::$DOC_TYPE,
                'docTitle'           => $this->getDocTitle(),
                'totalCount'         => $this->totalCount,
                'date'               => date('d.m.Y', strtotime($this->date)),
                'invoiceDate'        => $this->invoice_date
                    ? date('d.m.Y', strtotime($this->invoice_date))
                    : null,
                'invoiceNumber'      => $this->invoice_number,
                'comment'            => $this->comment,
            ]
        );

        return $returned;
    }
}
