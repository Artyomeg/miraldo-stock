<?php

namespace App\Http\Resources\Report;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplyWithProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'ts'                 => $this->ts,
            'counterpartyId'     => $this->supplier_id,
            'counterpartyTitle'  => $this->supplier->name,
            'amount'             => (float) $this->amount,
            'rubleAmount'        => (float) $this->rubleAmount,
            'currencySystemName' => $this->currency->system_name,
            'docType'            => 'supply',
            'docTitle'           => $this->getDocTitle(),
            'productCount'       => (float) $this->count,
            'productPrice'       => (float) $this->price,
            'productRubleAmount' => $this->productRubleAmount,
            'productRelationId'  => $this->productRelationId,
        ];
    }
}
