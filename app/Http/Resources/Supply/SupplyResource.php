<?php

namespace App\Http\Resources\Supply;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'date'               => date('d.m.Y', strtotime($this->date)),
            'amount'             => $this->amount,
            'currencySystemName' => $this->currency->system_name,
            'docTitle'           => $this->getDocTitle(),
            'number'             => $this->number,
            'rubleAmount'        => $this->rubleAmount,
            'supplierId'         => $this->supplier_id,
            'supplierTitle'      => $this->supplier->name
        ];
    }
}
