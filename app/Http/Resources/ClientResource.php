<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'client_pricelist_id' => $this->client_pricelist_id,
            'name'                => $this->name,
            'address'             => $this->address,
            'debt'                => $this->debt,
            'phone'               => $this->phone,
            'cashless'            => (boolean)$this->cashless,
            'isArchived'          => (boolean)$this->is_archived,
        ];
    }
}
