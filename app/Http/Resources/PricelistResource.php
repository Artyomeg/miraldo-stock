<?php

namespace App\Http\Resources;

use App\Http\Resources;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PricelistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'assigned_to_euro' => $this->assigned_to_euro,
            'productPrices'    => $this->whenLoaded(
                'productPrices',
                Resources\Product\ProductPriceResource::collection($this->productPrices),
            ),
        ];
    }
}
