<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @deprecated
 */
class ProductPriceResource extends JsonResource
{
    // YYY: лежит тут по случаю текущего неиспользования, но может пригодиться
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'count' => (float) $this->count,
            'id'    => $this->id,
            'name'  => $this->name
        ];
    }
}
