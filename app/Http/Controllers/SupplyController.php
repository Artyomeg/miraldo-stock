<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Models\Supplier;
use App\Models\Supply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplyController extends Controller
{
    const SUPPLY__RECORDS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliesQuery = Supply::query();

        $supplies = $suppliesQuery
            ? $suppliesQuery->orderBy('id', 'desc')->paginate(self::SUPPLY__RECORDS_PER_PAGE)
            : [];

        return view('supplies.index', compact('supplies'))
            ->with('i', (request()->input('page', 1) - 1) * self::SUPPLY__RECORDS_PER_PAGE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers  = Supplier::orderBy('name', 'asc')->pluck('name', 'id');
        $currencies = Currency::pluck('name', 'id');

        return view('supplies.create', compact(['suppliers', 'currencies']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'date'        => 'required',
            'supplier_id' => 'required'
        ]);

        $values = (array) $request->all();
        $values['number'] = Supply::getNewNumber();

        $supply = Supply::create($values);

        $supply->updateSupplyProducts(request()->SupplyProductsForm);

        return redirect()->route('supplies.index')
            ->with('success', "Поставка №\"{$supply->number}\" создана");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supply = Supply::find($id);

        return view('supplies.show', compact('supply'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supply = Supply::find($id);

        // if no supply - return 404
        if (!$supply) {
            $data = [
                'code'        => '404',
                'description' => 'Поставка не найдена',
            ];

            return response()
                ->view('errors.404', $data, 404);
        }

        $suppliers  = Supplier::orderBy('name', 'asc')->pluck('name', 'id');
        $currencies = Currency::pluck('name', 'id');

        return view('supplies.edit', compact(['supply', 'suppliers', 'currencies']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'date'        => 'required',
            'supplier_id' => 'required'
        ]);

        $supply = Supply::find($id);

        $supply->update($request->all());
        $supply->updateSupplyProducts(request()->SupplyProductsForm);

        return redirect()->route('supplies.index')
            ->with('success', "Поставка №\"{$supply->number}\" обновлена");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supply = Supply::find($id);

        // когда удаляем товар из поставки, значит у нас на складе его стало меньше - вычитаем товар со склада
        foreach ($supply->supplyProducts as $supplyProduct) {
            $supplyProduct->product->count -= $supplyProduct->count;
            $supplyProduct->product->save();
        }

        $supplyNumber = $supply->number;
        $supply->delete();

        return redirect()->route('supplies.index')
            ->with('success', "Поставка №\"{$supplyNumber}\" удалена");
    }
}
