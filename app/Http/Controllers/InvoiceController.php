<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Currency;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    const INVOICE__RECORDS_PER_PAGE = 100;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoicesQuery = Invoice::query();

        $invoices = $invoicesQuery
            ? $invoicesQuery
                ->orderBy('date', 'desc')
                ->orderBy('id', 'desc')
                ->paginate(self::INVOICE__RECORDS_PER_PAGE)
            : [];

        return view('invoices.index', compact('invoices'))
            ->with('i', (request()->input('page', 1) - 1) * self::INVOICE__RECORDS_PER_PAGE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'amount'    => 'required',
            'client_id' => 'required',
            'date'      => 'required'
        ]);

        $values = (array) $request->all();
        $values['number'] = Invoice::getNewNumber();

        $invoice = Invoice::create($values);
        $invoice->updateInvoiceProducts(request()->InvoiceProductsForm);

        return redirect()->route('invoices.index')
            ->with('success', "Счет №\"{$invoice->number}\" создан");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);

        return view('invoices.show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = Invoice::find($id);

        if (!$invoice) {
            // TODO: Добавить 2 языка
            abort(404, "Счет не найден");
        }

        return view('invoices.edit', compact(['invoice']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'amount'    => 'required',
            'client_id' => 'required',
            'date'      => 'required'
        ]);

        $invoice = Invoice::find($id);

        $invoice->update($request->all());
        $invoice->updateInvoiceProducts(request()->InvoiceProductsForm);

        return redirect()->route('invoices.index')
            ->with('success', "Счет №\"{$invoice->number}\" обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::find($id);

        // когда удаляем товар из счета, значит у нас на складе его стало больше - добавляем товар со склада
        foreach ($invoice->invoiceProducts as $invoiceProduct) {
            $invoiceProduct->product->count += $invoiceProduct->count;
            $invoiceProduct->product->save();
        }

        $invoiceNumber = $invoice->number;
        $invoice->delete();

        return redirect()->route('invoices.index')
            ->with('success', "Счет №\"{$invoiceNumber}\" удален");
    }
}
