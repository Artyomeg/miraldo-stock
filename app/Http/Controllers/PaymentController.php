<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Currency;
use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    const PAYMENT__RECORDS_PER_PAGE = 1000;

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $payment = Payment::find($id);

        if (!$payment) {
            // TODO: Добавить 2 языка
            abort(404, "Платеж не найден");
        }

        $currencies = Currency::pluck('name', 'id');

        return view('payments.edit', compact(['payment', 'currencies']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'date'      => 'required',
            'amount'    => 'required'
        ]);

        $payment = Payment::find($id);
        $payment->date = $request->get('date');
        $payment->amount = $request->get('amount');
        $payment->comment = $request->get('comment');
        $payment->currency_id = $request->get('currency_id');
        $payment->save();

        if ($payment->client_id) {
            return redirect()->route('clients.payments', ['id' => $payment->client_id])
                ->with('success', "Оплата обновлена");
        } else {
            return redirect()->route('reports.by-suppliers', ['supplier_id' => $payment->supplier_id])
                ->with('success', "Оплата обновлена");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment  = Payment::find($id);
        $clientId = $payment->client_id;
        $supplierId = $payment->supplier_id;
        $payment->delete();

        if ($clientId) {
            return redirect()->route('clients.payments', $clientId)
                ->with('success', "Оплата удалена");
        } else {
            return redirect()->route('reports.by-suppliers', [
                'supplier_id' => $supplierId
            ]);
        }
    }
}
