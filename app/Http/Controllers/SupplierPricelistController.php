<?php

namespace App\Http\Controllers;

use App\Models\SupplierPricelist;
use App\Models\SupplierPricelistProductPrice;
use Illuminate\Http\Request;

class SupplierPricelistController extends Controller
{
    const PRICELIST__RECORDS_PER_PAGE = 100;

    public function __construct()
    {
        $this->middleware(['auth', 'locale']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('supplierPricelists.index')
            ->with('i', (request()->input('page', 1) - 1) * self::PRICELIST__RECORDS_PER_PAGE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplierPricelists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required'
        ]);

        // fix: assign `assigned_to_euro` to bool
        $request->merge([
            'assigned_to_euro' => $request->assigned_to_euro === 'on',
        ]);

        $pricelist = SupplierPricelist::create($request->all());

        return redirect()->route('supplier-pricelists.index')
            ->with('success', "Прайс-лист поставщика \"{$pricelist->name}\" создан");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pricelist = SupplierPricelist::find($id);

        if ($pricelist === null) {
            abort(404, 'Прайслист для поставщиков не найден');
        }

        $currentDate = date('d.m.Y');
        $ORDER = 'ASC';

        $productPrices = SupplierPricelistProductPrice::join('products', 'supplier_pricelist_product_prices.product_id', '=', 'products.id')
            ->select('supplier_pricelist_product_prices.*')
            ->where('supplier_pricelist_id', $id)
            ->orderBy('products.name', $ORDER)
            ->get();

        return view('supplierPricelists.show', compact(['currentDate', 'pricelist', 'productPrices']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pricelist = SupplierPricelist::find($id);
        return view('supplierPricelists.edit', compact(['pricelist']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'  => 'required'
        ]);

        $pricelist = SupplierPricelist::find($id);
        $pricelist->assigned_to_euro = (boolean)$request->get('assigned_to_euro');
        $pricelist->name = $request->get('name');
        $pricelist->save();

        return redirect()->route('supplier-pricelists.index')
            // TODO: указать текст сообщения
            ->with('success', "Прайс-лист поставщика \"{$pricelist->name}\" обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricelist     = SupplierPricelist::find($id);
        $pricelistName = $pricelist->name;
        $pricelist->delete();

        return redirect()->route('supplier-pricelists.index')
            ->with('success', "Прайс-лист поставщика \"{$pricelistName}\" удален");
    }
}
