<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientPricelist;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    const CLIENT__RECORDS_PER_PAGE = 1000;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pricelists = ClientPricelist::pluck('name', 'id');

        return view('clients.create', compact(['pricelists']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function debts()
    {
        return view('clients.debts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'address' => 'required',
            'name'    => 'required',
            'phone'   => 'required'
        ]);

        $request->merge([
            'cashless'    => $request->has('cashless'),
            'is_archived' => $request->has('is_archived'),
        ]);

        $client = Client::create($request->all());

        return redirect()->route('clients.index')
            ->with('success', "Клиент \"{$client->name}\" создан");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        return view('clients.show', compact('client'));
    }

    public function payments($id)
    {
        $client = Client::find($id);

        if (!$client) {
            // TODO: Добавить 2 языка
            abort(404, "Клиент не найден");
        }

        return view('clients.payments', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client     = Client::find($id);
        $pricelists = ClientPricelist::pluck('name', 'id');

        return view('clients.edit', compact(['client', 'pricelists']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'address' => 'required',
            'name'    => 'required',
            'phone'   => 'required'
        ]);

        $client = Client::find($id);
        $request->merge([
            'cashless'    => $request->has('cashless'),
            'is_archived' => $request->has('is_archived'),
        ]);

        $client->update($request->all());

        return redirect()->route('clients.index')
            ->with('success', "Клиент \"{$client->name}\" обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client     = Client::find($id);
        $clientName = $client->name;
        $client->delete();

        return redirect()->route('clients.index')
            ->with('success', "Клиент \"{$clientName}\" удален");
    }
}
