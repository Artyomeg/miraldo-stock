<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ControlMode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ControlModeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'locale']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function showControlPage()
    {
        return view('auth.login-control');
    }

    public function setControlMode(Request $request)
    {
        $user = User::query()->first();
        $passwordIsCorrect = Hash::check($request->password, $user->control_mode_password);

        if ($passwordIsCorrect) {
            ControlMode::set();
            return redirect(User::CONTROL_HOME_URL);
        }

        return redirect()->route('control');
    }

    public function unsetControlMode()
    {
        ControlMode::unset();
        return redirect('/');
    }
}
