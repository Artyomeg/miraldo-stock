<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ClientPricelist;
use App\Models\ClientPricelistProductPrice;
use App\Models\SupplierPricelist;
use App\Models\SupplierPricelistProductPrice;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    const PRODUCT__RECORDS_PER_PAGE = 100;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['control-mode']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('products.index')
            ->with('i', (request()->input('page', 1) - 1) * self::PRODUCT__RECORDS_PER_PAGE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        // TODO: проверить почему дублируется переменная прайслиста клиента
        $clientPricelists   = ClientPricelist::all();
        $supplierPricelists = SupplierPricelist::all();
        $pricelists         = ClientPricelist::all();

        return view(
            'products.create',
            compact('pricelists', 'clientPricelists', 'supplierPricelists')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'  => 'required',
            'count' => 'required',
            'unit'  => 'required',
        ]);

        // 1 Создадим товар
        $product = Product::create($request->all());

        // 2 Установим цены на товар для клиентов
        if ($request->input('productPriceByClientPriceList') !== null) {
            foreach ($request->input('productPriceByClientPriceList') as $pricelistId => $productPriceValue) {
                $productPrice = ClientPricelistProductPrice::firstOrNew([
                    'product_id'          => $product->id,
                    'client_pricelist_id' => $pricelistId
                ]);

                $productPrice->price = $productPriceValue;
                $productPrice->save();
            }
        }

        // 3 Установим цены на товар от поставщиков
        if ($request->input('productPriceBySupplierPriceList') !== null) {
            foreach ($request->input('productPriceBySupplierPriceList') as $pricelistId => $productPriceValue) {
                $productPrice = SupplierPricelistProductPrice::firstOrNew([
                    'product_id'            => $product->id,
                    'supplier_pricelist_id' => $pricelistId
                ]);

                $productPrice->price = $productPriceValue;
                $productPrice->save();
            }
        }

        return redirect()->route('products.index')
                         ->with('success', "Товар \"{$product->name}\" создан");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function show($id)
    {
        $product = Product::find($id);

        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return View
     */
    public function edit($id)
    {
        $product = Product::find($id);

        if ($product === null) {
            abort(404, 'Товар не найден');
        }

        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'  => 'required',
            'count' => 'required',
        ]);

        // 1 Обновим сам товар
        $product = Product::find($id);
        $product->update($request->all());

        // 2 Обновим цены на товар для клиентов
        if ($request->input('productPriceByClientPriceList') !== null) {
            foreach ($request->input('productPriceByClientPriceList') as $pricelistId => $productPriceValue) {
                $productPrice = ClientPricelistProductPrice::firstOrNew([
                    'product_id'          => $id,
                    'client_pricelist_id' => $pricelistId
                ]);

                $productPrice->price = $productPriceValue;
                $productPrice->save();
            }
        }

        // 3 Обновим цены на товар от поставщиков
        if ($request->input('productPriceBySupplierPriceList') !== null) {
            foreach ($request->input('productPriceBySupplierPriceList') as $pricelistId => $productPriceValue) {
                $productPrice = SupplierPricelistProductPrice::firstOrNew([
                    'product_id'            => $id,
                    'supplier_pricelist_id' => $pricelistId
                ]);

                $productPrice->price = $productPriceValue;
                $productPrice->save();
            }
        }

        return redirect()->route('products.index')
                         ->with('success', "Товар \"{$product->name}\" обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $product     = Product::find($id);
        $productName = $product->name;
        $product->delete();

        return redirect()->route('products.index')
                         ->with('success', "Товар \"{$productName}\" удален");
    }
}
