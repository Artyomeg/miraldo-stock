<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest;
use App\Services\Stocktaking;

class RevisionController extends Controller
{
    const REVISION__RECORDS_PER_PAGE = 10;

    public function index()
    {
        return view('revisions.index');
    }

    public function stocktaking(BaseRequest $request) {
        $stocktaking = new Stocktaking(2020);
//        $stocktaking->handle();
    }
}
