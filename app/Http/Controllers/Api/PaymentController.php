<?php

namespace App\Http\Controllers\Api;

use App\Models\Payment;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends \App\Http\Controllers\Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            // TODO: написать валидатор - принимать или client_id, или supplier_id
//            'client_id' => 'required',
//            'supplier_id' => 'required',
            'date'      => 'required',
            'amount'    => 'required'
        ]);

        $values = (array) $request->all();

        if (isset($values['supplier_id'])) {
            $supplier = Supplier::find($values['supplier_id']);
            $values['currency_id'] = $supplier->currency_id;
        }

        $values['number'] = Payment::getNewNumber();

        return Payment::create($values);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $payment = Payment::find($id);
        $success = $payment->delete();

        return [
            'success' => $success,
        ];
    }
}
