<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PricelistResource;
use App\Models\SupplierPricelist;

class SupplierPricelistController extends \App\Http\Controllers\Controller
{
    public function list()
    {
        $pricelists = SupplierPricelist::all();
        return PricelistResource::collection($pricelists);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricelist = SupplierPricelist::find($id);
        $success = $pricelist->delete();

        return [
            'success' => $success,
        ];
    }
}
