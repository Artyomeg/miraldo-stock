<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\InvoiceProduct\InvoiceProductResource;
use App\Http\Resources\InvoiceResource;
use App\Models\Invoice;
use App\Models\InvoiceProduct;
use App\Services\PdfService;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{
    const INVOICE_PRODUCT__RECORDS_PER_PAGE = 10;

    public function list()
    {
        $invoices = Invoice::all();

        return InvoiceProductResource::collection($invoices);
    }

    public function show($id)
    {
        $invoice = Invoice::find($id);

        if ($invoice === null) {
            abort(404, 'Счет не найден');
        }

        return new InvoiceResource($invoice);
    }

    public function getInvoiceProductsByInvoiceId(BaseRequest $request)
    {
        $invoiceProducts = InvoiceProduct::where('invoice_id', $request->id)->get();

        return InvoiceProductResource::collection($invoiceProducts);
    }

    /**
     * @param  Invoice  $invoice
     *
     * @return Response
     */
    public function download(Invoice $invoice)
    {
        $invoice->load([
            'invoiceProducts',
            'invoiceProducts.product',
            'invoiceProducts.currency'
        ]);

        $service = new PdfService(
            view: 'invoices.pdf',
            data: compact('invoice')
        );

        return $service
            ->withFilename(
                sprintf("Счет %d от %s", $invoice->id, $invoice->date)
            )
            ->landscape()
            ->download();
    }
}
