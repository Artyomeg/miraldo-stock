<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Setting\DeleteSettingRequest;
use App\Http\Requests\Api\Setting\IndexSettingRequest;
use App\Http\Requests\Api\Setting\StoreSettingRequest;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use RuntimeException;

class SettingController extends Controller
{
    /**
     * @param  IndexSettingRequest  $request
     *
     * @return ResourceCollection
     */
    public function index(IndexSettingRequest $request)
    {
        $code = $request->validated(
            'code',
            'dollar_price'
        );

        return SettingResource::collection(
            Setting::withTrashed()
                ->where('code', $code)
                ->latest('id')
                ->get()
        );
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        request()->validate([
            'dollar_price' => 'required',
        ]);

        $oldSettings = Setting::euroPrice()->get();
        $newEuroPrice = Setting::formatEuro($request->get('dollar_price'));

        if ($newEuroPrice === 0) {
            return false;
        }

        if (count($oldSettings)) {
            foreach ($oldSettings as $oldSetting) {
                Setting::updateDollarAssignedPricelists($oldSetting->value, $newEuroPrice);
                $oldSetting->delete();
            }
        }

        Setting::create([
            'code' => 'dollar_price',
            'value' => $newEuroPrice,
        ]);

        return true;
    }

    /**
     * @param  StoreSettingRequest  $request
     *
     * @return ResourceCollection
     */
    public function store(StoreSettingRequest $request)
    {
        $code = $request->validated('code', 'dollar_price');
        $value = $request->validated('value');

        DB::transaction(function () use ($code, $value) {
            switch ($code) {
                case 'dollar_price':
                    $old_settings = Setting::withTrashed()->euroPrice()->get();
                    $new_value = Setting::formatEuro($value);

                    if ($old_settings->isNotEmpty()) {
                        $old_settings->each(function (Setting $setting) use ($new_value) {
                            Setting::updateDollarAssignedPricelists($setting->value, $new_value);
                            $setting->delete();
                        });
                    }
                    break;
            }

            Setting::query()->create(
                compact('code', 'value')
            );
        });

        return SettingResource::collection(
            Setting::withTrashed()
                ->where('code', $code)
                ->latest('id')
                ->get()
        );
    }

    /**
     * @param  DeleteSettingRequest  $request
     *
     * @return ResourceCollection
     */
    public function destroy(DeleteSettingRequest $request)
    {
        /** @var Setting[]|Collection $settings */
        $settings = Setting::withTrashed()->findMany(
            $request->validated('id')
        );

        if ($settings->isEmpty()) {
            throw ValidationException::withMessages([
                'id' => 'Settings not found',
            ]);
        }

        $codes = $settings->pluck('code')->unique();
        if ($codes->count() > 1) {
            throw ValidationException::withMessages([
                'id' => 'Settings have not unique codes',
            ]);
        }

        $code = $codes->first();

        $latest = Setting::query()
            ->where('code', $code)
            ->latest('id')
            ->firstOr(callback: fn() => throw new RuntimeException('Invalid request'));

        if ($settings->pluck('id')->contains($latest->id)) {
            throw ValidationException::withMessages([
                'id' => 'Latest value cannot be deleted',
            ]);
        }

        $settings->each->forceDelete();

        return SettingResource::collection(
            Setting::withTrashed()
                ->where('code', $code)
                ->latest('id')
                ->get()
        );
    }
}
