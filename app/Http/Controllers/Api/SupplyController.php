<?php

namespace App\Http\Controllers\Api;

use App\Models\Supply;
use Illuminate\Http\Request;

class SupplyController extends \App\Http\Controllers\Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        $supply = Supply::find($id);

        if (!$supply) {
            abort(404, 'Поставка не найдена');
        }

        return [
            'success' => true,
            'supply' => $supply,
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'date'        => 'required',
            'supplier_id' => 'required'
        ]);

        $values = (array) $request->all();
        $values['number'] = Supply::getNewNumber();

        $supply = Supply::create($values);

        $supply->updateSupplyProducts(request()->SupplyProductsForm);

        return [
            'success' => true,
            'supply' => $supply,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        request()->validate([
//            'date'        => 'required',
//            'supplier_id' => 'required'
//        ]);

        $supply = Supply::find($id);

        $invoiceDate = (bool)$request->has('invoice_date')
            ? $request->get('invoice_date')
            : null;
        $request->merge(['invoice_date' => $invoiceDate]);
        $supplyUpdateStatus = $supply->update($request->all());
        $supplyProductsUpdateStatus = $supply->updateSupplyProducts(request()->SupplyProductsForm);

        return [
            'success' => $supplyUpdateStatus,
            'supply' => $supply,
            'supplyUpdateStatus' => $supplyUpdateStatus,
            'supplyProductsUpdateStatus' => $supplyProductsUpdateStatus,
            'SupplyProductsForm' => request()->SupplyProductsForm,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $supply = Supply::find($id);

        // когда удаляем товар из поставки, значит у нас на складе его стало меньше - вычитаем товар со склада
        foreach ($supply->supplyProducts as $supplyProduct) {
            $supplyProduct->product->count -= $supplyProduct->count;
            $supplyProduct->product->save();
        }

        $success = $supply->delete();

        return [
            'success' => $success,
        ];
    }
}
