<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\SupplyProduct\SupplyProductResource;
use App\Models\SupplyProduct;

class SupplyProductController extends \App\Http\Controllers\Controller
{
    public function getSupplyProductsBySupplyId(BaseRequest $request)
    {
        $supplyProducts = SupplyProduct::where('supply_id', $request->id)->get();

        return SupplyProductResource::collection($supplyProducts);
    }
}
