<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DuplicateClientPricelistRequest;
use App\Http\Resources\PricelistResource;
use App\Models\ClientPricelist;
use App\Models\ClientPricelistProductPrice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class ClientPricelistController extends Controller
{
    /**
     * @return ResourceCollection
     */
    public function list()
    {
        $pricelists = ClientPricelist::all();

        return PricelistResource::collection($pricelists);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return PricelistResource
     */
    public function show($id)
    {
        $priceList = ClientPricelist::findOrFail($id);

        $ORDER = 'ASC';
        $productPrices = ClientPricelistProductPrice::join('products', 'client_pricelist_product_prices.product_id', '=', 'products.id')
            ->select('client_pricelist_product_prices.*')
            ->where('client_pricelist_id', $id)
            ->orderBy('products.name', $ORDER)
            ->with('product')
            ->get();

        return [
            'priceList' => new PricelistResource($priceList),
            'productPrices' => $productPrices,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $pricelist = ClientPricelist::findOrFail($id);
        $success   = $pricelist->delete();

        return response()->json([
            'success' => $success,
        ]);
    }

    /**
     * @param  DuplicateClientPricelistRequest  $request
     * @param  ClientPricelist  $pricelist
     *
     * @return PricelistResource
     */
    public function duplicate(DuplicateClientPricelistRequest $request, ClientPricelist $pricelist)
    {
        $new_pricelist = DB::transaction(function () use ($request, $pricelist) {
            $original = $pricelist->only(
                $pricelist->getFillable()
            );

            // duplicate ClientPricelist
            $new_pricelist = ClientPricelist::query()->create(
                array_merge($original, $request->validated())
            );

            $prices = $pricelist->productPrices->map(function (ClientPricelistProductPrice $price) use ($new_pricelist) {
                $price_attributes = $price->only(
                    $price->getFillable()
                );

                return array_merge($price_attributes, [
                    'client_pricelist_id' => $new_pricelist->id,
                ]);
            });

            // duplicate ClientPricelistProductPrice collection
            $new_pricelist->productPrices()->createMany($prices);

            return $new_pricelist;
        });

        return new PricelistResource(
            $new_pricelist->load('productPrices')
        );
    }
}
