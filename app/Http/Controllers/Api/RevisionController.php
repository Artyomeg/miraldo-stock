<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\Report\PaymentResource;
use App\Http\Resources\Revision\InvoiceResource;
use App\Http\Resources\Revision\SupplyResource;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Supply;
use Illuminate\Support\Facades\DB;

class RevisionController extends \App\Http\Controllers\Controller
{
    private $debt = 0;
    private $ORDER_BY = 'desc';

    /**
     * @param BaseRequest $request
     * @return array
     */
    public function getSuppliesAndInvoices(BaseRequest $request)
    {
        // 1 get docs collection
        // if only suppliers set - show only supplies
        if ((!empty($request->supplierIds)) && (empty($request->clientIds))) {
            // add json encode for remove default `data` json wrapper
            $docsCollection = $this->getSuppliesDocs($request);
        }
        // if only clients set - show invoices and payments
        elseif (empty($request->supplierIds) && (!empty($request->clientIds))) {
            // add json encode for remove default `data` json wrapper
            $docsCollection = $this->getDocsWithoutSupplies($request);
        }
        // if all fields are empty - show invoices and payments
        elseif (empty($request->supplierIds) && (empty($request->clientIds))) {
            // add json encode for remove default `data` json wrapper
            $docsCollection = $this->getDocsWithoutSupplies($request);
        }
        // if all of them set - show all filtered,
        // or if no one set
        else {
            $docsCollection = [];
        }

        // 2 all time payments
        $allTimePaymentAmount = $this->getAllTimePaymentSums($request);

        // 3 all time invoices (to get all time sells)
        $allTimeInvoiceAmount = $this->getAllTimeInvoiceSums($request);


        $revisionResource = [
            'docsCollection' => $docsCollection,
            'inStock'        => $this->getStockAmount($request),
            'total'          => [
                'debt' => $this->debt,
                'paid' => $allTimePaymentAmount,
                'sold' => $allTimeInvoiceAmount,
            ],
        ];

        return $revisionResource;
    }

    /**
     * @param BaseRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    private function getSuppliesDocs(BaseRequest $request)
    {
        // 1 supplies
        $supplyQuery = Supply::query()
            ->orderBy('date', $this->ORDER_BY)
            ->orderBy('id', $this->ORDER_BY);

        // 1.1 date from
        if ($request->from) {
            $supplyQuery->where('date', '>=', date('Y-m-d 00:00:00', strtotime($request->from)));
        }

        // 1.2 date to
        if ($request->to) {
            $supplyQuery->where('date', '<=', date('Y-m-d 23:59:59', strtotime($request->to)));
        }

        // 1.3 suppliers
        if (!empty($request->supplierIds)) {
            $supplyQuery->whereIn('supplier_id', $request->supplierIds);
        }

        // 1.4 products
        if (!empty($request->productIds)) {
            $supplyQuery
                ->leftJoin('supply-products', 'supplies.id', '=', 'supply-products.supply_id')
                ->whereIn('supply-products.product_id', $request->productIds)
                ->select(DB::raw('supplies.*'));

            if (count($request->productIds) === 1) {
                $supplyQuery->addSelect(DB::raw('`supply-products`.count'));
            }
        }

        return SupplyResource::collection($supplyQuery->get());
    }

    /**
     * @param BaseRequest $request
     * @return array
     */
    private function getDocsWithoutSupplies(BaseRequest $request)
    {
        // 1
        $invoiceQuery = Invoice::query()
            ->select(
                'invoices.amount',
                'invoices.client_id',
                'invoices.date',
                'invoices.id',
                'invoices.number',
                DB::raw('"invoice" as "docType"')
            );

        // 2
        $paymentQuery = Payment::query()
            ->select(
                'amount',
                'client_id',
                'date',
                'id',
                'number',
                DB::raw('"payment" as "docType"')
            );

        // 3 clients
        if (!empty($request->clientIds)) {
            $invoiceQuery->whereIn('client_id', $request->clientIds);
            $paymentQuery->whereIn('client_id', $request->clientIds);
        } else {
            $paymentQuery->whereNotNull('client_id');
        }

        // 4 products
        if (!empty($request->productIds)) {
            $invoiceQuery
                ->leftJoin('invoice-products', 'invoices.id', '=', 'invoice-products.invoice_id')
                ->whereIn('invoice-products.product_id', $request->productIds);
        }

        $docsQuery = $invoiceQuery
            ->union($paymentQuery)
            ->orderBy('date', $this->ORDER_BY)
            ->orderBy('id', $this->ORDER_BY);

        $docs = $docsQuery->get();

        $docDebt = 0;

        foreach ($docs as $doc) {
            $docDebt += $doc->docType == 'payment'
                ? (float)$doc->amount
                : -(float)($doc->amount);

            $doc->docDebt = $docDebt;
        }

        $docsCollection = [];

        foreach ($docs as $i => $doc) {
            if (
                strtotime($doc->date) < strtotime($request->from)
                || strtotime($doc->date) > strtotime($request->to)
            ) {
                continue;
            }

            $resourceClass = $doc->docType == 'invoice'
                ? 'App\Http\Resources\Report\InvoiceResource'
                : 'App\Http\Resources\Report\PaymentResource';

            $docsCollection[] = new $resourceClass($doc);
        }

        $this->debt = $docDebt;

        return $docsCollection;
    }

    private function getAllTimeInvoiceSums(BaseRequest $request)
    {
        $allTimeInvoiceQuery = Invoice::query();

        if (!empty($request->clientIds)) {
            $allTimeInvoiceQuery->whereIn('client_id', $request->clientIds);
        }

        $allTimeRawInvoiceAmount = $allTimeInvoiceQuery
            ->selectRaw('sum(amount) as amountsum')
            ->get();

        return isset($allTimeRawInvoiceAmount[0]->amountsum)
            ? +$allTimeRawInvoiceAmount[0]->amountsum
            : 0;
    }

    private function getAllTimePaymentSums(BaseRequest $request)
    {
        $allTimePaymentQuery = Payment::query();

        if (!empty($request->clientIds)) {
            $allTimePaymentQuery->whereIn('client_id', $request->clientIds);
        }

        $allTimeRawPaymentAmount = $allTimePaymentQuery
            ->selectRaw('sum(amount) as amountsum')
            ->whereNotNull('client_id')
            ->get();

        return isset($allTimeRawPaymentAmount[0]->amountsum)
            ? +$allTimeRawPaymentAmount[0]->amountsum
            : 0;
    }

    private function getStockAmount(BaseRequest $request)
    {
        $stockWeight = Product::sum('count');
        return $stockWeight * 1080;
    }
}
