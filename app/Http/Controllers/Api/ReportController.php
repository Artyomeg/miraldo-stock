<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Reports\ReportsByProductsRequest;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\BaseRequest;
use App\Http\Resources\Report\InvoiceWithProductResource;
use App\Http\Resources\Report\SupplyWithProductResource;
use App\Http\Resources\Report\InvoiceResource;
use App\Http\Resources\Report\SupplyResource;
use App\Models\Invoice;
use App\Models\Payment;
use App\Models\Supply;
use App\Presenters\RevisionBySuppliersPresenter;
use App\Presenters\ReportBySuppliesPresenter;

class ReportController extends Controller
{
    /**
     * @param  ReportsByProductsRequest  $request
     *
     * @return JsonResponse|ResourceCollection
     */
    public function byProducts(ReportsByProductsRequest $request)
    {
        // 1 supplies
        $supplyQuery = Supply::query()
            ->orderByDesc('date')
            ->whereBetween('date', [
                Carbon::parse($request->validated('from'))->startOfDay(), // 1.1 date from
                Carbon::parse($request->validated('to'))->endOfDay(), // 1.2 date to
            ]);

        // 1.3 suppliers
        if ($request->validated('supplierIds')) {
            $supplyQuery->whereIn('supplier_id', $request->validated('supplierIds'));
        }

        // 1.4 product
        if ($request->validated('productId')) {
            $supplyQuery
                ->leftJoin('supply-products', 'supplies.id', '=', 'supply-products.supply_id')
                ->where('supply-products.product_id', $request->validated('productId'))
                ->select(DB::raw('supplies.*'));

            $supplyQuery
                ->addSelect(DB::raw('`supply-products`.price'))
                ->addSelect(DB::raw('`supply-products`.count'))
                ->addSelect(DB::raw('`supply-products`.id productRelationId'));
        }

        $supplyCollection = SupplyWithProductResource::collection(
            $supplyQuery->get()
        );

        // 2 invoices
        $invoiceQuery = Invoice::query()
            ->orderByDesc('date')
            ->whereBetween('date', [
                Carbon::parse($request->validated('from'))->startOfDay(), // 1.1 date from
                Carbon::parse($request->validated('to'))->endOfDay(), // 1.2 date to
            ]);

        // 2.3 clients
        if ($request->validated('clientIds')) {
            $invoiceQuery->whereIn('client_id', $request->validated('clientIds'));
        }

        // 2.4 products
        if ($request->validated('productId')) {
            $invoiceQuery
                ->leftJoin('invoice-products', 'invoices.id', '=', 'invoice-products.invoice_id')
                ->where('invoice-products.product_id', $request->validated('productId'))
                ->select(DB::raw('invoices.*'));

            $invoiceQuery
                ->addSelect(DB::raw('`invoice-products`.price'))
                ->addSelect(DB::raw('`invoice-products`.count'))
                ->addSelect(DB::raw('`invoice-products`.discount'))
                ->addSelect(DB::raw('`invoice-products`.id productRelationId'));
        }

        $invoiceCollection = InvoiceWithProductResource::collection(
            $invoiceQuery->get()
        );

        // 4 merge collections
        // if only suppliers set - show only supplies
        if ($request->collect('supplierIds')->isNotEmpty() && $request->collect('clientIds')->isEmpty()) {
            // add json encode for remove default `data` json wrapper
            return $supplyCollection;
        }

        // if only clients set - show only invoices
        if ($request->collect('supplierIds')->isEmpty() && $request->collect('clientIds')->isNotEmpty()) {
            // add json encode for remove default `data` json wrapper
            return $invoiceCollection;
        }

        // if both are empty - show both
        if ($request->collect('supplierIds')->isEmpty() && $request->collect('clientIds')->isEmpty()) {
            return response()->json([
                // merge collections
                'data' => $supplyCollection
                    ->merge($invoiceCollection)
                    ->sortByDesc('ts')
                    ->sortByDesc('id')
                    ->values(),
            ]);
        }

        // if all of them set - show all filtered,
        // or if no one set - we should show error or empty array
        return response()->json([
            'data' => [],
        ]);
    }

    public function byPayments(BaseRequest $request)
    {
        $docDebt = 0;
        $clientId = $request->get('clientId');

        $invoiceQuery = Invoice::where('client_id', $clientId)
            ->select(
                'amount',
                'client_id',
                'date',
                'id',
                'number',
                DB::raw('"" as "comment"'),
                DB::raw('"invoice" as "docType"')
            );

        $paymentQuery = Payment::where('client_id', $clientId)
            ->select(
                'amount',
                'client_id',
                'date',
                'id',
                'number',
                'comment',
                DB::raw('"payment" as "docType"')
            );

        $docsQuery = $invoiceQuery
            ->union($paymentQuery)
            ->orderBy('date', 'asc')
            ->orderBy('id', 'asc');

        $docs = $docsQuery->get();

        foreach ($docs as $doc) {
            $docDebt += $doc->docType === 'payment'
                ? (float) $doc->amount
                : -(float) ($doc->amount);

            $doc->docDebt = $docDebt;
        }

        $docsCollection = [];

        foreach ($docs as $doc) {
            if (
                strtotime($doc->date) < strtotime($request->get('from'))
                || strtotime($doc->date) > strtotime($request->get('to'))
            ) {
                continue;
            }

            $resourceClass = $doc->docType === 'invoice'
                ? 'App\Http\Resources\Report\InvoiceResource'
                : 'App\Http\Resources\Report\PaymentResource';

            $docsCollection[] = new $resourceClass($doc);
        }

        $response = [
            'total'          => $docDebt,
            'docsCollection' => $docsCollection,
        ];

        return $response;
    }

    public function productsMoving(BaseRequest $request)
    {
        // 1 supplies
        $supplyQuery = Supply::query()->orderBy('date', 'desc');

        // 1.1 date from
        if ($request->from) {
            $supplyQuery->where('date', '>=', date('Y-m-d 00:00:00', strtotime($request->from)));
        }

        // 1.2 date to
        if ($request->to) {
            $supplyQuery->where('date', '<=', date('Y-m-d 23:59:59', strtotime($request->to)));
        }

        // 1.3 suppliers
        if (! empty($request->supplierIds)) {
            $supplyQuery->whereIn('supplier_id', $request->supplierIds);
        }

        // 1.4 supply-products
        $supplyQuery
            ->select('supplies.*')
            ->addSelect('supply-products.supply_id')
            ->selectRaw('sum(`supply-products`.`count`) as \'totalCount\'')
            ->leftJoin('supply-products', 'supplies.id', '=', 'supply-products.supply_id')
            ->groupBy(['supplies.id'])
            ->havingRaw('`supply-products`.`supply_id` IS NOT NULL');

        $supplyCollection = SupplyResource::collection($supplyQuery->get());

        // 2 invoices
        $invoiceQuery = Invoice::query()->orderBy('date', 'desc');

        // 2.1 date from
        if ($request->from) {
            $invoiceQuery->where('date', '>=', date('Y-m-d 00:00:00', strtotime($request->from)));
        }

        // 2.2 date to
        if ($request->to) {
            $invoiceQuery->where('date', '<=', date('Y-m-d 23:59:59', strtotime($request->to)));
        }

        // 2.3 clients
        if (! empty($request->clientIds)) {
            $invoiceQuery->whereIn('client_id', $request->clientIds);
        }

        // 2.4 invoice-products
        $invoiceQuery
            ->select('invoices.*')
            ->addSelect('invoice-products.invoice_id')
            ->selectRaw('sum(`invoice-products`.`count`) as \'totalCount\'')
            ->leftJoin('invoice-products', 'invoices.id', '=', 'invoice-products.invoice_id')
            ->groupBy(['invoices.id'])
            ->havingRaw('`invoice-products`.`invoice_id` IS NOT NULL');

        $invoiceCollection = InvoiceResource::collection($invoiceQuery->get());

        // 4 merge collections
        // 4.1 if only suppliers set - show only supplies
        if ((! empty($request->supplierIds)) && (empty($request->clientIds))) {
            $docs = $supplyCollection->all();

            // add json encode for remove default `data` json wrapper
            $docsCollection = [
                // merge collections
                'data'                 => $docs,
                'averageInvoiceAmount' => 0,
            ];
        } // 4.2 if only clients set - show only invoices
        elseif (empty($request->supplierIds) && (! empty($request->clientIds))) {
            $docs = $invoiceCollection->all();
            $averageInvoiceAmount = 0;

            foreach ($docs as $doc) {
                $averageInvoiceAmount += (float) $doc['amount'];
            }

            $averageInvoiceAmount = round($averageInvoiceAmount / count($docs), 2);

            // add json encode for remove default `data` json wrapper
            $docsCollection = [
                // merge collections
                'data'                 => $docs,
                'averageInvoiceAmount' => $averageInvoiceAmount,
            ];
        } // 4.3 if both are empty - show both
        elseif (empty($request->supplierIds) && (empty($request->clientIds))) {
            $invoices = $invoiceCollection->all();
            $averageInvoiceAmount = 0;

            foreach ($invoices as $invoice) {
                $averageInvoiceAmount += (float) $invoice['amount'];
            }

            $invoiceCount = count($invoices);
            $averageInvoiceAmount = $invoiceCount
                ? round($averageInvoiceAmount / count($invoices), 2)
                : 0;

            $docs = $supplyCollection->merge($invoiceCollection);
            $docsCollection = [
                // merge collections
                'data'                 => $docs,
                'averageInvoiceAmount' => $averageInvoiceAmount,
            ];
        }
        // 4.4 if all of them set - show all filtered,
        // or if no one set - we should show error or empty array
        else {
            $docsCollection = [
                'data'                 => [],
                'averageInvoiceAmount' => 0,
            ];
        }

        // 3 merge collections
        return $docsCollection;
    }

    public function revisionBySuppliers(BaseRequest $request)
    {
        $reportAboutSalesByProductsPresenter = new RevisionBySuppliersPresenter([
            'from'       => $request->from,
            'to'         => $request->to,
            'supplierId' => $request->supplierId,
        ]);

        return $reportAboutSalesByProductsPresenter->getDocs();
    }

    public function bySupplies(BaseRequest $request)
    {
        $reportAboutSalesByProductsPresenter = new ReportBySuppliesPresenter([
            'from'       => $request->from,
            'to'         => $request->to,
            'supplierId' => $request->supplierId,
        ]);

        return $reportAboutSalesByProductsPresenter->getDocs();
    }
}
