<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\BaseRequest;
use App\Http\Resources\InvoiceProduct\InvoiceProductResource;
use App\Models\Invoice;
use App\Models\InvoiceProduct;

class InvoiceProductController extends \App\Http\Controllers\Controller
{
    const INVOICE_PRODUCT__RECORDS_PER_PAGE = 10;

    public function getInvoiceProductsByInvoiceId(BaseRequest $request)
    {
        $invoiceProducts = InvoiceProduct::where('invoice_id', $request->id)->get();

        return InvoiceProductResource::collection($invoiceProducts);
    }

    public function getAllInvoices()
    {
        $invoices = Invoice::all();

        return InvoiceProductResource::collection($invoices);
    }
}
