<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Products\GetAllRequest;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductWithInvoicePricesResource;
use App\Http\Resources\Product\ProductWithSupplyPricesResource;
use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function list(GetAllRequest $request)
    {
        $products = Product::query()
            ->orderBy('name')
            ->get();

        switch ($request->validated()['order'] ?? null) {
            case 'supplier-code':
                $products = $products->sortBy(fn(Product $product) => $product->search_string);
                break;
        }

        return ProductResource::collection($products);
    }

    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function getProductsWithInvoicePrices()
    {
        $products = Product::query()
                           ->with('productInvoicePrices')
                           ->orderBy('products.name')
                           ->get();

        return ProductWithInvoicePricesResource::collection($products);
    }

    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function getProductsWithSupplyPrices()
    {
        $products = Product::query()
                           ->with('productSupplyPrices')
                           ->orderBy('products.name')
                           ->get();

        return ProductWithSupplyPricesResource::collection($products);
    }

    /**
     * @param  GetAllRequest  $request
     *
     * @return ResourceCollection
     */
    public function getAll(GetAllRequest $request)
    {
        $products = Product::query()
                           ->orderBy('name')
                           ->get();

        switch ($request->validated()['order'] ?? null) {
            case 'supplier-code':
                $products = $products->sortBy(fn(Product $product) => $product->search_string);
                break;
        }

        return ProductResource::collection($products);
    }

    /**
     * @return Response
     */
    public function getAllWithBarcode()
    {
        $products = Product::query()
                           ->whereNotNull('barcode')
                           ->orderBy('id')
                           ->get();

        return response([
            'data' => $products->mapWithKeys(
                fn(Product $product) => [$product->barcode => (new ProductResource($product))->resolve()]
            )
        ]);
    }
}
