<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SupplierResource;
use App\Models\Supplier;
use Illuminate\Http\Request;

class SupplierController extends \App\Http\Controllers\Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $suppliers = Supplier::query()->orderBy('name', 'asc')->get();
        return SupplierResource::collection($suppliers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);

        if (!$supplier) {
            abort(404, 'Поставщик не найден');
        }

        return [
            'success' => true,
            'supplier' => $supplier,
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'    => 'required',
            'phone'   => 'required',
            'address' => 'required'
        ]);

        $supplier = Supplier::create($request->all());

        return [
            'success' => true,
            'supplier' => $supplier,
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'    => 'required',
            'phone'   => 'required',
            'address' => 'required'
        ]);

        $supplier = Supplier::find($id);
        $updatedSuccess = $supplier->update($request->all());

        return [
            'success' => $updatedSuccess,
            'supplier' => $supplier,
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $success = $supplier->delete();

        return [
            'success' => $success,
        ];
    }
}
