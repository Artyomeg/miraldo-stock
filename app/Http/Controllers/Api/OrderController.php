<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Repositories\OrderRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    /**
     * @param  OrderRepository  $repository
     */
    public function __construct(
        protected OrderRepository $repository
    ) {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return ResourceCollection
     */
    public function index()
    {
        return OrderResource::collection(
            $this->repository->getIndexCollection()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UpdateOrderRequest  $request
     *
     * @return JsonResource
     */
    public function store(UpdateOrderRequest $request)
    {
        $number = Order::getNewNumber();

        $order = Order::query()->create(
            array_merge($request->validated(), compact('number'))
        );

        $this->repository->storeProducts($order, $request->validated()['orderProducts']);

        return new OrderResource(
            $order->load('client', 'orderProducts', 'orderProducts.product')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  Order  $order
     *
     * @return JsonResource
     */
    public function show(Order $order)
    {
        return new OrderResource(
            $order->load('client', 'orderProducts', 'orderProducts.product')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateOrderRequest  $request
     * @param  Order  $order
     *
     * @return JsonResource
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        $order->update(
            $request->validated()
        );

        $data = $request->validated()['orderProducts'];
//        $order->orderProducts->each(function (OrderProduct $order_product) use ($order, $data) {
//            // удаляем помеченные на удаление
//            if (! isset($data['old'][$order_product->id])) {
//                $order_product->delete();
//
//                return;
//            }
//
//            // если нуждается в обновлении - обновим
//            if (
//                (int) $data['old'][$order_product->id]['productId'] !== $order_product->product_id
//                || $data['old'][$order_product->id]['comment'] !== $order_product->comment
//            ) {
//                $order_product->update([
//                    'comment' => $data['old'][$order_product->id]['comment'] ?? null,
//                    'product_id' => $data['old'][$order_product->id]['productId'],
//                ]);
//            }
//        });

        // переберем присланные новые - добавляем все
//        collect($data['new'] ?? [])
//            ->each(function (array $new_order_product) use ($order) {
//                OrderProduct::query()->create([
//                    'product_id' => $new_order_product['productId'],
//                    'order_id'   => $order->id,
//                    'comment'    => $new_order_product['comment'] ?? null,
//                ]);
//            });
        $this->repository->storeProducts($order, $request->validated()['orderProducts']);

        return new OrderResource(
            $order->load('client', 'orderProducts', 'orderProducts.product')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Order  $order
     *
     * @return JsonResponse
     */
    public function destroy(Order $order)
    {
        DB::transaction(function () use ($order) {
            $order->orderProducts()->delete();
            $order->delete();
        });

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
