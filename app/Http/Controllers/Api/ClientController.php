<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends \App\Http\Controllers\Controller
{
    const PRODUCT__RECORDS_PER_PAGE = 10;

    public function list(Request $request)
    {
        $clientsQuery = Client::query()->orderBy('name', 'asc');

        if ($request->onlyNotArchived) {
            $clientsQuery = $clientsQuery->isNotArchived();
        }

        $clients = $clientsQuery->get();

        return ClientResource::collection($clients);
    }
}
