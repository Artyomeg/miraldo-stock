<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ActivityIndexRequest;
use App\Http\Resources\Activity\ActivityCollection;
use App\Repositories\ActivityRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use RuntimeException;

class ActivityController extends Controller
{
    /**
     * @param  ActivityIndexRequest  $request
     *
     * @return ActivityCollection
     */
    public function index(ActivityRepository $repository, ActivityIndexRequest $request)
    {
        $model_type  = isset($request->validated()['model_type']) ? $request->validated()['model_type'] : null;
        $model_id    = isset($request->validated()['model_id']) ? $request->validated()['model_id'] : null;
        $description = isset($request->validated()['description']) ? $request->validated()['description'] : null;

        if ($model_type) {
            if (! class_exists($model_type)) {
                throw ValidationException::withMessages([
                    'model_type' => __('validation.model_not_found'),
                ]);
            }
        }

        return new ActivityCollection(
            $repository->searchActivities(
                $model_type,
                $model_id,
                $description
            )
        );
    }

    /**
     * @param  Request  $request
     */
    public function store(Request $request)
    {
        throw new RuntimeException('Method not implemented');
    }

    /**
     * @param  int  $id
     */
    public function show($id)
    {
        throw new RuntimeException('Method not implemented');
    }

    /**
     * @param  Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        throw new RuntimeException('Method not implemented');
    }

    /**
     * @param  int  $id
     */
    public function destroy($id)
    {
        throw new RuntimeException('Method not implemented');
    }
}
