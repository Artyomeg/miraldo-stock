<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\SupplierInvoice;
use App\Http\Resources\SupplierInvoice\SupplierInvoiceResource;

class SupplierInvoiceController extends \App\Http\Controllers\Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        $supplierInvoice = SupplierInvoice::find($id);

        if (!$supplierInvoice) {
            abort(404, 'Счет поставщика не найден');
        }

        return [
            'success' => true,
            'supplierInvoice' => new SupplierInvoiceResource($supplierInvoice),
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'supplier_id' => 'required',
            'date'        => 'required',
            'amount'      => 'required'
        ]);

        $values = (array) $request->all();

        if (isset($values['supplier_id'])) {
            $docSupplier = Supplier::find($values['supplier_id']);
            $values['currency_id'] = $docSupplier->currency_id;
        }

        $values['number'] = SupplierInvoice::getNewNumber();

        return SupplierInvoice::create($values);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'supplier_id' => 'required',
            'date'        => 'required',
            'amount'      => 'required'
        ]);

        $supplierInvoice = SupplierInvoice::find($id);
        $updatedSuccess = $supplierInvoice->update($request->all());

        return [
            'success' => $updatedSuccess,
            'supplierInvoice' => new SupplierInvoiceResource($supplierInvoice),
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplierInvoice = SupplierInvoice::find($id);
        $success = $supplierInvoice->delete();

        return [
            'success' => $success,
        ];
    }
}
