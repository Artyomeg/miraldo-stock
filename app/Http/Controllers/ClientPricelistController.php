<?php

namespace App\Http\Controllers;

use App\Models\ClientPricelist;
use App\Models\ClientPricelistProductPrice;
use Illuminate\Http\Request;

class ClientPricelistController extends Controller
{
    // TODO: почистить локаль для неактуальных сущностей прайслистов и товаров-цен, в частности по __
    const PRICELIST__RECORDS_PER_PAGE = 100;

    public function __construct()
    {
        $this->middleware(['auth', 'locale']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('clientPricelists.index')
            ->with('i', (request()->input('page', 1) - 1) * self::PRICELIST__RECORDS_PER_PAGE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientPricelists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name'  => 'required'
        ]);

        $pricelist = ClientPricelist::create($request->all());

        return redirect()->route('client-pricelists.index')
            ->with('success', "Прайс-лист клиента \"{$pricelist->name}\" создан");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pricelist = ClientPricelist::find($id);

        if ($pricelist === null) {
            abort(404, 'Прайслист для клиентов не найден');
        }

        $currentDate = date('d.m.Y');
        $ORDER = 'ASC';

        $productPrices = ClientPricelistProductPrice::join('products', 'client_pricelist_product_prices.product_id', '=', 'products.id')
            ->select('client_pricelist_product_prices.*')
            ->where('client_pricelist_id', $id)
            ->orderBy('products.name', $ORDER)
            ->get();

        return view('clientPricelists.show', compact(['currentDate', 'pricelist', 'productPrices']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pricelist = ClientPricelist::find($id);
        return view('clientPricelists.edit', compact(['pricelist']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name'  => 'required'
        ]);

        $pricelist = ClientPricelist::find($id);
        $pricelist->assigned_to_euro = (boolean)$request->get('assigned_to_euro');
        $pricelist->name = $request->get('name');
        $pricelist->save();

        return redirect()->route('client-pricelists.index')
            // TODO: указать сообщение
            ->with('success', "Прайс-лист клиента \"{$pricelist->name}\" обновлен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricelist     = ClientPricelist::find($id);
        $pricelistName = $pricelist->name;
        $pricelist->delete();

        return redirect()->route('client-pricelists.index')
            ->with('success', "Прайс-лист клиента \"{$pricelistName}\" удален");
    }
}
