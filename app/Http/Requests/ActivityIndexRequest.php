<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityIndexRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'model_type' => [
                'nullable',
                'string'
            ],

            'model_id' => [
                'nullable',
                'integer'
            ],

            'description' => [
                'nullable',
                'string'
            ],
        ];
    }
}
