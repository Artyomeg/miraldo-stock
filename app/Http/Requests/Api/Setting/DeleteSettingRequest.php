<?php

namespace App\Http\Requests\Api\Setting;

use Illuminate\Foundation\Http\FormRequest;

class DeleteSettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id' => [
                'required',
                'array',
                'min:1',
            ],

            'id.*' => [
                'required',
                'int',
                'min:1',
            ],
        ];
    }
}
