<?php

namespace App\Http\Requests\Api\Reports;

use Illuminate\Foundation\Http\FormRequest;

class ReportsByProductsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => [
                'required',
                'date',
            ],

            'to' => [
                'required',
                'date',
            ],

            'productId' => [
                'nullable',
                'int',
            ],

            'supplierIds' => [
                'exclude_with:clientIds',
                'nullable',
                'array',
            ],

            'supplierIds.*' => [
                'int',
            ],

            'clientIds' => [
                'exclude_with:supplierIds',
                'nullable',
                'array',
            ],

            'clientIds.*' => [
                'int',
            ],
        ];
    }
}
