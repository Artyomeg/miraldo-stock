<?php
namespace App\Http\Middleware;

use Closure;
use App;
use Request;

class LocaleMiddleware
{
    // указываем, какие языки будем использовать в приложении
    public static $languages    = ['en', 'ru', 'it'];
    // основной язык, который не должен отображаться в URl
    public static $mainLanguage = 'ru';

    /*
     * Проверяет наличие корректной метки языка в текущем URL
     * Возвращает метку или значеие null, если нет метки
     */
    public static function getLocaleOld()
    {
        $uri = Request::path(); // получаем URI
        $segmentsURI = explode('/', $uri); // делим на части по разделителю "/"

        // Проверяем метку языка  - есть ли она среди доступных языков
        if (!empty($segmentsURI[0]) && in_array($segmentsURI[0], self::$languages)) {
            if ($segmentsURI[0] != self::$mainLanguage) {
                return $segmentsURI[0];
            }
        }

        return null;
    }

    public static function getLocale()
    {
        $langs = config('app.locales');

        // если есть кука - установим язык
        if (cookie('lang')->getValue()) {
            if (($key = array_search(cookie('lang')->getValue(), $langs)) === false) {
                abort('404');
            }

            return $langs[$key];
        } else {
            // устанавливаем куки
//            cookie('lang', config('app.locale'), 45000);
            return null;
        }

        return null;
    }

    /*
    * Устанавливает язык приложения в зависимости от метки языка из URL
    */
    public function handle($request, Closure $next)
    {
        if (session()->has('locale')) {
            app()->setLocale(session('locale'));
        }
        else {
            app()->setLocale(config('app.locale'));
        }

        return $next($request); //пропускаем дальше - передаем в следующий посредник
    }
}