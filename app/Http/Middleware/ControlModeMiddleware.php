<?php
namespace App\Http\Middleware;

use Closure;
use App\Models\ControlMode;

class ControlModeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!ControlMode::checkModeIsControl()) {
            return redirect()->route('control');
        }

        return $next($request);
    }
}
