<?php

use Carbon\Carbon;
use Carbon\CarbonPeriod;

/**
 * @param  mixed  $number
 * @param  bool  $showPennies
 *
 * @return string
 */
function fineNumber($number, $showPennies = true)
{
    if ($showPennies) {
        return ($number > floor($number))
            ? number_format((float) $number, 2, '.', ' ')
            : number_format((float) $number, 0, '.', ' ');
    }

    return number_format(round((float) $number), 0, '.', ' ');
}

/**
 * @param  \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder  $model
 *
 * @return array|mixed|string|string[]
 * @example `$rawSQL = getSql($supplyQuery);`
 */
function getSql($model)
{
    $replace = function ($sql, $bindings) {
        $needle = '?';
        foreach ($bindings as $replace) {
            $pos = strpos($sql, $needle);
            if ($pos !== false) {
                if (is_string($replace)) {
                    $replace = ' "' . addslashes($replace) . '" ';
                }
                $sql = substr_replace($sql, $replace, $pos, strlen($needle));
            }
        }

        return $sql;
    };

    return $replace($model->toSql(), $model->getBindings());
}

/**
 * @return float
 *
 * Использование:
 * $time_start = microtime_float();
 * ...
 * $time = microtime_float() - $time_start;
 */
function microtime_float()
{
    [$usec, $sec] = explode(" ", microtime());

    return ((float) $usec + (float) $sec);
}

/**
 * @return bool
 */
function isNightMode()
{
    return ! now()->isBetween(
        Carbon::createFromFormat('H:i', config('auth.night_mode.till')),
        Carbon::createFromFormat('H:i', config('auth.night_mode.from'))
    );
}