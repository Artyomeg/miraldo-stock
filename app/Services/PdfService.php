<?php declare(strict_types = 1);

namespace App\Services;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class PdfService
{
    /** @var \Barryvdh\DomPDF\PDF */
    protected $builder;

    /** @var string */
    protected $filename;

    /**
     * @param  string  $view
     * @param  mixed  $data
     */
    public function __construct(string $view, mixed $data = null)
    {
        $this->builder = Pdf::loadView($view, $data);
    }

    /**
     * @param  string  $filename
     *
     * @return $this
     */
    public function withFilename(string $filename)
    {
        $this->filename = (string) Str::of($filename)->rtrim('.pdf');

        return $this;
    }

    public function landscape()
    {
        $this->builder->setPaper('a4', 'landscape');

        return $this;
    }

    /**
     * @return Response
     */
    public function download()
    {
        return $this->builder->download(
            $this->getFilename()
        );
    }

    /**
     * @return Response
     */
    public function stream()
    {
        return $this->builder->stream(
            $this->getFilename()
        );
    }

    /**
     * @return string
     */
    protected function getFilename()
    {
        return sprintf(
            "%s.pdf",
            $this->filename ?: Str::uuid()
        );
    }
}
