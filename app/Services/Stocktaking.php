<?php

namespace App\Services;

use App\Models\Payment;
use App\Models\Invoice;
use Illuminate\Support\Facades\DB;

class Stocktaking
{
    private $yearUntil;
    private $indexedClients = [];

    public function __construct($yearUntil)
    {
        $this->yearUntil = $yearUntil;

        $this->setPaymentAmounts();
        $this->setInvoiceAmounts();

        $this->deleteOldYeadDocs();
        $this->getDebtsAndCreateInvoices();

        echo 'Done!';
    }

    private function setPaymentAmounts()
    {
        $paymentSums = Payment::query()
            ->select(
                'payments.client_id',
                DB::raw('sum(payments.amount) amountsum')
            )
            ->whereYear('date', '<=', $this->yearUntil)
            ->groupBy(['payments.client_id'])
            ->get();

        foreach ($paymentSums as $paymentSum) {
            if (!array_key_exists($paymentSum->client_id, $this->indexedClients)) {
                $this->indexedClients[$paymentSum->client_id]['invoiceAmounts'] = 0;
            }

            $this->indexedClients[$paymentSum->client_id]['paymentAmounts'] = (float) $paymentSum->amountsum;
        }
    }

    private function setInvoiceAmounts() {
        $invoiceSums = Invoice::query()
            ->select(
                'invoices.client_id',
                DB::raw('sum(invoices.amount) amountsum')
            )
            ->whereYear('date', '<=', $this->yearUntil)
            ->groupBy(['invoices.client_id'])
            ->get();

        foreach ($invoiceSums as $invoiceSum) {
            if (!isset($this->indexedClients[$invoiceSum->client_id])) {
                $this->indexedClients[$invoiceSum->client_id]['paymentAmounts'] = 0;
            }

            $this->indexedClients[$invoiceSum->client_id]['invoiceAmounts'] = (float) $invoiceSum->amountsum;
        }
    }

    private function deleteOldYeadDocs() {
        Payment::whereYear('date', '<=', $this->yearUntil)->delete();
        Invoice::whereYear('date', '<=', $this->yearUntil)->delete();
    }

    private function getDebtsAndCreateInvoices() {
        $newInvoiceNumber = Invoice::getNewNumber($this->yearUntil);
        $newPaymentNumber = Payment::getNewNumber($this->yearUntil);

        foreach ($this->indexedClients as $clientId => $indexedClient) {
            $currentClientDebt = $indexedClient['invoiceAmounts'] - $indexedClient['paymentAmounts'];

            // если погрешность в 1 рубль - проигнорируем её (пока не используется)
//            if ($currentClientDebt > -1 && $currentClientDebt < 1) {
//                continue;
//            }

            if ($currentClientDebt > 0) {
                $newInvoice = Invoice::create([
                    'amount'    => $currentClientDebt,
                    'client_id' => $clientId,
                    'date'      => "$this->yearUntil-12-31",
                    'number'    => $newInvoiceNumber
                ]);

                $newInvoiceNumber++;
            } elseif ($currentClientDebt < 0) {
                $newPayment = Payment::create([
                    'amount'    => -$currentClientDebt,
                    'client_id' => $clientId,
                    'date'      => "$this->yearUntil-12-31",
                    'comment'   => "Итог года $this->yearUntil",
                    'number'    => $newPaymentNumber
                ]);
            }
        }
    }
}
