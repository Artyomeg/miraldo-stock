<?php

namespace App\Console\Commands;

use App\Models\Client;
use App\Models\Invoice;
use App\Models\Supply;
use Illuminate\Console\Command;

class RevisionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        echo '123';


        return;
        // 1 get supplies
        $supplies = Supply::query()
            ->whereYear('date', '=', date('Y'))
            ->orderBy('date', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        $suppliesUpdated = 0;

        if (!empty($supplies)) {
            foreach ($supplies as $i => $supply) {
                if ($supply->number != $i + 1) {
                    $supply->number = $i + 1;
                    $supply->save();

                    $suppliesUpdated++;
                }
            }
        }

        echo "Supplies updated: {$suppliesUpdated}\n";


        // 2 get invoices
        $invoices = Invoice::query()
            ->whereYear('date', '=', date('Y'))
            ->orderBy('date', 'asc')
            ->orderBy('id', 'asc')
            ->get();

        $invoicesUpdated = 0;

        if (!empty($invoices)) {
            foreach ($invoices as $i => $invoice) {
                if ($invoice->number != $i + 1) {
                    $invoice->number = $i + 1;
                    $invoice->save();

                    $invoicesUpdated++;
                }
            }
        }
        echo "Invoices updated: {$invoicesUpdated}\n";
    }

    private function getClientDebtUntilYear()
    {
        $debt = 0;

        if ($this->invoices) {
            foreach ($this->invoices as $invoice) {
                $debt += $invoice->amount;
            }
        }

        if ($this->payments) {
            foreach ($this->payments as $payment) {
                $debt -= $payment->amount;
            }
        }

        return $debt;
    }
}
