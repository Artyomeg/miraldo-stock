<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\UnexpectedSessionUsageException;

class ResetAuth extends Command
{
    /** @var string */
    protected $signature = 'auth:reset';

    /** @var string */
    protected $description = 'Logout users when day/night mode changed';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        switch (config('session.driver')) {
            case 'file':
                $sessions = glob(config('session.files') . '/*');
                foreach ($sessions as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                break;

            case 'database':
                DB::connection(config('session.connection'))
                    ->table(config('session.table'))
                    ->truncate();
                break;

            default:
                throw new UnexpectedSessionUsageException('Unexpected session driver');
        }

        return Command::SUCCESS;
    }
}
